module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
            return
        }
        if (Game.time >= creep.memory.targettime + 25) {
            creep.memory.target = undefined;
        }
        var constructionSite = undefined;
        if (!creep.isBoosted() && creep.ticksToLive < 350 && creep.hitsMax >= 2600) {
            creep.memory.renewing = true;
        }
        if (creep.memory.renewing === true) {
            goRenew(creep);
            return;
        }
        if (creep.ticksToLive < 500 && creep.hitsMax >= 2600) {
            goRenew(creep)
            return
        } else {
            if (creep.memory.working === true && creep.carry.energy === 0) {
                creep.memory.working = false;
            } else if (creep.memory.working === false && creep.carry.energy === creep.carryCapacity) {
                creep.memory.working = true;
                creep.memory.target = undefined;
            }
            if (creep.memory.targetTime + 50 < Game.time) {
                creep.memory.target = undefined;
            }

            if (creep.memory.working === true) {
                if (_.isString(creep.memory.target)) {
                    constructionSite = Game.getObjectById(creep.memory.target)
                } else {
                    if (Object.keys(Game.constructionSites).length > 0) {
                        constructionSitez = creep.pos.findClosestConstructionSite().id;
                        constructionSite = Game.getObjectById(constructionSitez);
                        creep.memory.target = constructionSite.id;
                        creep.memory.targetTime = Game.time
                    } else {
                        creep.memory.role = "pioneer";
                        creep.memory.homeRoom = creep.pos.findClosestSpawn().room.name;
                        console.log(creep.name + ' is turning into a pioneer for room: ' + creep.memory.homeRoom);
                    }
                }
                if (_.isEmpty(constructionSite)) {
                    creep.memory.target = undefined;
                    creep.memory.stuck++
                    if (creep.memory.stuck > 15) {
                        creep.memory.role = "pioneer";
                        creep.memory.homeRoom = creep.pos.findClosestSpawn().room.name;
                        console.log(creep.name + ' is turning into a pioneer for room: ' + creep.memory.homeRoom);
                    }
                } else {
                    delete creep.memory.stuck
                }
                if (_.isString(creep.memory.target) && constructionSite.pos) {

                    if (creep.build(constructionSite) !== 0) {
                        creep.travelTo(constructionSite, {range: 1, ignoreRoads: false, allowSK: true});
                    }

                } else {
                    creep.travelTo(constructionSite, {range: 2, ignoreRoads: false, allowSK: true});
                }

            } else if (creep.memory.working === false) {
                conta = creep.getEasyEnergy()

                if (conta) {
                    if (creep.pos.isNearTo(conta)) {
                        creep.withdraw(conta, RESOURCE_ENERGY)
                    } else {
                        creep.travelTo(conta, {
                            ignoreRoads: (creep.carry.energy === 0),
                            ignoreCreeps: false,
                            allowSK: true
                        });
                    }
                }
                if (!conta) {
                    source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
                    if (source && creep.harvest(source) !== 0) {
                        creep.travelTo(source, {ignoreRoads: (_.sum(creep.store) === 0)});

                    }
                }
            }
        }
        let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (droppedEnergy.length >= 1 && droppedEnergy[0].resourceType === RESOURCE_ENERGY) {
            creep.pickup(droppedEnergy[0]);
        }
    }
}
