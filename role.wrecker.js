module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        missiontarget = false
        target = false
        if (mission.target) {
            missiontarget = Game.getObjectById(mission.target)
            if (missiontarget) {
                target = Game.getObjectById(creep.memory.targetToDismantle)
                if (target && creep.dismantle(target) !== 0) {
                    creep.travelTo(target, {range: 1, brave: true, maxRooms: 1, ignoreCreeps: false})
                    return
                } else {
                    return
                }
            }
        }
        cacheTargets(creep.pos);
        if (creep.memory.targetToDismantle) {
            target = Game.getObjectById(creep.memory.targetToDismantle)
            if (target && creep.dismantle(target) !== 0) {
                creep.travelTo(target, {range: 1, brave: true, ignoreCreeps: false})
                return
            }
        }
        if (creep.dismantle(missiontarget) !== 0) {
            t = creep.moveTo(missiontarget)
            if (t === -2) {
                findWallToDestroy(creep, missiontarget)
                target = Game.getObjectById(creep.memory.targetToDismantle)
                if (target && creep.dismantle(target) !== 0) {
                    creep.travelTo(target, {range: 1, brave: true, maxRooms: 1, ignoreCreeps: false})
                    return
                }
            }
        } else {
            creep.say('Yum');
            return
        }
        if (!target && !missiontarget && creep.pos.roomName === mission.missionRoom) {
            creep.memory.mission = {}
            creep.memory.mission.missionRoom = mission.missionRoom
            creep.memory.mission.target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (c) => (
                    c.structureType !== STRUCTURE_WALL
                    && c.structureType !== STRUCTURE_RAMPART
                    && c.structureType !== STRUCTURE_ROAD
                    && c.structureType !== STRUCTURE_CONTAINER
                    && c.structureType !== STRUCTURE_CONTROLLER
                    && c.structureType !== STRUCTURE_EXTRACTOR
                    && c.structureType !== STRUCTURE_KEEPER_LAIR
                    && c.structureType !== STRUCTURE_PORTAL
                    && !(c.structureType === STRUCTURE_STORAGE && c.store && c.store.energy > 150000)
                )
            })
        } else {
            newPos = new RoomPosition(25, 25, mission.missionRoom)
            creep.travelTo(newPos, {useFindRoute: true})
        }
    }
}


function findWallToDestroy(creep, targetToDismantle) {
    let path = PathFinder.search(creep.pos, {pos: targetToDismantle.pos, range: 1}, {
        maxRooms: 1, roomCallback: getRoomCallbackForWallDestruction
    }).path;
    for (let roomPos of path) {
        let structures = roomPos.lookFor(LOOK_STRUCTURES);
        if (structures.length > 0) {
            for (let s of structures) {
                if (s.structureType === STRUCTURE_WALL || s.structureType === STRUCTURE_RAMPART) {
                    creep.memory.targetToDismantle = s.id;
                    return;
                }
            }
        }
    }
    log.error("Found no walls/ramparts to destroy to get to an interesting target in room " + creep.room.name);
}

function getRoomCallbackForWallDestruction(roomName) {
    let room = Game.rooms[roomName];
    if (!room)
        return new PathFinder.CostMatrix;
    let costs = new PathFinder.CostMatrix;
    room.find(FIND_STRUCTURES).forEach(function (structure) {
        if (structure.structureType === STRUCTURE_WALL || structure.structureType === STRUCTURE_RAMPART) {
            costs.set(structure.pos.x, structure.pos.y, Math.min(150, structure.hits / 20000));
        }
    });
    return costs;
}
