require('prototype.Room')();
require('prototype.Structure')();

global.runLink = function () {
    for (r in Game.rooms) {
        
        let room = Game.rooms[r];
        if (!room || !room.controller || (room.controller && !room.controller.my)) {
            continue
        }
        
        let links = room.find(FIND_MY_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_LINK});
        if (_.isUndefined(room.memory.links)) {
            room.memory.links = {};
        }
        let storageLink = undefined;
        let sourceLink = undefined;
        let controllerLink = undefined
        if (room.memory.links.storageLink && Game.time % 20 !== 0) {
            storageLink = Game.getObjectById(room.memory.links.storageLink);
            if (!storageLink) {
                delete room.memory.links
            }
            continue
        }
        if (room.memory.links.controllerLink && Game.time % 20 !== 0) {
            controllerLink = Game.getObjectById(room.memory.links.controllerLink);
            if (!controllerLink) {
                delete room.memory.links
            }
            continue
        }
        for (link of links) {
            if (link.cooldown > 0) {
                continue;
            }
            if (storageLink && link.id ===storageLink.id) {
                continue;
            }
            if (controllerLink && link.id === controllerLink.id) {
                continue;
            }
            if (link && link.pos && link.pos.inRangeTo(room.controller.pos, 3)) {
                controllerLink = link;
                room.memory.links.controllerLink = link.id;
            }
            if (link && link.pos && room.storage && link.pos.inRangeTo(room.storage.pos, 3)) {
                storageLink = link;
                console
                room.memory.links.storageLink = link.id;
            }
            if ((!storageLink||storageLink.id !== link.id) && (!controllerLink|| controllerLink.id !== link.id) && link.energy >= 600 && link.cooldown === 0) {
                sourceLink = link
            }
        }
        if (sourceLink != undefined && controllerLink != undefined) { 
            if ((controllerLink && controllerLink.store.getFreeCapacity(RESOURCE_ENERGY) > (LINK_CAPACITY * 0.5))
                &&
                (sourceLink.energy >= (sourceLink.store.getCapacity(RESOURCE_ENERGY) * 0.5))) {
                sourceLink.transferEnergy(controllerLink);
                console.log('running link in '+ r + ' source to controller')
                continue;
            }
        }
        if (sourceLink != undefined && storageLink != undefined) {
            if ((storageLink && storageLink.store.getFreeCapacity(RESOURCE_ENERGY) > (LINK_CAPACITY * 0.5))
                &&
                (sourceLink.energy >= (sourceLink.energyCapacity * 0.5))) {
                sourceLink.transferEnergy(storageLink);
                console.log('running link in '+ r + ' source to storage')
                continue
            }
        }
        if (controllerLink != undefined && storageLink != undefined) {
            if ((controllerLink && controllerLink.store.getFreeCapacity(RESOURCE_ENERGY) > (LINK_CAPACITY * 0.5))
                &&
                (storageLink.energy >= (storageLink.energyCapacity * 0.5))) {
                storageLink.transferEnergy(controllerLink);
                console.log('running link in '+ r + ' storage to controller')
            }
        }
    }
};

module.exports = runLink;
