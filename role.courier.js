module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        var structure = undefined;
        var conta = {};
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.roomName
        }
        if (creep.pos.roomName !== creep.memory.homeRoom) {
            creep.travelTo(Game.rooms[creep.memory.homeRoom].controller)
        }
        if (creep.memory.working === true && _.sum(creep.carry) === 0) {
            creep.memory.working = false;
            creep.memory.target = undefined;
        } else if (_.sum(creep.store) >= (creep.store.getCapacity())) {
            creep.memory.working = true;
            creep.memory.target = undefined;
        }
        if (creep.store && creep.store.energy >= (creep.room.energyCapacityAvailable - creep.room.energyAvailable)) {
            creep.memory.working = true;
        }
        if (creep.store && creep.store.getFreeCapacity() > 0 && (creep.room.energyCapacityAvailable === creep.room.energyAvailable)) {
            if (creep.memory.fullTimer) {
                creep.memory.fullTimer++
            } else {
                creep.memory.fullTimer = 1
            }
            if (creep.memory.fullTimer > 3) {
                creep.memory.working = false;
                creep.memory.fullTimer = 0
            }
        }
        if (Game.time % 15 === 0) {
            creep.memory.target = undefined;
        }
        if (Game.time % 11 === 0) {
            creep.memory.target = undefined;
        }
        if (creep.memory.working === true) {
            if (creep.memory.target !== undefined) {
                structure = Game.getObjectById(creep.memory.target);
                if (structure) {
                    global.takenExtensions.add(creep.memory.target);
                }
            }
            if (structure && structure.energy && structure.energy === structure.energyCapacity) {
                structure = creep.memory.target = undefined;
            }
            if (structure === undefined) {
                structure = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                    filter: (s) => ((s.energy / s.energyCapacity) < 0.95) && s.structureType === STRUCTURE_TOWER
                });
                if (!structure) {
                    structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                        filter: (s) => !global.takenExtensions.has(s.id) && s.energy < s.energyCapacity && s.structureType !== STRUCTURE_CONTAINER && s.structureType !== STRUCTURE_LINK && s.structureType != STRUCTURE_LAB
                    });
                }
                if (structure && creep.memory.role != 'pioneer') {
                    creep.memory.target = structure.id
                    global.takenExtensions.add(structure.id)
                }

            }

            if (structure !== undefined) {
                if (creep.pos.isNearTo(structure)) {
                    creep.transfer(structure, RESOURCE_ENERGY)
                    creep.memory.target = undefined;
                    global.takenExtensions.delete(creep.memory.target)
                    if (structure && structure.store && structure.store.getFreeCapacity(RESOURCE_ENERGY) >= creep.store.getUsedCapacity(RESOURCE_ENERGY)) {
                        creep.grabEasyEnergy()
                    }
                } else {
                    creep.travelTo(structure, {maxRooms: 1, repath: 0.2});
                }
            }
        } else {
            conta = creep.getEasyEnergy()
            if (!conta) {
                creep.memory.working = true
                conta = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: function (s) {
                        return s.structureType === STRUCTURE_CONTAINER
                    }
                });
            }
            if (creep.withdraw(conta, RESOURCE_ENERGY) !== 0) {
                creep.travelTo(conta, {offRoad: (_.sum(creep.store) === 0), stuckValue: 1, maxRooms: 1, range: 1});
            }
        }

        let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (droppedEnergy.length >= 1 && droppedEnergy[0].resourceType === RESOURCE_ENERGY) {
            creep.pickup(droppedEnergy[0]);
        }
    }
};
