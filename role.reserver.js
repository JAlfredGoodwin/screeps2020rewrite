module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (creep.memory.mission) {
            mem = creep.memory.mission
        } else {
            creep.memory.role = 'zombie';
            return
        }
        targetController = Game.getObjectById(mem.target)
        delete Memory.missions.civil[mem.mission]
        if (!targetController) {
            targetController = {};
            targetController.pos = new RoomPosition(25, 25, mem.missionRoom);
        }
        if (creep.pos.isNearTo(targetController)) {
            if ((targetController.owner  &&  !targetController.my) || (targetController.reservation && targetController.reservation.username !== WHOAMI)) {
                creep.attackController(targetController);
                creep.memory.role = 'zombie';
            } else {
                creep.reserveController(targetController);
            }
        } else {
            if (targetController.pos) {
                creep.travelTo(targetController.pos, {useFindRoute:true});
            }
        }
    }
}
