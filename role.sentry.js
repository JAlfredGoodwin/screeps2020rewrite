module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (creep.memory.renewing && creep.ticksToLive > 1000) {
            creep.memory.renewing = false;
        }
        if (!creep.isBoosted() && creep.ticksToLive < 350 && creep.hitsMax > 2200) {
            creep.memory.renewing = true;
        }
        if (creep.memory.renewing === true) {
            goRenew(creep);
            return;
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        missiontarget = false
        if (mission.target) {
            missiontarget = Game.getObjectById(mission.target)
        }
        cacheTargets(creep.pos);
        patient = false
        target = false
        rangedHeal = true
        if (missiontarget) { /// /We have a missiontarget
            multiTarget = creep.pos.findInRange(targetcache[creep.pos.roomName].targets, 3)
            if (creep.rangedAttack(missiontarget) !== 0) {
                creep.travelTo(missiontarget, {movingTarget: true, range: 2, brave: true, freshMatrix: true})

            } else {
                creep.heal(creep)
            }
            if (creep.pos.getRangeTo(missiontarget) < 3) {
                creep.fleeTarget(missiontarget);

            }
        } else {/// we dont have a mT
            if (creep.pos.roomName === mission.missionRoom) { /// we're in the right room
                target = creep.pos.findClosestByRange(targetcache[creep.pos.roomName].targets)
                if (targetcache[creep.pos.roomName].targets.length > 0) {
                    multiTarget = creep.pos.findInRange(targetcache[creep.pos.roomName].targets, 3)
                }
                if (creep.hasActiveBodypart(RANGED_ATTACK) && target) {
                    if (multiTarget.length > 2) {
                        creep.rangedMassAttack();
                        rangedHeal = false
                    } else {
                        if (creep.rangedAttack(target) !== 0) {
                            creep.travelTo(target, {range: 3, brave: true, freshMatrix: true})
                        }
                    }
                    if (creep.pos.getRangeTo(target) < 3) {
                        creep.fleeTarget(target);
                    }
                }
                if (creep.hasActiveBodypart(HEAL)) {
                    patient = creep.pos.findClosestByPath(FIND_MY_CREEPS, {filter: (s) => s.hitsMax > s.hits})
                    if (patient && creep.pos.inRangeTo(patient, 1)) {
                        creep.heal(patient)
                    } else {
                        if (patient && rangedHeal && creep.pos.inRangeTo(patient, 3)) {
                            creep.rangedHeal(patient)
                        }
                    }
                    if (!target && patient) {
                        creep.travelTo(patient)
                    }
                }
                if (!target && !patient) {
                    if (Object.keys(targetcache).length > 1) {
                        min = 500
                        for (roomname in targetcache) {
                            if (roomname === 'time' || !targetcache[roomname].targets || Object.keys(targetcache[roomname].targets).length < 1) {
                                continue
                            }
                            len = Game.map.findRoute(creep.pos.roomName, roomname).length
                            if (len < min) {
                                creep.memory.mission.missionRoom = roomname
                                min = len
                            }
                        }
                        if (min < 500) {
                            log.log(creep.name + ' is moving to ' + creep.memory.mission.missionRoom + ' at a distance of ' + min)
                        } else {
                            creep.moveOffRoad()
                        }
                    }
                }
            } else { //// wrong room
                target = creep.pos.findClosestByRange(targetcache[creep.pos.roomName].targets)
                if (targetcache[creep.pos.roomName].targets.length > 0) {
                    multiTarget = creep.pos.findInRange(targetcache[creep.pos.roomName].targets, 3)
                }
                if (creep.hasActiveBodypart(RANGED_ATTACK) && target) {
                    if (!multiTarget || multiTarget.length < 4) {
                        creep.rangedAttack(target)
                    } else {
                        if (multiTarget.length > 0) {
                            creep.rangedMassAttack()
                            rangedHeal = false
                        }
                    }
                }
                if (creep.hasActiveBodypart(HEAL)) {
                    patient = creep.pos.findClosestByPath(FIND_MY_CREEPS, 3, {filter: (s) => s.hitsMax > s.hits})
                    if (patient && creep.pos.inRangeTo(patient, 1)) {
                        creep.heal(patient)
                    } else {
                        if (patient && rangedHeal && creep.pos.inRangeTo(patient, 3)) {
                            creep.rangedHeal(patient)
                        }
                    }
                }
                newPos = new RoomPosition(25, 25, mission.missionRoom)
                creep.travelTo(newPos,{ignoreRoads:true,useFindRoute:true, brave:true})
            }
        }
    }
}
