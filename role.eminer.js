module.exports = {
    run: function (creep) {
        if (creep.memory.mission) {
            mission = creep.memory.mission
            if (!_.contains(Memory.CurrentlyMining, mission.target)) {
                Memory.CurrentlyMining.push(mission.target)
            }
        } else {
            creep.memory.role = 'zombie'
            log.error('miner with no mission')
            creep.say('ERROR')
        }
        if (creep.spawning) {
            return
        }
        if (!creep.memory.targetSource) {
            creep.memory.targetSource = mission.target
        }

        var repair = false;
        var source = Game.getObjectById(creep.memory.targetSource);
        var spots = {};
        var spot = {};
        link = {}
        var nearlink = {};

        if (creep.ticksToLive < 100 && creep.room.controller && !creep.room.controller.my) {
            creep.memory.role = 'repairer';
            creep.memory.working = true;
        }
        if (creep.memory.targetSource) {
            source = Game.getObjectById(creep.memory.targetSource);
        }
        if (!source) {
            memsource = Memory.sources[creep.memory.targetSource].pos
            sourcePos = new RoomPosition(memsource.x, memsource.y, memsource.roomName)
        }
        Memory.sources[creep.memory.targetSource].takenBy = creep.name
        if (!creep.memory.room) {
            creep.memory.room = Memory.sources[creep.memory.targetSource].pos.roomName
        }
        mineroom = creep.memory.room;
        if (creep.pos.roomName === mineroom) {
            if (creep.memory.assignedspot && source instanceof Source) {
                spot = Game.getObjectById(creep.memory.assignedspot);
                delete creep.memory.placing
            } else {
                spots = source.pos.findInRange(FIND_STRUCTURES, 1, {filter: (c) => c.structureType === STRUCTURE_CONTAINER});
                spot = spots[0];
                if (spots.length > 0) {
                    creep.memory.assignedspot = spot.id;
                }
            }

            if (!spot && creep.pos.isNearTo(source) && source instanceof Source) {
                creep.memory.placing = true
                creep.room.createConstructionSite(creep.pos, STRUCTURE_CONTAINER);
            } else {
                creep.travelTo(source)
            }
            if (!creep.memory.assignedspot && creep.memory.placing && source instanceof Source) {
                constructionSite = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);
                if (creep.store.energy > 20) {
                    creep.build(constructionSite)
                    let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
                    if (droppedEnergy.length >= 1 && droppedEnergy[0].resourceType == RESOURCE_ENERGY) {
                        creep.pickup(droppedEnergy[0]);
                    }
                } else {
                    creep.harvest(source)
                }
            }
            if (spot && !creep.pos.isEqualTo(spot)) {
                creep.travelTo(spot, {useFindRoute: true, ignoreCreeps: false, repath: 0.05});
            } else {
                creep.harvest(source);
            }

            if (spot && spot.hits < (spot.hitsMax - 5000)) {
                creep.repair(spot);
                creep.withdraw(spot, RESOURCE_ENERGY)
                repair = true;
            } else if (creep.room.controller && creep.room.controller.my && creep.room.controller.level > 4 && creep.store.getFreeCapacity() <= 15) {
                if (creep.memory.link) {
                    link = Game.getObjectById(creep.memory.link)
                } else {
                    nearlink = creep.pos.findInRange(FIND_STRUCTURES, 1, {filter: (c) => c.structureType === STRUCTURE_LINK});
                    if (nearlink.length > 0) {
                        creep.memory.link = nearlink[0].id;
                        link = nearlink[0]
                    } else {
                        if (Game.rooms[creep.pos.roomName].controller.level > 5 && spot && creep.pos.isEqualTo(spot.pos)) {
                            creep.pos.findLinkSpot()
                        }
                    }
                }
                if (link) {
                    transfer = creep.transfer(link, RESOURCE_ENERGY)
                    if (transfer !== 0 && transfer !== -8) {
                        delete creep.memory.link
                    }
                }
                if (!link && !repair && source.energy > 0) {
                    creep.harvest(source);
                }
            }
            if (spot && _.sum(spot.store) >= 1500 /*&& Game.rooms[creep.pos.roomName].controller && !Game.rooms[creep.pos.roomName].controller.my*/) {
                if (!creep.memory.missionTicks) {
                    creep.memory.missionTicks = 0
                }
                creep.memory.missionTicks++;
            } else {
                if (spot && _.sum(spot.store) < 500) {
                    creep.memory.missionTicks = -500
                }
            }
            if (spot && spot.id && creep.memory.missionTicks > 500) {
                console.log(creep.name + ' is requesting a hauler')
                createMission('haul', spot.id, creep.pos.roomName)
                creep.memory.missionTicks += -2000
            }
        } else {
            if (source && source.pos) {
                (creep.travelTo(source, {useFindRoute: true}))
            } else {
                let newPos = new RoomPosition(25, 25, creep.memory.room);
                creep.travelTo(newPos, {useFindRoute: true, ignoreCreeps: true});
            }
        }

        if (creep.ticksToLive < 100 && creep.memory.targetSource) {
            delete Memory.sources[creep.memory.targetSource].takenBy
            if (creep.room.controller && !creep.room.controller.my && (!creep.room.controller.reservation || creep.room.controller.reservation.username !== WHOAMI || creep.room.controller.reservation.ticksToEnd <= 1500)) {
                createMission('reserve', creep.room.controller.id, creep.pos.roomName)
                console.log(creep.name + ' is requesting a reserver')
            }
            if (Memory.Settings.Misc.highlevel > 3) {
                spawn = creep.pos.findClosestSpawn();
                buildRoad(spawn, creep, 5)
            }
        }
        let dropped = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (dropped.length >= 1 && dropped[0].amount >= 500 && !checkEmptier(creep.pos.roomName) && !creep.memory.emptier) {
            creep.pickup(dropped[0]);
            createMission('empty', dropped[0].id, creep.pos.roomName)
            creep.memory.emptier=true
        }
    }
}

RoomPosition.prototype.findLinkSpot = function () {
    let Max = 100
    anchor = Memory.rooms[this.roomName].anchor
    let splitpos = anchor.split('_');
    anchorPos = new RoomPosition(splitpos[0], splitpos[1], this.roomName)
    winnerx = 0
    for (let x = -1; x < 1; x++) {
        for (let y = -1; y < 1; y++) {
            let candidate = new RoomPosition(this.x + x, this.y + y, this.roomName)
            if (emptySpot(candidate)) {
                num = candidate.getRangeTo(anchorPos)
                if (num <= Max) {
                    winnerx = candidate.x
                    winnery = candidate.y
                }
            }
        }
    }
    if (winnerx > 0) {
        let spot = new RoomPosition(winnerx, winnery, this.roomName)
        spot.createConstructionSite(STRUCTURE_LINK)
    }
}

checkEmptier = function (room) {
    for (amission in Memory.missions.civil) {
        parts = amission.split('_');
        if (_.contains(parts, room) && _.contains(parts, 'empty') && Memory.missions.civil[amission].missionRoom === room) {
            return true
        }
    }
    return false
}
