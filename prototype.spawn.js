module.exports = function () {
    StructureSpawn.prototype.createWorker = function (energy, roleName, missionInput) {
        if (this.room.controller.level < 7 || roleName === 'pioneer') {
            energy = Math.min(energy, 1600)
        }

        switch (true) {
            case energy >= 3200:
                template = 'WWWWWWWWmmmmmmmmmmmmmmmmWWWWWWWWcccccccccccccccc' // 16xMWC
                break
            case energy >= 3000:
                template = 'WWWWWWWWmmmmmmmmmmmmmmmWWWWWWWccccccccccccccc' // 15xMWC
                break
            case energy >= 2800:
                template = 'WWWWWWWWmmmmmmmmmmmmmmWWWWWWcccccccccccccc'//14xMWC
                break
            case energy >= 2400:
                template = 'WWWWWWWWWWmmmmmmmmmmmmWWcccccccccccc'//12xMWC
                break
            case energy >= 1800:
                template = 'WWWWWWWWmmmmmmmmmWccccccccc' // 9xMWC
                break
            case energy >= 1600:
                template = 'WWWWWWWWmmmmmmmmcccccccc' // 8xMWC
                break
            case energy >= 1400:
                template = 'WWWWWWmmmmmmmWccccccc' // 7xMWC
                break
            case energy >= 1200:
                template = 'WWWWWmmmmmmWcccccc' // 6xMWC
                break
            case energy >= 1000:
                template = 'WWWWmmmmmWccccc' // 5xMWC
                break
            case energy >= 800:
                template = 'WWWmmmmWcccc' // 4xMWC
                break
            case energy >= 600:
                template = 'WWmmmWccc' // 3xMWC
                break
            case energy >= 400:
                template = 'WWmmcc' // 2xMWC
                break
            case energy >= 300:
                template = 'Wmmcc' // Starter
                break
            default:
                return -6
        }
        body = generateBodyFromString(template)
        var mem = {role: roleName, working: true, mission: missionInput};
        let nomen = roleName + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createMiner = function (energy, roleName, mem) {
        if (energy < 350) {
            return -6
        }
        if (roleName === 'eminer') {
            energy = Math.min(energy, 1900)
        }
        body = []
        let reps = (energy - 100) / 250
        if (reps > 15) {
            reps = 15
        }
        for (W = 1; W <= reps; W++) {
            body.push(WORK);
        }
        for (W = 1; W <= reps; W++) {
            body.push(WORK);
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        body.push(CARRY)
        body.push(MOVE)
        mem = {role: roleName, working: true, mission: mem, targetSource: mem.missionTarget};
        let nomen = roleName + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createDigger = function (energy, roleName, mem) {
        if (energy < 350) {
            return -6
        }
        if (roleName === 'eminer') {
            energy = Math.min(energy, 1900)
        }
        body = []
        let reps = (energy) / 250
        if (reps > 25) {
            reps = 25
        }
        for (W = 1; W <= reps; W++) {
            body.push(WORK);
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        mem = {role: roleName, working: true, mission: mem, targetSource: mem.missionTarget};
        let nomen = roleName + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createCourier = function (energy, roleName, missionInput) {
        if (energy < 150) {
            return -6
        }
        if (roleName === 'eminer') {
            energy = Math.min(energy, 1900)
        }

        body = []
        let reps = energy / 150
        if (reps > 16) {
            reps = 16
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(CARRY);
        }
        var mem = {role: roleName, working: true, mission: missionInput};
        let nomen = roleName + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createHauler = function (energy, roleName, missionInput) {
        if (energy < 450) {
            return -6
        }
        if (this.room.controller.level < 5) {
            energy = Math.min(energy, 1600)
        }
        body = [WORK, MOVE]
        let reps = (energy - 150) / 150
        if (reps > 16) {
            reps = 16
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(CARRY);
        }
        var mem = {role: roleName, working: true, mission: missionInput};
        let nomen = roleName + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.spawnScout = function (scoutroom) {
        if (this.room.energyAvailable < 50) {
            return -6
        }
        body = [MOVE]
        var mem = {role: 'scout', scoutroom: scoutroom};
        let nomen = 'scout' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createClaimer = function (energy, roleName, missionInput) {
        if (energy < 800) {
            return -6
        }
        if (energy < 850) {
            body = [MOVE, MOVE, MOVE, MOVE, CLAIM]
        } else {
            body = [MOVE, MOVE, MOVE, MOVE, MOVE, CLAIM]
        }
        var mem = {role: 'claimer', mission: missionInput};
        let nomen = 'claimer' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createReserver = function (energy, roleName, missionInput) {
        if (energy < 1300) {
            return -6
        }
        body = []
        let reps = energy / 650
        if (reps > 5 && this.room.controller.level < 8) {
            reps = 5
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(CLAIM);
        }
        var mem = {role: 'reserver', mission: missionInput};
        let nomen = 'reserver' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createSoldier = function (energy, roleName, missionInput) {
        if (energy < 180) {
            return -6
        }
        limit = this.room.controller.level * 1000
        energy = Math.min(energy, limit)
        body = []
        let reps = energy / 130
        if (reps > 20) {
            reps = 20
        }
        for (m = 1; m <= reps; m++) {
            body.push(ATTACK);
        }
        for (c = 1; c <= reps; c++) {
            body.push(MOVE);
        }
        var mem = {role: roleName, mission: missionInput};
        let nomen = 'soldier' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createArcher = function (energy, roleName, missionInput) {
        if (energy < 200) {
            return -6
        }
        limit = this.room.controller.level * 1000
        energy = Math.min(energy, limit)
        body = []
        let reps = energy / 200
        if (reps > 10) {
            reps = 10
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(RANGED_ATTACK);
        }
        var mem = {role: roleName, mission: missionInput};
        let nomen = 'archer' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createSentry = function (energy, roleName, missionInput) {
        if (energy < 500) {
            return -6
        }
        body = []
        let reps = energy / 500
        if (reps > 10) {
            reps = 10
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(RANGED_ATTACK);
        }
        for (m = 1; m <= reps; m++) {
            body.push(HEAL);
        }
        var mem = {role: roleName, mission: missionInput};
        let nomen = roleName + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createMedic = function (energy, roleName, missionInput) {
        if (energy < 550) {
            return -6
        }
        body = []
        let reps = energy / 360
        if (reps > 12) {
            reps = 12
        }
        for (m = 1; m <= reps; m++) {
            body.push(TOUGH);
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(MOVE);
        }
        for (m = 1; m <= reps; m++) {
            body.push(HEAL);
        }
        var mem = {role: roleName, mission: missionInput};
        let nomen = 'medic' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createHealer = function (energy, roleName, missionInput) {
        if (energy < 300) {
            return -6
        }
        body = []
        let reps = energy / 300
        if (reps > 10) {
            reps = 10
        }
        for (c = 1; c <= reps; c++) {
            body.push(MOVE);
        }
        for (m = 1; m <= reps; m++) {
            body.push(HEAL);
        }
        var mem = {role: roleName, mission: missionInput};
        let nomen = 'healer' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    StructureSpawn.prototype.createTank = function (energy, roleName, missionInput) {
        if (energy < 900) {
            return -6
        }
        body = []
        let reps = energy / 190
        if (reps > 12) {
            reps = 12
        }
        for (m = 1; m <= reps; m++) {
            body.push(TOUGH);
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        for (c = 1; c <= reps; c++) {
            body.push(ATTACK);
        }
        for (m = 1; m <= reps; m++) {
            body.push(MOVE);
        }
        var mem = {role: roleName, mission: missionInput};
        let nomen = 'tank' + '_' + Game.time % 100 + Memory.counters.CreepCounter;
        Memory.counters.CreepCounter++
        return this.spawnCreep(body, nomen, {memory: mem});
    }
    generateBodyFromString = function (template) {
        const body = {m: MOVE, c: CARRY, W: WORK, a: ATTACK, r: RANGED_ATTACK, h: HEAL, t: TOUGH, k: CLAIM};
        const parts = [];
        for (var i = 0; i < template.length; i++) {
            const c = template.charAt(i);
            parts.push(body[c]);
        }
        return parts
    }
    StructureSpawn.prototype.missionSpawn = function (energy) {
////////urgent Civil Missions
        for (thatmission in Memory.missions.civil) {
            thismission = Memory.missions.civil[thatmission];
            if (thismission.spawned || (thismission.assignedSpawnRoom && !_.contains(thismission.assignedSpawnRoom, this.pos.roomName))) {
                continue
            }
            mem = {}
            mem.mission = thismission.name
            mem.target = thismission.missionTarget
            mem.missionRoom = thismission.missionRoom
            switch (thismission.missionType) {
                case 'haul':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 5) {
                        name = this.createHauler(energy, 'hauler', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a hauler for mission ' + thismission.name)
                    break;
                case 'dismantle':
                    name = this.createMiner(energy, 'wrecker', mem)
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a dismantler for mission ' + thismission.name)
                    break;
                case 'claim':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 5) {
                        name = this.createClaimer(energy, 'claimer', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a claimer for mission ' + thismission.name + ' to claim room ' + thismission.missionRoom)
                    break;
                case 'mine':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 3 || (Memory.rooms[thismission.missionRoom] && Memory.rooms[thismission.missionRoom].my && Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 5 )) {
                        name = this.createMiner(energy, 'eminer', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a miner for mission ' + thismission.name + ' for room ' + thismission.missionRoom)
                    break;

            }
            if (name === 0) {
                thismission.spawned = true
                Memory.counters.civilMissionCounter++
                return true
            }

        }
////////Military Missions
        for (thatmission in Memory.missions.military) {
            thismission = Memory.missions.military[thatmission];
            if (thismission.spawned === 0 || (thismission.assignedSpawnRoom && _.contains(thismission.assignedSpawnRoom, this.pos.roomName)) || Game.map.getRoomLinearDistance(this.pos.roomName, thismission.missionRoom) > 6) {
                continue
            }
            mem = {}
            mem.mission = thismission.name
            mem.target = thismission.missionTarget
            mem.missionType = thismission.missionType
            mem.missionRoom = thismission.missionRoom
            switch (thismission.creepTypes[0]) {
                case 'tank':
                    if (Memory.rooms[thisroomName].level > 4) {
                        name = this.createTank(energy, 'tank', mem)
                    } else {
                        continue
                    }
                    break;
                case 'soldier':
                    if (Memory.rooms[thisroomName].level > 2) {
                        name = this.createSoldier(energy, 'soldier', mem)
                    } else {
                        continue
                    }
                    break;
                case 'ranger':
                    if (Memory.rooms[thisroomName].level > 3) {
                        name = this.createSentry(energy, 'ranger', mem)
                    } else {
                        continue
                    }
                    break;
                case 'archer':
                    if (Memory.rooms[thisroomName].level > 3) {
                        name = this.createArcher(energy, 'archer', mem)
                    } else {
                        continue
                    }
                    break;
                case 'healer':
                    if (Memory.rooms[thisroomName].level > 2) {
                        name = this.createHealer(energy, 'healer', mem)
                    } else {
                        continue
                    }
                    break;
                case 'medic':
                    if (Memory.rooms[thisroomName].level > 4) {
                        name = this.createMedic(energy, 'medic', mem)
                    } else {
                        continue
                    }
                    break;
                case 'sentry':
                    if (Memory.rooms[thisroomName].level > 3) {
                        name = this.createSentry(energy, 'sentry', mem)
                    } else {
                        continue
                    }
                    break;
                case 'wrecker':
                    if (Memory.rooms[thisroomName].level > 4) {
                        name = this.createDigger(energy, 'wrecker', mem)
                    } else {
                        continue
                    }
                    break;
            }
            if (name === 0) {
                log.notify(thisroomName + ' is spawning a ' + thismission.creepTypes[0] + ' for mission ' + thismission.name)
                thismission.creepTypes.shift()
                thismission.spawned = thismission.creepTypes.length
                //console.log(thismission.name + ' has ' + thismission.spawned)
                return;
            }

        }
////////Nonurgent Civil Missions
        for (thatmission in Memory.missions.civil) {
            thismission = Memory.missions.civil[thatmission];
            if (thismission.spawned || (thismission.assignedSpawnRoom && !_.contains(thismission.assignedSpawnRoom, this.pos.roomName))) {
                continue
            }
            mem = {}
            mem.mission = thismission.name
            mem.target = thismission.missionTarget
            mem.missionRoom = thismission.missionRoom
            switch (thismission.missionType) {
                case 'pioneer':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 7) {
                        name = this.createWorker(energy, 'pioneer', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a pioneer for mission ' + thismission.name)
                    break;
                case 'scout':
                    name = this.spawnScout(mem.missionRoom)
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a scout for mission ' + thismission.name)
                    break;
                case 'loot':
                    name = this.createCourier(energy, 'looter', mem)
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a courier for mission ' + thismission.name)
                    break;
                case 'rob':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 7 && Memory.rooms[thisroomName].level > 3) {
                        name = this.createCourier(energy, 'thief', mem)
                    }
                    if (name && name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a thief for mission ' + thismission.name)
                    break;
                case 'reserve':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 8) {
                        name = this.createReserver(energy, 'reserver', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a reserver for mission ' + thismission.name)
                    break;
                case 'transport':
                    name = this.createHauler(energy, 'trucker', mem)
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a hauler for mission ' + thismission.name)
                    break;
                case 'empty':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 4) {
                        name = this.createCourier(energy, 'emptier', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a courier for mission ' + thismission.name)
                    break;
                case 'mineral':
                    if (Game.map.findRoute(this.pos.roomName, thismission.missionRoom).length < 2 && Memory.rooms[thisroomName].level > 3) {
                        name = this.createDigger(energy, 'digger', mem)
                    } else {
                        continue
                    }
                    if (name !== 0) {
                        continue
                    }
                    log.notify(thisroomName + ' is spawning a digger for mission ' + thismission.name)
                    break;
            }
            if (name === 0) {
                thismission.spawned = true
                Memory.counters.civilMissionCounter++
                return true
            }
        }
    }
}
