/*
 Core stamp
[STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
    [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_SPAWN, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
    [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_SPAWN, STRUCTURE_ROAD, STRUCTURE_SPAWN, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
    [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_OBSERVER, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
    [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
    [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_STORAGE, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
    [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_LINK, STRUCTURE_ROAD, STRUCTURE_TERMINAL, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
    [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_NUKER, STRUCTURE_POWER_SPAWN, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
    [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD]];

    Flower Stamp
                [[STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_ROAD]];

    */


global.buildRoomCore = function (room, stamp = 'core') {
    if (!Memory.rooms[room].anchor) {
        return log.err('No anchor for ' + room)
    }
    if (Game.rooms[room].controller.level < 3) {
        buildFirstSpawn(room)
        return
    }
    anchor = Memory.rooms[room].anchor
    if (!Game.rooms[room] && Memory.Settings.Logging.StampBuilderReports) {
        log.warn('No vision on room ' + room + ' so build was canceled')
        return
    }
    let splitpos = anchor.split('_');
    let anchorPos = new RoomPosition(Number(splitpos[0]), Number(splitpos[1]), room)
    switch (stamp) {
        case 'core':
            offset = 4;
            stamparray = Memory.stamps.core
            break;
    }
    let nums = 0;
    if (!Memory.rooms[room].linkpos) {
        findControllerLinkSpot(room)
    }
    for (let y2 = 0 - offset; y2 < 1 + offset; y2++) {
        for (let x2 = 0 - offset; x2 < 1 + offset; x2++) {
            if (Object.keys(Game.constructionSites).length <= Memory.Settings.Misc.AutoConstructionSitesLimit) {
                let workingPos = new RoomPosition((anchorPos.x + x2), (anchorPos.y + y2), room);
                if (emptySpot(workingPos)) {
                    p = stamparray[y2 + offset][x2 + offset]
                    let site = workingPos.createConstructionSite(p)
                    if (site === 0) {
                        nums++
                    } else {
                        if (Memory.Settings.Logging.StampBuilderReports) {
                            log.err('Builder failed to place a construction site at ' + workingPos.x + ',' + workingPos.y, ' in room ' + room)
                        }
                    }
                }
            } else {
                if (Memory.Settings.Logging.StampBuilderReports) {
                    log.warn('Ran out of free construction sites while placing buildings in room ' + room + ' after placing ' + nums + ' sites')
                }
                return
            }
        }
    }
    if (nums > 0 && Memory.Settings.Logging.StampBuilderReports) {
        log.notification('Placed ' + nums + 'constructions sites successfully in room ' + room)
    }
    if (Memory.rooms[room].linkpos.controller && Memory.rooms[room].level > 4) {
        link = Memory.rooms[room].linkpos.controller
        let linkpos = link.split('_');
        let conpos = new RoomPosition(linkpos[0], linkpos[1], room)
        conpos.createConstructionSite(STRUCTURE_LINK)
    }
}

global.buildFirstSpawn = function (room) {
    if (!Memory.rooms[room].anchor) {
        return log.err('No anchor for ' + room)
    }
    let r = Memory.rooms[room].anchor;
    let splitpos = r.split('_');
    let anchorpos = new RoomPosition(splitpos[0], splitpos[1], room);
    let spawn1 = new RoomPosition(anchorpos.x, anchorpos.y - 3, room);
    let ex1 = new RoomPosition(anchorpos.x - 2, anchorpos.y - 3, room);
    let ex2 = new RoomPosition(anchorpos.x + 2, anchorpos.y - 3, room);
    let ex3 = new RoomPosition(anchorpos.x + 3, anchorpos.y - 4, room);
    let ex4 = new RoomPosition(anchorpos.x - 3, anchorpos.y - 4, room);
    let ex5 = new RoomPosition(anchorpos.x - 3, anchorpos.y - 2, room);
    let anchor = new RoomPosition(anchorpos.x, anchorpos.y, room);
    if (Object.keys(Game.spawns).length > 7) {
        spawn1.createConstructionSite(STRUCTURE_RAMPART);
    } else {
        spawn1.createConstructionSite(STRUCTURE_SPAWN)
    }
    ex1.createConstructionSite(STRUCTURE_EXTENSION);
    ex2.createConstructionSite(STRUCTURE_EXTENSION);
    ex3.createConstructionSite(STRUCTURE_EXTENSION);
    ex4.createConstructionSite(STRUCTURE_EXTENSION);
    ex5.createConstructionSite(STRUCTURE_EXTENSION);
    anchor.createConstructionSite(STRUCTURE_CONTAINER);
    anchor.createConstructionSite(STRUCTURE_ROAD);
    return log.notify('Placed early buildings for room ' + room)
}

global.buildExtensions = function (room, which) {
    if (Memory.Settings.Logging.StampBuilderReports) {
        log.notify('Trying to build the ' + which + ' set of extensions for room:' + room)
    }
    if (!Memory.rooms[room].anchor) {
        return log.err('No anchor for ' + room)
    }
    if (Game.rooms[room].controller.level < 3) {
        buildFirstSpawn(room)
        return
    }
    anchor = Memory.rooms[room].extensions.first
    if (!Game.rooms[room] && Memory.Settings.Logging.StampBuilderReports) {
        log.warn('No vision on room ' + room + ' so build was canceled')
        return
    }
    offset = 2
    switch (which) {
        case 'first':
            anchor = Memory.rooms[room].extensions.first
            break;
        case 'second':
            anchor = Memory.rooms[room].extensions.second
            break;
        case 'third':
            anchor = Memory.rooms[room].extensions.third
            break;
        case 'fourth':
            anchor = Memory.rooms[room].extensions.fourth
            break;
    }
    let splitpos = anchor.split('_');
    let anchorPos = new RoomPosition(Number(splitpos[0]), Number(splitpos[1]), room)
    stamparray = Memory.stamps.flower
    var nums = 0;
    for (let y2 = 0 - offset; y2 < 1 + offset; y2++) {
        for (let x2 = 0 - offset; x2 < 1 + offset; x2++) {
            if (Object.keys(Game.constructionSites).length <= Memory.Settings.Misc.AutoConstructionSitesLimit) {
                let workingPos = new RoomPosition((anchorPos.x + x2), (anchorPos.y + y2), room);
                if (emptySpot(workingPos)) {
                    p = stamparray[y2 + offset][x2 + offset]
                    let site = workingPos.createConstructionSite(p)
                    if (site == 0) {
                        nums++
                    } else {
                        there = workingPos.lookFor(LOOK_STRUCTURES)
                        if (there[0] != p && there[1] != p) {
                            //log.err('Builder failed to place a construction site at ' + workingPos.x + ',' + workingPos.y, ' in room ' + room)
                        }
                    }
                }
            } else {
                //log.warn('Ran out of free construction sites while placing buildings in room ' + room + ' after placing ' + nums + ' sites')
                return
            }
        }
    }
    if (nums > 0 && Memory.Settings.Logging.StampBuilderReports) {
        log.notification('Placed ' + nums + 'constructions sites successfully in room ' + room)
    }
}
global.emptySpot = function (positon) {
    items = Game.rooms[positon.roomName].lookAt(positon)
    if (items.length === 0 && items[0].terrain === 'wall') {
        return false
    }
    for (item = 0; item < items.length; item++) {
        let thisitem = items[item]
        if (thisitem.type === 'terrain' && thisitem.terrain === 'wall') {
            return false
        }
        if (thisitem.type === 'structure') {
            if (thisitem.structure.structureType !== 'road') {
                return false
            }
        }
    }
    return true;
};
findControllerLinkSpot = function (room) {
    let Max = 0
    anchor = Memory.rooms[room].anchor
    let splitpos = anchor.split('_');
    anchorpos = new RoomPosition(splitpos[0], splitpos[1], room)
    for (let x = 0; x < 50; x++) {
        for (let y = 0; y < 50; y++) {
            let candidate = new RoomPosition(x, y, room)
            if (candidate.getRangeTo(Game.rooms[room].controller) === 3 && candidate.getRangeTo(anchorpos) > 5) {
                num = candidate.freePositionsAround().length
                if (num >= Max) {
                    winnerX = candidate.x
                    winnerY = candidate.y
                    Max = num
                }
            }
        }
    }
    if (winnerX) {
        Memory.rooms[room].linkpos = {}
        Memory.rooms[room].linkpos.controller = winnerX + "_" + winnerY
    }
}


global.clearWrongStructures = function (room) {
    anchor = Memory.rooms[room].anchor
    offset = 4;
    stamparray = Memory.stamps.core
    let splitpos = anchor.split('_');
    let anchorPos = new RoomPosition(Number(splitpos[0]), Number(splitpos[1]), room)
    for (let y2 = 0 - offset; y2 < 1 + offset; y2++) {
        for (let x2 = 0 - offset; x2 < 1 + offset; x2++) {
            let workingPos = new RoomPosition((anchorPos.x + x2), (anchorPos.y + y2), room);
            p = stamparray[y2 + offset][x2 + offset]
            correctSpot(workingPos, p)
        }
    }
}

global.correctSpot = function (positon, p) {
    items = Game.rooms[positon.roomName].lookAt(positon)
    if (items.length === 0 && items[0].terrain === 'wall') {
        return false
    }
    for (item = 0; item < items.length; item++) {
        let thisitem = items[item]
        if (thisitem.type === 'terrain' && thisitem.terrain === 'wall') {
            return false
        }
        if (thisitem.type === 'structure') {
            if (thisitem.structure.structureType !== 'road' && thisitem.structure.structureType !== STRUCTURE_EXTENSION && thisitem.structure.structureType !== STRUCTURE_SPAWN && thisitem.structure.structureType !== p) {
                console.log('boom')
                Game.getObjectById(thisitem.structure.id).destroy()
            }
        }
    }

}
