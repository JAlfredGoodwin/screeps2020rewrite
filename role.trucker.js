/*  Civil Mission Structure
 *   mission.name = thismission
 *   mission.target = mTarget; resource to be grabbed
 *   mission.missionRoom = mRoom; room to deliver to
 *   mission.assignedSpawnRoom = homeRoom; room to grab from
 */
module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        } else {
            creep.memory.role = 'zombie'
        }
        if (!creep.memory.mission.assignedSpawnRoom){
            creep.memory.mission.assignedSpawnRoom = creep.pos.roomName
        }
        delete Memory.missions.civil[mission.mission]
        if (_.sum(creep.store) === 0) {
            creep.memory.ticks = undefined;
            creep.memory.working = false;
            delete creep.memory.homeRoom
        }
        if (creep.memory.working === false && creep.store.getFreeCapacity() === 0) {
            creep.memory.working = true;
            creep.memory.ticks = undefined;

        }
        if (creep.store.energy > 0) {
            let roadspot = creep.pos.lookFor(LOOK_STRUCTURES, {filter: (c) => c.structureType === STRUCTURE_ROAD});
            if (roadspot[0] && (roadspot[0].hits + 200) < roadspot[0].hitsMax) {
                creep.repair(roadspot[0]);
            }
        }
        if (creep.pos.roomName !== mission.missionRoom && creep.memory.working === true) {
            if (Memory.rooms[mission.missionRoom] && Memory.rooms[mission.missionRoom].anchor) {
                anchor = Memory.rooms[mission.missionRoom].anchor
                let splitpos = anchor.split('_');
                let anchorPos = new RoomPosition(Number(splitpos[0]), Number(splitpos[1]), mission.missionRoom)
                creep.travelTo(anchorPos)
            }
            return
        }
        if (creep.pos.roomName !== mission.assignedSpawnRoom && creep.memory.working === false) {

            if (Memory.rooms[mission.assignedSpawnRoom] && Memory.rooms[mission.assignedSpawnRoom].anchor) {
                anchor = Memory.rooms[mission.assignedSpawnRoom].anchor
                let splitpos = anchor.split('_');
                let anchorPos = new RoomPosition(Number(splitpos[0]), Number(splitpos[1]), mission.assignedSpawnRoom)
                creep.travelTo(anchorPos)
            }
            return
        }
        if (creep.pos.roomName === mission.assignedSpawnRoom) {
            creep.grabExcessResource(mission.target)
            return
        }
        if (creep.pos.roomName === mission.missionRoom) {
            require('role.delivery').run(creep);
        }
    }
}
