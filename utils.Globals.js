global.WHOAMI = _.find(Game.structures).owner.username;
global.gTA = function (x, y, rN) {
    if (x instanceof RoomPosition) {
        return gTA(x.x, x.y, x.roomName);
    }
    if (!global.terrain) {
        global.terrain = {}
    }
    if (!global.terrain[rN]) {
        global.terrain[rN] = Game.map.getRoomTerrain(rN);
    }

    return global.terrain[rN].get(x, y);
};


global.ensureNumber = function ensureNumber(val) {
    return val || 0;
}

global.roomNameToXY = function (name) {
    name = name.toUpperCase();
    var match = name.match(/^(\w)(\d+)(\w)(\d+)$/);
    if (!match) {
        return [undefined, undefined];
    }
    var [, hor, x, ver, y] = match;
    if (hor == 'W') {
        x = -x - 1;
    } else {
        x = +x;
    }
    if (ver == 'N') {
        y = -y - 1;
    } else {
        y = +y;
    }
    return [x, y];
};

global.getRoomManhattanDistance = function (room1, room2, continuous = false) {
    var [x1, y1] = roomNameToXY(room1);
    var [x2, y2] = roomNameToXY(room2);
    var dx = Math.abs(x2 - x1);
    var dy = Math.abs(y2 - y1);
    if (continuous) {
        var worldSize = Game.map.getWorldSize();
        dx = Math.min(worldSize - dx, dx);
        dy = Math.min(worldSize - dy, dy);
    }
    return dx + dy;
};

global.isAllied = function (target) {
    if (!Memory.targetRecognition)
        Memory.targetRecognition = {allies: ['SemperRabbit', 'NihilRex', 'WarInternal'], neutral: ['Source Keeper']};
    if (target instanceof ConstructionSite || target instanceof Creep || target instanceof OwnedStructure) {
        return Memory.targetRecognition.allies.indexOf(target.owner.username) != -1;
    }
    return false;
};
global.isNeutral = function (target) {
    if (!Memory.targetRecognition)
        Memory.targetRecognition = {allies: ['SemperRabbit', 'NihilRex', 'WarInternal'], neutral: ['Source Keeper']};
    if (target instanceof ConstructionSite || target instanceof Creep || target instanceof OwnedStructure) {
        return Memory.targetRecognition.neutral.indexOf(target.owner.username) != -1;
    }
    return false;
};
global.countNukersRaw = (roomName) => _.countBy(Game.structures, s => s.structureType === STRUCTURE_NUKER && Game.map.getRoomLinearDistance(s.pos.roomName, roomName) <= NUKE_RANGE);
global.findNuker = (roomName) => _.find(Game.structures, s => s.structureType === STRUCTURE_NUKER && Game.map.getRoomLinearDistance(s.pos.roomName, roomName) <= NUKE_RANGE);
global.findArmedNuker = (roomName) => _.find(Game.structures, s => s.structureType === STRUCTURE_NUKER && s.cooldown == 0 && s.ghodium == s.ghodiumCapacity &&
    Game.map.getRoomLinearDistance(s.pos.roomName, roomName) <= NUKE_RANGE && !s.launchedNuke);
global.terminal =
    function (roomName) {
        let room = Game.rooms[roomName];
        if (room == undefined) return {
            send: function () {
                console.log("Room Undefined")
            }
        };

        let terminal = room.terminal;
        if (terminal == undefined) return {
            send: function () {
                console.log("Terminal Undefined")
            }
        };

        return terminal;
    };
if (Game.profiler) {
    global.p = Game.profiler;
    global.o = Game.profiler.output;
    global.rb = function () {
        Game.profiler.reset();
        Game.profiler.background();
    }
}
// credit due for next line to warinternal
global.ex = (x) => JSON.stringify(x, null, 2);

global.g = {
    c: global.gc = Game.creeps,
    f: global.gf = Game.flags,
    s: global.gs = Game.spawns,
    r: global.gr = Game.rooms,
    m: global.gm = Game.market,
};

global.goid = Game.getObjectById;

global.r = function r(rName) {
    return gr[rName];
};

global.total = function () {
    return Object.keys(Game.creeps).length;
};

global.LOGGING_ENABLED = true;
global.logging = function (bool) {
    global.LOGGING_ENABLED = bool
};
global.log = {
    log: function log(arg) {
        if (global.LOGGING_ENABLED) return console.log(arg)
    },
    warn: function warn(arg) {
        if (global.LOGGING_ENABLED) return console.log('<span style=color:#FFBF3F>' + arg + '</span>');
    },
    notification: function notify(arg) {
        if (global.LOGGING_ENABLED) return console.log('<span style=color:#0fff0f>' + arg + '</span>');
    },
    notify: function notify(arg) {
        if (global.LOGGING_ENABLED) return console.log('<span style=color:#0fff0f>' + arg + '</span>');
    },
    err: function err(arg) {
        if (global.LOGGING_ENABLED) return console.log('<span style=color:#D18F98>' + arg + '</span>');
    },
    error: function error(arg) {
        if (global.LOGGING_ENABLED) return console.log('<span style=color:#D18F98>' + arg + '</span>');
    },
};
global.role = function (t) {
    for (var r2 in Game.rooms) {
        var r = Game.rooms[r2];
        console.log(r.name + ' ' + r.find(FIND_MY_CREEPS, {filter: (c) => c.memory.role == t}))
    }
};
global.listMineRooms = function () {
    let t = _.sortBy(Memory.MineRooms);
    console.log('Currently Mining Rooms ' + t)
};
global.errName = function (err) {
    switch (err) {
        case ERR_NOT_OWNER:
            return 'ERR_NOT_OWNER';
        case ERR_NO_PATH:
            return 'ERR_NO_PATH';
        case ERR_NAME_EXISTS:
            return 'ERR_NAME_EXISTS';
        case ERR_BUSY:
            return 'ERR_BUSY';
        case ERR_NOT_FOUND:
            return 'ERR_NOT_FOUND';
        case ERR_NOT_ENOUGH_RESOURCES:
            return 'ERR_NOT_ENOUGH_ENERGY/ERR_NOT_ENOUGH_RESOURCES/ERR_NOT_ENOUGH_EXTENSIONS';
        case ERR_INVALID_TARGET:
            return 'ERR_INVALID_TARGET';
        case ERR_FULL:
            return 'ERR_FULL';
        case ERR_NOT_IN_RANGE:
            return 'ERR_NOT_IN_RANGE';
        case ERR_INVALID_ARGS:
            return 'ERR_INVALID_ARGS';
        case ERR_TIRED:
            return 'ERR_TIRED';
        case ERR_NO_BODYPART:
            return 'ERR_NO_BODYPART';
        case ERR_RCL_NOT_ENOUGH:
            return 'ERR_RCL_NOT_ENOUGH';
        case ERR_GCL_NOT_ENOUGH:
            return 'ERR_GCL_NOT_ENOUGH';
    }
    return '';
};

function getMinerals() {
    let minerals = {};
    let rooms = Game.rooms;
    for (let room in rooms) {
        if (!Game.rooms[room].controller || !Game.rooms[room].controller.my) {
            continue;
        }
        let mineral = Game.rooms[room].getMineral();
        if (mineral instanceof Mineral) {
            if (minerals[mineral.mineralType] !== undefined) {
                minerals[mineral.mineralType]++;
            } else {
                minerals[mineral.mineralType] = 1;
            }
        }
    }
    return minerals;
}

exports.getMinerals = getMinerals;

global.mineralreport = function () {
    let minerals = getMinerals();
    let output = "Minerals currently controlled: <br/>";
    for (let m of Object.keys(minerals)) {
        output += minerals[m] + " " + m + " | ";
    }
    return output.slice(0, -2);
};
global.removeRoadsNear = function (input, range = 1) {
    if (!(input instanceof RoomPosition)) {
        var inputpos = input.pos;
    } else {
        var inputpos = input
    }
    let array = inputpos.findInRange(FIND_STRUCTURES, range, {filter: s => s.structureType === STRUCTURE_ROAD});
    array.forEach(s => s.destroy())
};
global.removeEnemyNear = function (input, range = 1) {
    if (!(input instanceof RoomPosition)) {
        var inputpos = input.pos;
    } else {
        var inputpos = input;
    }
    let array = inputpos.findInRange(FIND_STRUCTURES, range, {
        filter: s => (
            !s.my && (
                (s.store && _.sum(s.store) === 0)
                || s.structureType === STRUCTURE_RAMPART
            )
        )
    });
    array.forEach(s => s.destroy())
};
global.REVERSE_DIR = {
    [TOP]: BOTTOM,
    [TOP_RIGHT]: BOTTOM_LEFT,
    [RIGHT]: LEFT,
    [BOTTOM_RIGHT]: TOP_LEFT,
    [BOTTOM]: TOP,
    [BOTTOM_LEFT]: TOP_RIGHT,
    [LEFT]: RIGHT,
    [TOP_LEFT]: BOTTOM_RIGHT
};
global.updateMarketPrices = function () {
    Memory.marketBuy = {};
    Memory.marketSell = {};
    let myActiveOrderIds = _.map(getMyActiveOrders(), function (o) {
        return o.id;
    });
    for (let o of Game.market.getAllOrders()) {
        if (o.amount >= 10000 && !_.contains(myActiveOrderIds, o.id)) {
            if (o.type === ORDER_SELL && (Memory.marketSell[o.resourceType] === undefined || Memory.marketSell[o.resourceType] > o.price)) {
                Memory.marketSell[o.resourceType] = o.price;
            } else if (o.type === ORDER_BUY && (Memory.marketBuy[o.resourceType] === undefined || Memory.marketBuy[o.resourceType] < o.price)) {
                Memory.marketBuy[o.resourceType] = o.price;
            }
        }
    }
};
global.getMyActiveOrders = function () {
    return _.filter(Game.market.orders, function (o) {
        return o.active;
    });
};
global.breakdownRoomNames = function (roomName) {
    let [name, ew, ewVal, ns, nsVal] = roomName.match(/^([W|E])([0-9]+)([N|S])([0-9]+)$/)
    return {name, ew, ewVal, ns, nsVal}
};
global.findNewRoom = function (roomName) {
    ListRooms = Game.map.describeExits(roomName);
    for (list in ListRooms) {
        if (Memory.rooms[ListRooms[list]] && Memory.rooms[ListRooms[list]].updateTime < Game.time - 500 && !_.contains(global.scouting, ListRooms[list])) {
            return ListRooms[list]
        }
        if (!Memory.rooms[ListRooms[list]] && !_.contains(global.scouting, ListRooms[list])) {
            return ListRooms[list]
        }
    }
    return undefined
};
global.findNewRooms = function (pos) {
    for (room in Game.rooms) {
        let newroom = findNewRoom(room)
        if (newroom) {
            return newroom
        }
    }
}
global.addMineRoom = function (mineroom) {
    if (Memory.rooms[mineroom]) {
        Memory.rooms[mineroom].extracting = true
        evaluateRoomForMining(mineroom)
    }
}
global.cleanupOrders = function () {
    for (let OrderId in Game.market.orders) {
        Memory.Test = OrderId
        let Order = Game.market.getOrderById(OrderId);
        if (!Order || Order === null || Order.amount === 0) {
            Game.market.cancelOrder(OrderId)
        }
    }
}
global.changeOrderPrice = function (Type, resource, Price) {
    let count = 0
    let result = -1
    for (let OrderId in Game.market.orders) {

        let Order = Game.market.getOrderById(OrderId);
        if (Order && Order.type == Type && Order.resourceType == resource && Order.price != Price) {
            result = Game.market.changeOrderPrice(OrderId, Price)
        }
        //console.log(OrderId + ' ' + result)
        if (result === 0) {
            count = count + 1
        }

    }
    console.log('Changed ' + count + ' ' + Type + ' orders for ' + resource + ' to price of ' + Price)
}
global.goRenew = function (creep) {
    let nearSpawner = creep.pos.findClosestSpawn();
    if (nearSpawner.renewCreep(creep) != 0) {
        if (!creep.pos.isNearTo(nearSpawner)) {
            creep.travelTo(nearSpawner)
        }
    }
};
global.isHighway = function (roomName) {
    let parsed = /^[WE]([0-9]+)[NS]([0-9]+)$/.exec(roomName);
    return ((parsed[1] % 10 === 0) || (parsed[2] % 10 === 0));
};
global.cacheTargets = function (pos) {
    if (!global.targetcache || global.targetcache.time !== Game.time) {
        global.targetcache = {}
        global.targetcache.time = Game.time
    }
    if (!global.targetcache[pos.roomName]) {
        global.targetcache[pos.roomName] = {}
    }
    strucs = Game.rooms[pos.roomName].find(FIND_HOSTILE_STRUCTURES, {
        filter: (s) => s.structureType !== STRUCTURE_CONTROLLER
            && s.structureType !== STRUCTURE_STORAGE
            && s.structureType !== STRUCTURE_EXTRACTOR
            && s.structureType !== STRUCTURE_POWER_BANK
            && s.structureType !== STRUCTURE_TERMINAL
            && s.structureType !== STRUCTURE_KEEPER_LAIR
            && !s.my
            && !isNeutral(s)
    })

    creeps = Game.rooms[pos.roomName].find(FIND_HOSTILE_CREEPS, {filter: (s) => !isNeutral(s)})
    sites = Game.rooms[pos.roomName].find(FIND_HOSTILE_CONSTRUCTION_SITES)
    wounded = Game.rooms[pos.roomName].find(FIND_MY_CREEPS, {filter: (s) => s.hits < s.hitsMax})

    if (creeps != null && strucs == null && sites == null) {
        global.targetcache[pos.roomName].targets = [...creeps]
    }
    if (creeps != null && strucs != null && sites == null) {
        global.targetcache[pos.roomName].targets = [...strucs, ...creeps]
    }
    if (creeps == null && strucs != null && sites == null) {
        global.targetcache[pos.roomName].targets = [...strucs]
    }
    if (creeps != null && strucs != null && sites != null) {
        global.targetcache[pos.roomName].targets = [...sites, ...strucs, ...creeps]
    }
    if (creeps == null && strucs != null && sites != null) {
        global.targetcache[pos.roomName].targets = [...sites, ...strucs]
    }
    if (creeps != null && strucs == null && sites != null) {
        global.targetcache[pos.roomName].targets = [...sites, ...creeps]
    }
    if (creeps == null && strucs == null && sites != null) {
        global.targetcache[pos.roomName].targets = [...sites]
    }
    if (wounded.length > 0) {
        global.targetcache[pos.roomName].wounded = wounded
    }
}
global.getBoosted = function (creep, role) {
    if (creep.isBoosted()) {
        creep.memory.boosted = true
    }
    let compounds = [];
    switch (role) {
        case 'upgrade':
            compounds.push(RESOURCE_GHODIUM_HYDRIDE, RESOURCE_GHODIUM_ACID, RESOURCE_CATALYZED_GHODIUM_ACID);
            break;
        case 'build':
            compounds.push(RESOURCE_LEMERGIUM_HYDRIDE, RESOURCE_LEMERGIUM_ACID, RESOURCE_CATALYZED_LEMERGIUM_ACID);
            break;
        case 'dismantle':
            compounds.push(RESOURCE_ZYNTHIUM_HYDRIDE, RESOURCE_ZYNTHIUM_ACID, RESOURCE_CATALYZED_ZYNTHIUM_ACID);
            break;
        case 'heal':
            compounds.push(RESOURCE_LEMERGIUM_ALKALIDE, RESOURCE_LEMERGIUM_OXIDE, RESOURCE_CATALYZED_LEMERGIUM_ALKALIDE);
            break;
        case 'ranged':
            compounds.push(RESOURCE_KEANIUM_OXIDE, RESOURCE_KEANIUM_ALKALIDE, RESOURCE_CATALYZED_KEANIUM_ALKALIDE);
            break;
        case 'tough':
            compounds.push(RESOURCE_GHODIUM_OXIDE, RESOURCE_GHODIUM_ALKALIDE, RESOURCE_CATALYZED_GHODIUM_ALKALIDE,);
            break;
    }
    let labs = creep.room.find(FIND_MY_STRUCTURES, {
        filter: (lab) => lab.structureType === STRUCTURE_LAB
            && lab.mineralAmount
            && lab.mineralAmount >= 150
            && lab.mineralType
            && _.contains(compounds, lab.mineralType)
    });
    if (!labs || !labs.length) {
        return ERR_NOT_ENOUGH_RESOURCES
    }
    let choice = _.max(labs, function (lab) {
        return lab.mineralAmount
    });
    if (choice) {
        if (creep.pos.isNearTo(choice)) {
            if (choice.boostCreep(creep) === 0) {
                creep.memory[role + '_boosted'] = true;
                return 'done';
            }
        } else {
            creep.travelTo(choice, {ignoreCreeps: false, repath: 0.1, maxRooms: 1})
            return 'boosting';
        }
    }
    return ERR_NOT_ENOUGH_RESOURCES;
};
global.initMemory = function (reset = false) {
    if (!Memory.Settings || reset === true) {
        Memory.Settings = {}
        Memory.Settings.Logging = {}
        Memory.Settings.Logging.AnchorPlanning = true
        Memory.Settings.Logging.AnchorErrors = false
        Memory.Settings.Logging.AnchorFlagging = true
        Memory.Settings.Logging.MinerPicks = false
        Memory.Settings.Logging.PlannerFlags = false
        Memory.Settings.Logging.StampBuilderReports = false
        Memory.Settings.CPU = {}
        Memory.Settings.CPU.LowerBucketLimit = 2000
        Memory.Settings.CPU.MediumBucketLimit = 4500
        Memory.Settings.CPU.UpperBucketLimit = 9000
        Memory.Settings.CPU.UsageLimit = 0.8
        Memory.Settings.Misc = {}
        Memory.Settings.Misc.AutoConstructionSitesLimit = 75
        Memory.Settings.Misc.StampSize = 3
        Memory.Settings.Misc.FatigueForRoads = 550
        Memory.Settings.Misc.highlevel = 1
        Memory.Settings.MiningDistanceInSteps = 200;
        Memory.Settings.WallLimit = 15000000
        Memory.Settings.SpawnLimits = {};
        Memory.Settings.SpawnLimits.Civilian = {};
        Memory.Settings.SpawnLimits.Civilian.Upgraders = 1;
        Memory.Settings.SpawnLimits.Civilian.Repairers = 1;
        Memory.Settings.SpawnLimits.Civilian.Builders = 2;
        Memory.Settings.SpawnLimits.Civilian.Couriers = 2;
        Memory.Settings.miningDeposits = false;
        Memory.Settings.miningPower = false;
        Memory.powerbanks={};
        Memory.deposits={};
        if (!Memory.stamps || reset === true) {
            Memory.stamps = {}
        }
        if (!Memory.stamps.core || reset === true) {
            Memory.stamps.core = [
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_SPAWN, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_SPAWN, STRUCTURE_ROAD, STRUCTURE_SPAWN, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_OBSERVER, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_STORAGE, STRUCTURE_ROAD, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_LINK, STRUCTURE_ROAD, STRUCTURE_TERMINAL, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_NUKER, STRUCTURE_POWER_SPAWN, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD]];
        }
        if (!Memory.stamps.flower || reset === true) {
            Memory.stamps.flower = [[STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_ROAD],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION],
                [STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_EXTENSION],
                [STRUCTURE_ROAD, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_EXTENSION, STRUCTURE_ROAD]];
        }
    }

    if (!Memory.planner || reset === true) {
        Memory.planner = {}
    }
    if (!Memory.sources || reset === true) {
        Memory.sources = {}
    }
    if (!Memory.counters || reset === true) {
        Memory.counters = {}
        Memory.counters.civilMissionCounter = 1
        Memory.counters.militaryMissionCounter = 1
        Memory.counters.CreepCounter = 1
    }
    if (!Memory.missions || reset === true) {
        Memory.missions = {};
        Memory.missions.civil = {};
        Memory.missions.military = {};
    }

}
global.cleanupMissions = function (limit = 1000) {
    numc = 0
    numm = 0
    for (amission in Memory.missions.civil) {
        thismission = Memory.missions.civil[amission];
        if (thismission.time < (Game.time - limit) && thismission.missionType !== 'claim') {
            delete Memory.missions.civil[amission]
            numc++
        }
    }
    for (amission in Memory.missions.military) {
        thismission = Memory.missions.military[amission];
        if (thismission.time < (Game.time - (limit * 1.5)) || thismission.creepTypes.length === 0) {
            delete Memory.missions.military[amission]
            numm++
        }
    }
    if (numc > 2 || numm > 2) {
        log.notification(numc + ' stale civilian missions removed')
        log.notification(numm + ' stale military missions removed')
        log.notification((numm + numc) + ' total missions removed')
    }
    if (Object.keys(Memory.missions.civil).length === 0) {
        Memory.counters.civilMissionCounter = 1
    }
    if (Object.keys(Memory.missions.military).length === 0) {
        Memory.counters.militaryMissionCounter = 1
    }
}
global.cleanupMemory = function () {
    try {
        if (Memory.counters.CreepCounter > 920) {
            Memory.counters.CreepCounter = 0
        }
        if (Memory.counters.missionCounter > 750) {
            Memory.counters.missionCounter = 0
        }

        if (Game.time % 100 === 0) {
            Intel.manageReceptions();
        }
        if (Game.time % 250 === 0) {
            cleanupMissions(1200)
            if (global.fatigueMap && Game.cpu.bucket > Memory.Settings.CPU.UpperBucketLimit) {
                let maxfatigue = _.max(global.fatigueMap);
                let roadnum = 0;
                let roomlist = [];
                if (maxfatigue >= Memory.Settings.Misc.FatigueForRoads) {
                    for (r in global.fatigueMap) {
                        if (global.fatigueMap[r] >= maxfatigue * 0.95) {
                            let splitpos = r.split('-');
                            let roadPos = new RoomPosition(splitpos[1], splitpos[2], splitpos[0])
                            roadPos.createConstructionSite(STRUCTURE_ROAD);
                            roadnum++;
                            delete global.fatigueMap[r];
                            if (!_.contains(roomlist, roadPos.roomName)) {
                                roomlist.push(roadPos.roomName)
                            }
                        }
                    }
                    console.log("----INFO:" + roadnum + 'roads were attemped in rooms: ' + roomlist + '.----')
                    global.fatigueMap = {}
                } else {
                    for (r in global.fatigueMap) {
                        global.fatigueMap[r] = global.fatigueMap[r] - 30
                    }
                }
            }
        }
        if (Game.cpu.bucket >= 9925 ) {
//        Game.cpu.generatePixel()
        }
    } catch {
    }
}
