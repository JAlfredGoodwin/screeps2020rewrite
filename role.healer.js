module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = mission.missionRoom
        }
        if (creep.pos.roomName !== creep.memory.homeRoom) {
            newPos = new RoomPosition(25, 25, creep.memory.homeRoom)
            creep.travelTo(newPos)
        }
        patient = creep.pos.findClosestByPath(FIND_MY_CREEPS, {
            filter: (s) => s.hits < s.hitsMax
        })
        if (!patient) {
            patient = creep.pos.findClosestByRange(FIND_HOSTILE_CONSTRUCTION_SITES)
        }
        if (creep.heal(patient) !== 0) {
            creep.travelTo(patient, {movingTarget:true})
            creep.rangedHeal(patient)
        }
        target= creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (target && target.pos && creep.pos.inRangeTo(target, 2)){
            creep.fleeTarget(target)
        }
    }
}
