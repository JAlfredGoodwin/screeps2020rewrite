module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.roomName
        }
        if (creep.pos.roomName !== creep.memory.homeRoom) {
            creep.travelTo(Game.rooms[creep.memory.homeRoom].controller)
        }
        if (creep.memory.working === true && _.sum(creep.carry) === 0) {
            creep.memory.working = false;
            creep.memory.target = undefined;
        } else if (_.sum(creep.store) >= (creep.store.getCapacity()) || creep.memory.done >5 ) {
            creep.memory.working = true;
            creep.memory.done = 0
        }
        if (creep.memory.working === true) {
            require('role.delivery').run(creep);
        }
        if (creep.memory.working === false) {
            target = creep.getDroppedResources()
            if (target && creep.pos.isNearTo(target)) {
                if (target instanceof Resource) {
                    creep.pickup(target)
                } else {
                    _.some(target.store, (amount, resource) => amount && creep.withdraw(target, resource) !== undefined);
                }
            } else {
                creep.travelTo(target)
            }
            if (!target){
                creep.memory.done++
            }
        }
    }
}
