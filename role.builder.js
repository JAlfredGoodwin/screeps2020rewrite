require('prototype.Creep');
module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
            return
        }
        var source = {};
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.roomName
        }
        var conta = {};
        var constructionSite = undefined;
        if (creep.memory.renewing && creep.ticksToLive > 1000) {
            creep.memory.renewing = false;
        }
        if (!creep.isBoosted() && creep.ticksToLive < 350 && creep.hitsMax > 2200 && creep.room.energyAvailable && creep.room.energyAvailable >= 200) {
            creep.memory.renewing = true;
        }
        if (creep.memory.renewing === true && creep.room.energyAvailable && creep.room.energyAvailable >= 200) {
            goRenew(creep);
            return;
        }
        if (creep.memory.working === true && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = false;
            creep.memory.target = undefined;
        } else if (creep.memory.working === false && creep.store[RESOURCE_ENERGY] === creep.carryCapacity) {
            creep.memory.working = true;
            creep.memory.target = undefined;
        }
        if (creep.memory.targetTime + 25 < Game.time) {
            creep.memory.target = undefined;
        }
        if (creep.pos.roomName !== creep.memory.homeRoom) {
            let homePos = new RoomPosition(25, 25, creep.memory.homeRoom)
            creep.travelTo(homePos, {range: 10})

        } else {
            if (creep.memory.working === true) {
                if (_.isString(creep.memory.target)) {
                    constructionSite = Game.getObjectById(creep.memory.target)
                } else {
                    creep.memory.target = undefined;
                    constructionSitez = creep.room.find(FIND_MY_CONSTRUCTION_SITES);
                    if (constructionSitez.length > 0) {
                        constructionSite = creep.pos.findClosestByRange(constructionSitez);
                        creep.memory.target = constructionSite.id
                        creep.memory.targetTime = Game.time;
                    } else {
                        require('role.upgrader').run(creep);
                    }
                }
                if (_.isEmpty(constructionSite)) {
                    creep.memory.target = undefined;
                    return;
                }
                if (_.isString(creep.memory.target) && constructionSite.pos) {
                    if (creep.build(constructionSite) === ERR_NOT_IN_RANGE) {
                        creep.travelTo(constructionSite, {range: 3, ignoreCreeps: false, repath: 0.9, maxRooms: 1});
                    }
                }
            } else if (creep.memory.working === false) {
                if (creep.memory.tanking && Game.getObjectById(creep.memory.tanking) && Game.getObjectById(creep.memory.tanking).store && Game.getObjectById(creep.memory.tanking).store.energy > 10) {
                    conta = Game.getObjectById(creep.memory.tanking)
                } else {
                    conta = creep.getEasyEnergy()
                }
                if (conta) {
                    creep.memory.tanking = conta.id;
                    if (creep.pos.isNearTo(conta)) {
                        creep.withdraw(conta, RESOURCE_ENERGY);
                        delete creep.memory.tanking
                    } else {
                        creep.travelTo(conta, {
                            ignoreRoads: (_.sum(creep.store) === 0),
                            ignoreCreeps: false,
                            repath: 0.9
                        });
                    }
                }
                if (!conta) {
                    source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
                    if (creep.pos.isNearTo(source)) {
                        creep.harvest(source)
                    } else {
                        creep.travelTo(source, {
                            ignoreRoads: (_.sum(creep.store) === 0),
                            repath: 0.9,
                            ignoreCreeps: false
                        });
                    }
                }
            }
            let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
            if (droppedEnergy.length >= 1 && droppedEnergy[0].resourceType == RESOURCE_ENERGY) {
                creep.pickup(droppedEnergy[0]);
            }
            if (creep.store.energy > 0) {
                let roadspot = creep.pos.lookFor(LOOK_STRUCTURES, {filter: (c) => c.structureType === STRUCTURE_ROAD});
                if (roadspot[0] && roadspot[0].hits < roadspot[0].hitsMax) {
                    creep.repair(roadspot[0]);
                }
            }
        }
    }
};
