module.exports = {
    run: function (creep) {
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.roomName
        }
        if (creep.memory.mission){
            mission=creep.memory.mission
        }
        if (creep.spawning) {
            return
        }
        let source = {};
        if (_.sum(creep.store) === 0) {
            creep.memory.working = false;
        } else if (_.sum(creep.store) === creep.carryCapacity) {
            creep.memory.working = true;
        }
        let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (droppedEnergy.length >= 1) {
            creep.pickup(droppedEnergy[0]);
        }
        if (creep.ticksToLive <= 150) {
            creep.memory.working = true
            if (_.sum(creep.store) === 0) {
                creep.memory.role = 'zombie'
            }
        }
        if (creep.memory.working === true) {
            let structure = Game.rooms[creep.memory.homeRoom].storage;

            if (structure !== undefined) {
                if (creep.pos.isNearTo(structure)) {
                    _.some(creep.carry, (amount, resource) => amount && creep.transfer(structure, resource) !== undefined);
                } else {
                    creep.travelTo(structure, {ignoreCreeps: false, repath: 0.9})
                }
            } else {
                require('role.hauler').run(creep);
            }
        } else {
            source=Game.getObjectById(mission.target)
            if (!source){source = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => s.store && (_.sum(s.store) - s.store[RESOURCE_ENERGY]) > 5 && !s.my
            });}
            if (!source) {
                source = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s) => s.store && _.sum(s.store) > 0
                        && s.structureType !== STRUCTURE_STORAGE
                        && s.structureType !== STRUCTURE_TOWER
                        && s.structureType !== STRUCTURE_TERMINAL
                        && s.structureType !== STRUCTURE_SPAWN
                        && s.structureType !== STRUCTURE_NUKER
                        && s.structureType !== STRUCTURE_LAB
                        && s.structureType !== STRUCTURE_POWER_SPAWN
                        && s.structureType !== STRUCTURE_FACTORY
                        && s.structureType !== STRUCTURE_TOWER
                });
            }
            if (source) {
                if (creep.pos.isNearTo(source)) {
                    _.some(source.store, (amount, resource) => amount && creep.withdraw(source, resource) !== undefined);
                } else {
                    creep.travelTo(source, {offRoad: (creep.store.getUsedCapacity() === 0), repath: 0.1})
                }
            } else {
                creep.moveOffRoad();
                creep.memory.working = true;
            }

        }
    }
};
