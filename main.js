//return
global.lastMemoryTick = undefined;

function tryInitSameMemory() {
    if (lastMemoryTick && global.LastMemory && Game.time == (lastMemoryTick + 1)) {
        delete global.Memory;
        global.Memory = global.LastMemory;
        RawMemory._parsed = global.LastMemory
    } else {
        Memory;
        global.LastMemory = RawMemory._parsed
    }
    lastMemoryTick = Game.time
}


require('prototype.Structure');
//require('utils.WarInternalGlobals')();
require('prototype.Room')();
require('prototype.Source')();
require('prototype.Creep')();
require('utils.Globals');
require('utils.Commands');
require('StampBuilder');
require('planner')
require('structure.link')
var Traveler = require('utils.traveler2');
var RoleRunner = require('RoleRunner');
var SpawnManager = require('SpawnManager');
require('utils.Intel');


//return;


module.exports.loop = function () {
    if (!Memory.respawn && Object.keys(Game.creeps).length === 0 && Object.keys(Game.structures).length === 2) {
        log.warn('******RESPAWN DETECTED******')
        log.warn('Respawn Time: ' + Game.time)
        log.notification('Memory cleared and Home Room changed')
        delete Memory.rooms;
        delete Memory.sources;
        delete Memory.CurrentlyMining
        delete Memory.reservedRooms
        delete Memory.reservedControllers
        delete global.LastMemory
        initMemory(true)
        intelRun();
        start = _.find(Game.structures).pos.roomName
        evaluateRoomForMining(start)
        Memory.Settings.Misc.HomeRoom = start
        Memory.Settings.Misc.RespawnTime = Game.time
        roomPlanningProcess()
        buildRoomCore(start)
        Memory.respawn = true
    }
///// End Respawncheck

    delete global.targetcache
    initMemory()
    intelRun();
    if (!Memory.CurrentlyMining || Game.time % 1000 === 0) {
        Memory.CurrentlyMining = []
    }
    if (Game.time % 100 === 0) {
        buildSourcesForMining();
        evaluateSourcesForMining();
    }
    global.takenExtensions = new Set();
    if (!global.takenSources) {
        global.takenSources = [];
    }



    for (let name in Memory.creeps) {
        if (Game.creeps[name] === undefined) {
            if (Memory.creeps.mission && Memory.creeps.mission.mission){
                delete Memory.missions.civil[Memory.creeps.mission.mission]
                delete Memory.missions.military[Memory.creeps.mission.mission]
                delete Memory.powerbanks[Memory.creeps.mission.target].mission
                delete Memory.deposits[Memory.creeps.mission.target].mission
            }
            delete Memory.creeps[name];
        }
    }
    if (Game.time % 5 === 0) {
        for (m in Memory.sources) {
            if ((Memory.sources[m].takenBy && !Game.creeps[Memory.sources[m].takenBy] )|| (!Memory.sources[m].takenBy && _.contains(Memory.CurrentlyMining, Memory.sources[m].id))) {
                delete Memory.sources[m].takenBy
                if (!mineMissionCheck(Memory.sources[m].pos.roomName, Memory.sources[m].id) && Memory.rooms[Memory.sources[m].pos.roomName].extracting) {
                    createMission('mine', Memory.sources[m].id, Memory.sources[m].pos.roomName)
                }
            }
        }
    }
    if (Game.time % 4 === 0 || Game.cpu.bucket > 7500 || Memory.Threats === true) {
        var hurt = {};
        var towers = _.filter(Game.structures, (str) => {
            return str.structureType === STRUCTURE_TOWER
        });
        for (tower of towers) {
            if (tower) {
                let closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
                if (closestHostile) {
                    console.log('BANG')
                    tower.attack(closestHostile);
                } else {
                    hurt = tower.pos.findClosestByRange(FIND_MY_CREEPS, {filter: (c) => c.hits + 5 < c.hitsMax});
                    tower.heal(hurt);

                }

                if (!closestHostile && !hurt) {
                    dmg = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => (structure.hits <= 3001 && (structure.hits + 3500) < structure.hitsMax && tower.energy >= 900 && structure.my == true)
                            || (((structure.hits - 990) <= 200) && structure.structureType !== STRUCTURE_WALL
                                && structure.structureType !== STRUCTURE_RAMPART && tower.energy >= 800)
                            || ((structure.structureType === STRUCTURE_WALL || structure.structureType === STRUCTURE_RAMPART) && (structure.hits < structure.hitsMax && structure.hits < (Memory.Settings.WallLimit * 0.01)) && tower.energy >= 990)
                    });
                    if (dmg) {
//                        tower.repair(dmg);
                    }
                }
            }
        }
    }
    RoleRunner.run();

    if (Game.time % 13 === 0 && Game.cpu.bucket > 5000) {
        let terminals = _.filter(Game.structures, (str) => {
            return str.structureType === STRUCTURE_TERMINAL
        });
        if (terminals.length > 0) {
            var maxterm = 0;
            var minterm = 700000;
            var maxtermroom = undefined;
            var mintermroom = undefined;
            var totnrg = 0;
            for (term of terminals) {
                let termnrg = 0;
                let storenrg = 0;
                let troom = term.pos.roomName;
                if (troom.cooldown) {
                    continue;
                }
                if (Game.flags['Push'] && Game.flags['Push'].pos.roomName !== troom && (Game.rooms[troom].storage.store[RESOURCE_ENERGY] + Game.rooms[troom].terminal.store[RESOURCE_ENERGY]) > 100000) {
                    Game.rooms[troom].terminal.send(RESOURCE_ENERGY, 5000, Game.flags['Push'].pos.roomName);
                    continue;
                }

                termnrg = Game.rooms[troom].terminal.store[RESOURCE_ENERGY];
                if (Game.rooms[troom].storage) {
                    storenrg = Game.rooms[troom].storage.store[RESOURCE_ENERGY]
                }
                totnrg = termnrg + storenrg;

                if (Game.rooms[troom].controller.level >= 6 && totnrg < 160000 && totnrg < minterm) {
                    minterm = totnrg;
                    mintermroom = troom;
                }
                if (totnrg > 250000 && (totnrg > maxterm) && Game.rooms[troom].terminal.cooldown === 0) {
                    maxtermroom = troom;
                    maxterm = totnrg;
                }
                if (Game.rooms[troom].controller.level < 6) {
                    continue;
                }
                if (!Memory.Terminals) {
                    Memory.Terminals = {};
                }
                if (!Memory.Terminals[term.id]) {
                    Memory.Terminals[term.id] = {};
                }
            }
        }

        if (maxtermroom && mintermroom) {
            console.log('Sending energy from ' + maxtermroom + ' to ' + mintermroom);
            Game.rooms[maxtermroom].terminal.send(RESOURCE_ENERGY, 25000, mintermroom)

        }
    }

    if (Game.time % 150 === 14) {
        updateMarketPrices()
    }

    if (Game.time % 8 === 0 || Game.cpu.bucket > 5000) {

    }

    if (Game.cpu.bucket >= 8000 || Game.time % 50 === 0 || Memory.Threats === true) {
        SpawnManager.run();

    }
    roomPlanningProcess();
    runLink()
    if (Game.time % 2500 < 44) {
        sendExcess(RESOURCES_ALL[Game.time % 2500])
    }


    if (Game.time % 25 === 21) {
        for (s in Game.spawns) {
            buildRoomCore(Game.spawns[s].pos.roomName)
            if (Memory.rooms[Game.spawns[s].pos.roomName].level === 5 && Memory.rooms[Game.spawns[s].pos.roomName].extensions && Memory.rooms[Game.spawns[s].pos.roomName].extensions.fourth) {
                buildExtensions(Game.spawns[s].pos.roomName, 'first')
            }
            if (Memory.rooms[Game.spawns[s].pos.roomName].level > 5 && Memory.rooms[Game.spawns[s].pos.roomName].extensions && Memory.rooms[Game.spawns[s].pos.roomName].extensions.fourth) {
                buildExtensions(Game.spawns[s].pos.roomName, 'first')
                buildExtensions(Game.spawns[s].pos.roomName, 'second')
            }
            if (Memory.rooms[Game.spawns[s].pos.roomName].level === 8 && Memory.rooms[Game.spawns[s].pos.roomName].extensions && Memory.rooms[Game.spawns[s].pos.roomName].extensions.fourth) {
                buildExtensions(Game.spawns[s].pos.roomName, 'third')
                buildExtensions(Game.spawns[s].pos.roomName, 'fourth')
            }
        }
        let important = _.filter(Game.structures, (str) => {
            return (str.structureType === STRUCTURE_TOWER
                || str.structureType === STRUCTURE_SPAWN
                || str.structureType === STRUCTURE_STORAGE
                || str.structureType === STRUCTURE_TERMINAL
                || str.structureType === STRUCTURE_LINK
            )
        });
        for (struct of important) {
            struct.pos.createConstructionSite(STRUCTURE_RAMPART);
        }
    }
    if (Game.time % 100 === 0) {
        empireReport()
    }
    cleanupMemory()
}
empireReport = function () {
    tab = '        '
    stars = '********'
    log.warn(tab + stars + tab + 'Empire report - 100 tick ' + tab + stars + tab)
    statusCheck()
    mineralreport()
    nums = 0
    storeenergy = 0
    storenum = 0
    cont = 0
    for (room in Memory.rooms) {
        if (Memory.rooms[room].my) {
            roomCheck(room)
            nums++
            if (Game.rooms[room].storage) {
                storeenergy += Game.rooms[room].storage.store.energy
                storenum++
            }
            cont += Game.rooms[room].controller.level
        }
    }
    log.notification('We currently control ' + nums + ' rooms and ' + Object.keys(Game.spawns).length + ' spawns')
    log.notification(tab + 'Our average RCL is ' + (cont / nums) + ' with the highest room at ' + Memory.Settings.Misc.highlevel)
    log.notification('Our storages contain ' + storeenergy.toLocaleString() + ' across ' + storenum.toLocaleString() + ' storages for an average of ' + (storeenergy / storenum).toLocaleString() + ' per room with a storage')
    updateRoomScores()
    log.notification(tab + 'Next Room suggested for  colonization is ' + Memory.Settings.Misc.nextRoom + ' with a score of ' + Memory.Settings.Misc.nextRoomScore + '.')
    log.notification(tab + 'There are currently ' + Object.keys(Memory.missions.civil).length + ' civil missions and ' + Object.keys(Memory.missions.military).length + ' military missions pending spawn.')
    log.notification('We are currently working on ' + Object.keys(Game.constructionSites).length + ' construction sites.')
}
mineMissionCheck = function (room,id) {
    for (amission in Memory.missions.civil) {
        parts = amission.split('_');
        if (_.contains(parts, room) && _.contains(parts, 'mine') && Memory.missions.civil[amission].missionTarget === id) {
            return true
        }
    }
    return false
}
