module.exports = function(){
global.getRoomType = function (roomName) {
	const res = /[EW](\d+)[NS](\d+)/.exec(roomName);
	const [, EW, NS] = res;
	const EWI = EW % 10, NSI = NS % 10;
	if (EWI === 0 || NSI === 0) {
		return 'Highway';
	} else if (EWI === 5 && NSI === 5) {
		return 'Center';
	} else if (Math.abs(5 - EWI) <= 1 && Math.abs(5 - NSI) <= 1) {
		return 'SourceKeeper';
	} else {
		return 'Room';
	}
};
    Room.prototype.findStructuresByType = function(types) { // i tend to forget which one to use
      var ret = [];
        if(!_.isArray(types)) types = [types];
            for(let type of types) {
                var find_ = FIND_MY_STRUCTURES;
                if(type == STRUCTURE_WALL      || type == STRUCTURE_ROAD  ||
                    type == STRUCTURE_CONTAINER || type == STRUCTURE_PORTAL) find_ = FIND_STRUCTURES;
                var structList = this.find(find_, {filter: { structureType: type }});
                if(structList && structList.length>0)
                    for (let struct of structList) {
//                if(_.isUndefined(struct.memory.ignore))
                        ret.push(struct);
                    }
            }
        return ret;
    };
Room.prototype.getMineral = function () {
    let minerals = this.find(FIND_MINERALS);
    if (minerals.length > 0) {
        return minerals[0];
    }
    return undefined;
};
RoomPosition.prototype.isEqualTo = function(firstArg, secondArg){
    var x = firstArg,
        y = secondArg,
        roomName;
    if (_.isUndefined(secondArg)) {
        var pos = firstArg;
        if (pos.pos) {
            pos = pos.pos;
        }
        x = pos.x;
        y = pos.y;
        roomName = pos.roomName;
    }
    return x == this.x && y == this.y && (!roomName || roomName == this.roomName);
};
Room.prototype.marketSellerList = function(compound, priceCap, distanceCap) {
    let result = Game.market.getAllOrders({type: ORDER_SELL, resourceType: compound});
    let result1 = [];
	for (res of result){
		if (res.price <= priceCap && Game.map.getRoomLinearDistance(this.name, res.roomName) <= distanceCap){
			result1.push(res)
		}
	}
	result1.sort((a,b) => (a.price - b.price));
    return result1;
};

Room.prototype.marketBuyerList = function(compound, priceCap, distanceCap) { //  This is finding a buyer - we need caching!
    let result = Game.market.getAllOrders({type: ORDER_BUY, resourceType: compound});
    let result1 = [];
	for (res of result){
		if (res.price >= priceCap && Game.map.getRoomLinearDistance(this.name, res.roomName) <= distanceCap && res.amount > 0){
			result1.push(res)
		}
	}
	result1.sort((a,b) => (b.price - a.price));
    return result1;
};


Room.prototype.bestMarketSellerPrice = function(compound, distanceCap) {
    let offers = this.marketSellerList(compound, 10000, distanceCap);
    if (offers.length > 0) {
        return offers[0].price;
    }
};

Room.prototype.bestMarketBuyerPrice = function(compound, distanceCap) {
    let offers = this.marketBuyerList(compound, 0.01, distanceCap);
    if (offers.length > 0) {
        return offers[0].price;
    }
};

Room.prototype.marketBuy = function(compound, amount, priceCap, distanceCap) {
    let offers = this.marketSellerList(compound, priceCap, distanceCap);
    if (offers.length > 0){
        let best = offers[0];
        let result = Game.market.deal(best.id, Math.min(amount, best.amount), this.name);
        console.log(this.name + ": buying " + Math.min(amount, best.amount) + " of " + compound + " at " + best.price + " per unit. Result: " + result + ".");
    }
};

Room.prototype.marketSell = function(compound, amount, priceCap, distanceCap) {
    let offers = this.marketBuyerList(compound, priceCap, distanceCap);
    if (offers.length > 0){
        let best = offers[0];
        let result = Game.market.deal(best.id, Math.min(amount, best.amount), this.name);
        console.log(this.name + ": selling " + Math.min(amount, best.amount) + " of " + compound + " at " + best.price + " per unit. Result: " + result + ".");
    }
}

Mineral.prototype.getMiningPositions = function () {
    let positions = [];
    for (let x = -1; x < 2; x++) {
        for (let y = -1; y < 2; y++) {
            let position = new RoomPosition(this.pos.x + x, this.pos.y + y, this.room.name);
            if (!(position.x === this.pos.x && position.y === this.pos.y)) {
                let terrainAtPositon = Game.map.getTerrainAt(position);
                if (terrainAtPositon === "swamp" || terrainAtPositon === "plain") {
                    positions.push(position);
                }
            }
        }
    }
    return positions;
}
    RoomPosition.prototype.freePositionsAround = function () {
        let positions = [];
        for (let x = -1; x < 2; x++) {
            for (let y = -1; y < 2; y++) {
                let position = new RoomPosition(this.x + x, this.y + y, this.roomName);
                if (!(position.x === this.x && position.y === this.y)) {
                    let terrainAtPositon = Game.map.getTerrainAt(position);
                    if (terrainAtPositon === "swamp" || terrainAtPositon === "plain") {
                        positions.push(position);
                    }
                }
            }
        }
        return positions;
    }
/**
 * Since findClosestByPath is a limited to a single room, we have this.
 *
 * @param {Object} goals - collection of RoomPositions or targets
 * @param {function} itr - iteratee function, called per goal object
 * @todo: Add cost matrix support
 * @todo: Add maxCost support
 * @todo: replace with roomCallback
 * @todo: maxCost testing
 */
RoomPosition.prototype.findClosestByPathFinder = function (goals, itr = _.identity, opts = {}) {
	const mapping = _.map(goals, itr);
	if (_.isEmpty(mapping))
		return { goal: null };
	_.defaults(opts, {
		maxOps: 16000,
		plainCost: 2,
		swampCost: 5,

	});
	const result = PathFinder.search(this, mapping, opts);
	/*if (!result.path.length) {
		log.error(ex(mapping));
		log.error(ex(opts));
		log.error(ex(result));
	}*/
	// if(result.incomplete)
	//	throw new Error('Path incomplete');
	let last = _.last(result.path);
	if (last == null)
		last = this;
	// return {goal: null};
	const goal = _.min(goals, g => last.getRangeTo(g.pos));
	return {
		goal: (Math.abs(goal) !== Infinity) ? goal : null,
		cost: result.cost,
		ops: result.ops,
		incomplete: result.incomplete,
		path: result.path
	};
};

RoomPosition.prototype.findClosestSpawn = function () {
	const spawns = Game.spawns;
	return this.findClosestByPathFinder(spawns, (spawn) => ({ pos: spawn.pos, range: 1 })).goal;
};

RoomPosition.prototype.findClosestConstructionSite = function () {
	return this.findClosestByPathFinder(Game.constructionSites,
		(cs) => ({ pos: cs.pos, range: 1 })).goal;
};

RoomPosition.prototype.findClosestStorage = function () {
	const storages = _.filter(Game.structures, 'structureType', STRUCTURE_STORAGE);
	return this.findClosestByPathFinder(storages, s => ({ pos: s.pos, range: 1 })).goal;
};

RoomPosition.prototype.findClosestCreep = function () {
	return this.findClosestByPathFinder(Game.creeps,
		(c) => ({ pos: c.pos, range: 1 })).goal;
};
RoomPosition.prototype.openSpace = function (input = 3) {
    for (let x = 0 - input; x < 1 + input; x++) {
        for (let y = 0 - input; y < 1 + input; y++) {
            let position = new RoomPosition(this.x + x, this.y + y, this.roomName);
            let terrainAtPositon = Game.map.getTerrainAt(position);
            if (terrainAtPositon === 'wall') {
                return false;
            }
        }
    }
    return true;
};

};
