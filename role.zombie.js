module.exports = {
    run: function (creep) {
        if (Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
            return
        }
        if (creep.memory.zombie) {
            spawn = Game.getObjectById(creep.memory.zombie)

        } else {
            creep.memory = {role: 'zombie'}
            spawn = creep.pos.findClosestSpawn();
            creep.memory.zombie = spawn.id
        }
        if (spawn) {
            if (spawn.recycleCreep(creep) !== 0) {
                creep.travelTo(spawn, {ignoreCreeps: false, repath: 0.7});
            }
        } else {
            if (!creep.memory.homeroom) {
                targ = creep.pos.findClosestSpawn()
                creep.memory.homeroom = targ.room.name;
                creep.travelTo(targ);
                return
            }
            newPos = new RoomPosition(25, 25, creep.memory.homeroom, {range: 9});
            creep.travelTo(newPos);
        }
    }
};
