module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.LowerBucketLimit) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        missiontarget = false
        target = Game.getObjectById(creep.memory.target)
        if (target) {
            if (creep.dismantle(target) !== 0) {
                creep.travelTo(target)
                return
            }
        }
        cacheTargets(creep.pos);
        if (creep.pos.roomName === mission.missionRoom) {
            if (!creep.memory.target) {
                creep.memory.target =
                    creep.pos.findClosestByPath(
                        FIND_STRUCTURES, {
                            filter: (c) => (
                                c.structureType === STRUCTURE_WALL ||
                                c.structureType === STRUCTURE_RAMPART
                            )
                        }
                    ).id
            }
            target = Game.getObjectById(creep.memory.target)
            if (target) {
                if (creep.dismantle(target) !== 0) {
                    creep.travelTo(target)

                }
            } else {
                delete creep.memory.target
            }
        } else {
            newPos = new RoomPosition(25, 25, mission.missionRoom)
            creep.travelTo(newPos)
        }
    }
}





