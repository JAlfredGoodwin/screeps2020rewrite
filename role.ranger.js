module.exports = {
    run: function (creep) {
        if (creep.spawning|| Game.cpu.bucket < Memory.Settings.CPU.LowerBucketLimit) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        missiontarget = false
        if (mission.target) {
            missiontarget = Game.getObjectById(mission.target)
        }
        cacheTargets(creep.pos);
        patient = false
        target = false
        rangedHeal = true
        if (missiontarget) { /// /We have a missiontarget
            multiTarget = creep.pos.findInRange(targetcache[creep.pos.roomName].targets, 3)
            if (creep.rangedAttack(missiontarget) !== 0) {
                creep.travelTo(missiontarget, {range: 2, movingTarget: true, brave: true})
            } else {
                creep.heal(creep)
            }
            if (creep.pos.getRangeTo(missiontarget) < 3) {
                creep.fleeTarget(missiontarget);
            }
        } else {/// we dont have a mT
            if (creep.pos.roomName === mission.missionRoom) { /// we're in the right room
                target = creep.pos.findClosestByRange(targetcache[creep.pos.roomName].targets)
                if (targetcache[creep.pos.roomName].targets.length > 0) {
                    multiTarget = creep.pos.findInRange(targetcache[creep.pos.roomName].targets, 3)
                }
                if (creep.hasActiveBodypart(RANGED_ATTACK) && target) {
                    if (multiTarget.length > 2) {
                        creep.rangedMassAttack();
                        rangedHeal = false
                    } else {
                        if (creep.rangedAttack(target) != 0) {
                            creep.travelTo(target, {range: 3, brave: true, freshMatrix: true})
                        } else {
                            creep.travelTo(target, {range: 2, brave: true})
                        }
                    }
                    if (creep.pos.getRangeTo(target) < 3) {
                        creep.fleeTarget(target);
                    }

                }
                if (creep.hasActiveBodypart(HEAL)) {
                    patient = creep.pos.findClosestByPath(FIND_MY_CREEPS, {filter: (s) => s.hitsMax > s.hits})
                    if (patient && creep.pos.inRangeTo(patient, 1)) {
                        creep.heal(patient)
                    } else {
                        if (patient && rangedHeal && creep.pos.inRangeTo(patient, 3)) {
                            creep.rangedHeal(patient)
                        }
                    }
                    if (!target && patient) {
                        creep.travelTo(patient)
                    }
                }
                if (!target && !patient) {
                    creep.say('BORED')
                    creep.memory.bored++
                    if (creep.memory.bored >= creep.ticksToLive - 275) {
                        delete Memory.missions.military[creep.memory.mission.mission]
                        creep.memory.role = 'zombie'
                    }
                }
            } else { //// wrong room
                target = creep.pos.findClosestByRange(targetcache[creep.pos.roomName].targets)
                if (targetcache[creep.pos.roomName].targets.length > 0) {
                    multiTarget = creep.pos.findInRange(targetcache[creep.pos.roomName].targets, 3)
                }
                if (creep.hasActiveBodypart(RANGED_ATTACK) && target) {
                    if (!multiTarget || multiTarget.length < 4) {
                        creep.rangedAttack(target)
                    } else {
                        if (multiTarget.length > 0) {
                            creep.rangedMassAttack()
                            rangedHeal = false
                        }
                    }
                }
                if (creep.hasActiveBodypart(HEAL)) {
                    patient = creep.pos.findClosestByPath(FIND_MY_CREEPS, {filter: (s) => s.hitsMax > s.hits})
                    if (patient && creep.pos.inRangeTo(patient, 1)) {
                        creep.heal(patient)
                    } else {
                        if (patient && rangedHeal && creep.pos.inRangeTo(patient, 3)) {
                            creep.rangedHeal(patient)
                        }
                    }
                }
                newPos = new RoomPosition(25, 25, mission.missionRoom)
                creep.travelTo(newPos)
            }
        }
    }
}
