module.exports = {
    run: function (creep) {
        if (!global.scouting) {
            global.scouting = []
        }
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
            return
        }
        if (creep.memory.sleep && creep.memory.sleep > Game.time) {
            creep.say('naptime');
            return;
        }
        if (!creep.memory.scoutroom) {
            creep.memory.scoutroom = findNewRooms(creep.pos.roomName)
            global.scouting.push(creep.memory.scoutroom)
        }

        scoutroom = creep.memory.scoutroom
        if (scoutroom) {
            pos = new RoomPosition(24, 24, scoutroom)
        } else {
            csp = creep.pos.findClosestSpawn()
            creep.travelTo(csp)
            if (Game.time % 50 === 0
            ) {
                log.notify(creep.name + ' is stuck and moving towards ' + csp.pos.roomName);
            }
            return
        }
        if (!creep.pos.inRangeTo(pos, 15)) {
            creep.travelTo(pos, {range: 15, ignoreRoads: true});
        } else {
            if (creep.pos.roomName === scoutroom) {
                delete creep.memory.scoutroom;
                creep.memory.sleep = Game.time + 5
            }
        }
    }
}
