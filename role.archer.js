module.exports = {
    run: function (creep) {
        if (creep.spawning || Memory.Settings.CPU.LowerBucketLimit) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        if (mission.target) {
            missiontarget = Game.getObjectById(mission.target)
        }
        cacheTargets(creep.pos);
        if (missiontarget) {
            if (creep.rangedAttack(missiontarget) !== 0) {
                creep.travelTo(missiontarget, {movingTarget: true, range: 2, brave: true})
                return
            } else {
                creep.say('TWANG')
                return
            }
            if (creep.pos.getRangeTo(missiontarget) < 3) {
                creep.fleeTarget(missiontarget);
            }
        } else {
            if (creep.pos.roomName === mission.missionRoom) {
                target = creep.pos.findClosestByRange(targetcache[creep.pos.roomName].targets)
                if (target) {
                    if (creep.rangedAttack(target) !== 0) {
                        creep.travelTo(target, {movingTarget: true, range: 2, brave: true})

                    } else {
                        creep.say('TWANG')
                    }
                    if (creep.pos.getRangeTo(target) < 3) {
                        creep.fleeTarget(target);
                    }
                } else {
                    creep.say('BORED')
                    creep.memory.bored++
                    if (creep.memory.bored >= 75) {
                        delete Memory.missions.military[creep.memory.mission.mission]
                        creep.memory.role = 'zombie'
                    }
                }
            } else {
                newPos = new RoomPosition(25, 25, mission.missionRoom)
                creep.travelTo(newPos, {range: 2, brave: true,ignoreRoads:true,useFindRoute:true})
            }
        }
    }
}
