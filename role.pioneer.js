/*  Civil Mission Structure
 *   mission.name = thismission
 *   mission.missionType = mType;
 *   mission.target = mTarget; room to work in
 *   mission.missionRoom = mRoom; room to work in
 *   mission.assignedSpawnRoom = homeRoom;
 *   mission.spawned = false;
 *   mission.time = Game.time;
 */
module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.LowerBucketLimit) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        } else {
            mission = {'target': creep.pos.roomName, 'missionRoom': creep.pos.roomName, 'missionType': 'pioneer'}
        }
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = mission.missionRoom
        }
        delete Memory.missions.civil[mission.mission]
        if (creep.memory.renewing && creep.ticksToLive > 1000) {
            creep.memory.renewing = false;
        }
        if (!creep.isBoosted() && creep.ticksToLive < 350 && creep.hitsMax > 2200 && creep.room.energyAvailable && creep.room.energyAvailable >= 200) {
            creep.memory.renewing = true;
        }
        if (creep.memory.renewing === true && creep.room.energyAvailable && creep.room.energyAvailable >= 200) {
            goRenew(creep);
            return;
        }
        if (creep.pos.roomName !== creep.memory.homeRoom) {
            newpos = new RoomPosition(25, 25, creep.memory.homeRoom)
            creep.travelTo(newpos)
            return;
        }
        if (creep.store.energy === 0) {
            creep.memory.ticks = undefined;
            creep.memory.working = false;
            delete creep.memory.target
            creep.memory.state = 'builder'
        }
        if (creep.memory.working === false && creep.store.energy >= creep.carryCapacity) {
            creep.memory.working = true;
            creep.memory.target = false;
            creep.memory.ticks = undefined;
        }
        if (creep.memory.working === true && Game.rooms[creep.pos.roomName].energyCapacityAvailable && Game.rooms[creep.pos.roomName].energyCapacityAvailable > 1 && Game.rooms[creep.pos.roomName].energyAvailable < Game.rooms[creep.pos.roomName].energyCapacityAvailable) {
            require('role.courier').run(creep)
            if (creep.store.energy > 0) {
                let roadspot = creep.pos.lookFor(LOOK_STRUCTURES, {filter: (c) => c.structureType === STRUCTURE_ROAD});
                if (roadspot[0] && roadspot[0].hits < roadspot[0].hitsMax) {
                    creep.repair(roadspot[0]);
                }
            }
            return;
        }
        if (creep.hasActiveBodypart(WORK)) {
            require('role.repairer').run(creep);
        }
    }
};
