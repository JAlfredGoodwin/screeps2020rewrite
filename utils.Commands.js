/** Console Commands
 *  Civil -
 * orderMoreEnergy(number)
 *  Loops through owned terminals, and checks energy available.  Orders energy at market price to bring the room up to the input
 *
 * mineralReport
 *  Counts the minerals in your owned rooms and returns the totals
 *
 * removeRoadsnear(pos, range)
 *  destroys all roads within <range> of the input RoomPosition
 *
 * removeEnemyNear(pos, range)
 *  destroys hostile structures within <range> of RoomPosition unless they contain energy
 *
 * removeFlags
 *  destroys all Flags
 *
 * roomCleanup(roomname, amount=100)
 *  destroys constructionsites in roomname with progress less than amount
 *
 * allConstructionCleanup
 *  destroys all constructionsites
 *
 * avoidRoom(roomName)
 *  adds roomName to the avoid list for Traveler
 *
 * cleanupRoads
 * roadsiteCleanup
 *  remove all road constructionsites with less than 50 progress
 *
 * statusCheck
 *  returns the % value of your progress to the next GCL
 *
 * launchNuke(roomPosition)
 *  Launches the first armed nuker it finds at roomPosition
 *  *no guarantee of closest nuker firing
 *
 * countNukers(roomName)
 *  counts the number of nukers that can reach the target, whether armed or not
 *
 * addAlly(playerName)
 * addNeutral(playerName)
 *  adds playerName to the appropriate target control list
 *
 * buildRoad(RoomPosition1,RoomPosition2,non-road cost as integer)
 *  builds a road between the two positions, pathing as if the plains in between had the cost value entered
 *  *swamps are automatically 5 times the plains cost
 *  *higher values help merge roads into highways, lower values improve directness
 *  *cost = 0 makes it take the most direct non-tunnel path appropriate
 *
 * reassignRole(oldRole, newRole, work = false)
 * reassignRoom(oldRole, newRole, room, work = false)
 *  changes the creep role of creeps in oldRole to newRole, with creep.memory.working set to the appropriate value
 *  room variant restricts itself to the input room only
 *
 * createMission
 * createSquad = (missionType, missionTarget, missionRoom, assignedSpawnRoom) - all strings
 *  creates a mission object for the specific types
 *
 * sendPioneer(room, from-optional)
 *  orders a Pioneer for the target room
 *
 * claimRoom(room, from-optional)
 *  creates a mission to claim a room
 *
 * sendScout(room, from-optional)
 *  orders a scout be sent to a given room
 *
 * transport(from, to,res=energy)
 * sendExcess( res = energy)
 *  sends resource from place with excess (5k+) to place with shortage
 */


global.orderMoreEnergy = function (minAmountWanted) {
    cleanupOrders();
    updateMarketPrices();
    let terminals = _.filter(Game.structures, (str) => {
        return str.structureType === STRUCTURE_TERMINAL
    });
    if (terminals.length > 0) {
        for (term of terminals) {
            let terminalEnergy = 0;
            let storageEnergy = 0;
            let troom = term.pos.roomName;

            terminalEnergy = Game.rooms[troom].terminal.store[RESOURCE_ENERGY];
            if (Game.rooms[troom].storage) {
                storageEnergy = Game.rooms[troom].storage.store[RESOURCE_ENERGY]
            }
            let totalEnergy = terminalEnergy + storageEnergy;
            if (minAmountWanted - totalEnergy < 5000) {
                continue
            }
            if (Game.rooms[troom].controller.level >= 6 && totalEnergy < minAmountWanted && Game.market.credits > 15000 && Game.rooms[troom].energyCapacityAvailable > 900) {

                let orderThisAmount = _.round(((minAmountWanted - totalEnergy) * 1.1), -3)
                let price = _.round(1.05 * (Memory.marketBuy[RESOURCE_ENERGY]), 3)
                if (_.round(price < 0.002, 3)) {
                    price = 0.002
                }
                result = Game.market.createOrder(ORDER_BUY, RESOURCE_ENERGY, price, orderThisAmount, troom)
                if (result == 0) {
                    console.log(troom + ' has ordered ' + orderThisAmount + ' energy for a price of ' + price)
                } else {
                    console.log(troom + ' has failed to order ' + orderThisAmount + ' energy')
                }
            }
        }
    }
}
global.removeRoadsNear = function (input, range = 1) {
    if (!(input instanceof RoomPosition)) {
        var inputpos = input.pos;
    } else {
        var inputpos = input
    }
    let array = inputpos.findInRange(FIND_STRUCTURES, range, {filter: s => s.structureType === STRUCTURE_ROAD});
    array.forEach(s => s.destroy())
};
global.removeEnemyNear = function (input, range = 1) {
    if (!(input instanceof RoomPosition)) {
        var inputpos = input.pos;
    } else {
        var inputpos = input;
    }
    let array = inputpos.findInRange(FIND_HOSTILE_STRUCTURES, range, {
        filter: s => (
            !s.store || (s.store && _.sum(s.store) === 0)
        )
    });
    array.forEach(s => s.destroy())
};
global.removeFlags = function (flagname) {
    for (f in Game.flags) {
        if (Game.flags[f].name.startsWith(flagname)) {
            Game.flags[f].remove()
        }
    }
};
global.removeAllFlags = function (flagname) {
    for (f in Game.flags) {
        Game.flags[f].remove()
    }
};
global.roomCleanup = function (someRoom, amt = 100) {
    _(Game.constructionSites).filter((c) => c.pos.roomName === someRoom && c.progress < amt).forEach((c) => c.remove()).value();
};
global.allConstructionCleanup = function () {
    _(Game.constructionSites).forEach((c) => c.remove()).value();
};
global.avoidRoom = function (roomName) {
    if (!Memory.rooms[roomName]) {
        Memory.rooms[roomName] = {};
    }
    Memory.rooms[roomName].avoid = 1;
    log.notify('Traveler will now attempt to avoid room:' + roomName)
};
global.cleanupRoads = function () {
    roadsiteCleanup()
};
global.roadsiteCleanup = function () {
    let num = 0;
    for (site in Game.constructionSites) {
        let Site = Game.constructionSites[site];
        if (Site.structureType === STRUCTURE_ROAD && Site.progress < 50) {
            Site.remove();
            num++
        }
    }
    console.log('Removed ' + num + ' empty road construction sites.')
};
global.statusCheck = function () {
    let prograw = (Game.gcl.progress / Game.gcl.progressTotal) * 100;
    let prog = prograw.toFixed(3);
    console.log(prog + ' percent of the way to GCL ' + (Game.gcl.level + 1));
};
global.roomCheck = function (room) {
    if (Game.rooms[room].controller.level === 8) {
        return
    }
    let prograw = (Game.rooms[room].controller.progress / Game.rooms[room].controller.progressTotal) * 100;
    let prog = prograw.toFixed(3);
    console.log(room + ' is ' + prog + ' percent of the way to RCL ' + (Game.rooms[room].controller.level + 1));
};
global.launchNuke = function (target) {
    let nuker = global.findArmedNuker(target.pos.roomName);
    if (nuker != undefined) {
        result = nuker.launchNuke(target.pos)
        nuker.launchedNuke = true
        log.notification(nuker.pos.roomName + " attempted a nuke launch on tick " + Game.time + ". Target is room:" + target.pos.roomName + '.  Result was: ' + result)
    } else {
        console.log('No armed nuke available')
    }
};
global.countNukers = function (roomName) {
    let count = countNukersRaw(roomName);
    console.log(count.true + ' nukers in range of' + roomName)
};
global.addAlly = function (playerName) {
    if (!Memory.targetRecognition)
        Memory.targetRecognition = {allies: ['SemperRabbit', 'NihilRex', 'WarInternal'], neutral: ['Source Keeper']};
    Memory.targetRecognition.allies.push(playerName)
}
global.addNeutral = function (playerName) {
    if (!Memory.targetRecognition)
        Memory.targetRecognition = {allies: ['SemperRabbit', 'NihilRex', 'WarInternal'], neutral: ['Source Keeper']};
    Memory.targetRecognition.neutral.push(playerName)
}
global.buildRoad = function (origin, targetpos, cost) {
    var goal = {
        pos: targetpos.pos,
        range: 1
    };
    var scost = cost * 2;
    //Find path
    var path = PathFinder.search(origin.pos, goal, {
        plainCost: cost, swampCost: scost, maxOps: 12000,
        roomCallback: function (roomName) {
            //Set room to be the room being searched
            let room = Game.rooms[roomName];
            //If no visiblity in the room, return
            if (!room) return;
            //Costs will be the new costmatrix
            let costs = new PathFinder.CostMatrix;

            var structs = room.find(FIND_STRUCTURES, {
                filter: s => s.structureType != STRUCTURE_CONTAINER &&
                    s.structureType != STRUCTURE_RAMPART && s.structureType != STRUCTURE_ROAD
            });
            for (var c in structs) {
                costs.set(structs[c].pos.x, structs[c].pos.y, 0xff);
            }
            //Set all roads to cost 1
            var structs = room.find(FIND_STRUCTURES, {filter: s => s.structureType == STRUCTURE_ROAD});
            for (var c in structs) {
                costs.set(structs[c].pos.x, structs[c].pos.y, 1);
            }
            return costs;
        }
    });
    var counter = 0;
    let sites = Object.keys(Game.constructionSites).length
    for (var x in path.path) {
        //Create multi-lane road
        if (Game.rooms[path.path[x].roomName] != undefined && sites < 50) {
            Game.rooms[path.path[x].roomName].createConstructionSite(path.path[counter].x, path.path[counter].y, STRUCTURE_ROAD);
            counter = counter + 1;
            sites++
        }
    }
};
global.reassignRole = function (oldRole, newRole, work = false) {
    let nums = 0;
    for (let name in Game.creeps) {
        let creep = Game.creeps[name];
        if (creep.memory.role == oldRole) {
            delete creep.memory
            creep.memory.role = newRole;
            creep.memory.working = work;
            nums++;
        }
    }
    console.log(nums + ' creeps reassigned from ' + oldRole + ' to ' + newRole)
};
global.reassignRoom = function (oldRole, newRole, room, work = false) {
    var nums = 0;
    for (let name in Game.creeps) {
        var creep = Game.creeps[name];
        if (creep.memory.role !== oldRole) {
            continue;
        }
        if (room && creep.pos.roomName !== room) {
            continue;
        }
        delete creep.memory
        creep.memory.role = newRole;
        if (work) {
            creep.memory.working = true;
        } else {
            creep.memory.working = false;
        }
        nums++;
    }
    console.log(nums + ' creeps reassigned from ' + oldRole + ' to ' + newRole)
};
global.sendPioneer = function (mTarget, aroom = null) {
    createMission('pioneer', mTarget, mTarget, aroom);
    log.notification('Ordering a pioneer for room ' + mTarget)
}
global.sendScout = function (mTarget) {
    createMission('scout', mTarget, mTarget);
    log.notification('Ordering a scout for room ' + mTarget)
}
global.sendPoke = function (mTarget) {
    createMission('poke', mTarget, mTarget);
    log.notification('Ordering a scout for room ' + mTarget)
}
global.claimRoom = function (mTarget, aRoom = null) {
    if (Game.rooms[mTarget] && Game.rooms[mTarget].controller && Game.rooms[mTarget].controller.id) {
        createMission('claim', Game.rooms[mTarget].controller.id, mTarget, aRoom);
        sendScout(mTarget);
        sendPioneer(mTarget)
        log.notification('Ordering a claimer for room ' + mTarget)
    } else {
        log.warn('Cannot create mission to claim ' + mTarget + ' without room vision')
    }
}
global.createMission = function (mType, mTarget, mRoom, aRoom) {
    mcounter = ('' + Memory.counters.civilMissionCounter).padStart(3, 0)
    thismission = mcounter + '_' + mType + '_' + mRoom;
    Memory.missions.civil[thismission] = {}
    Memory.missions.civil[thismission].name = thismission
    Memory.missions.civil[thismission].missionType = mType;
    Memory.missions.civil[thismission].missionTarget = mTarget;
    Memory.missions.civil[thismission].missionRoom = mRoom;
    Memory.missions.civil[thismission].assignedSpawnRoom = aRoom;
    Memory.missions.civil[thismission].spawned = false;
    Memory.missions.civil[thismission].time = Game.time;
    Memory.counters.civilMissionCounter++
    return thismission
}

global.createSquad = function (mType, mTarget, mRoom, aRoom) {
    mcounter = ('' + Memory.counters.militaryMissionCounter).padStart(3, 0)
    thismission = mcounter + '_' + mType + '_' + mRoom;
    Memory.missions.military[thismission] = {}
    Memory.missions.military[thismission].name = thismission
    Memory.missions.military[thismission].missionType = mType;
    Memory.missions.military[thismission].creepTypes = [];
    Memory.missions.military[thismission].missionRoom = mRoom;
    Memory.missions.military[thismission].missionTarget = mTarget;
    Memory.missions.military[thismission].assignedSpawnRoom = aRoom;
    Memory.missions.military[thismission].spawned = false
    Memory.missions.military[thismission].time = Game.time;
    Memory.counters.militaryMissionCounter++
    switch (mType) {
        case 'assault':
            Memory.missions.military[thismission].creepTypes = ['tank', 'medic', 'medic', 'tank']
            break;
        case 'sentry':
            Memory.missions.military[thismission].creepTypes = ['sentry', 'sentry']
            break;
        case 'harass':
            Memory.missions.military[thismission].creepTypes = ['soldier', 'medic']
            break;
        case 'probe':
            Memory.missions.military[thismission].creepTypes = ['ranger']
            break;
        case 'poke':
            Memory.missions.military[thismission].creepTypes = ['soldier', 'soldier']
            break;
        case 'drain':
            Memory.missions.military[thismission].creepTypes = ['ranger', 'healer']
            break;
        case 'wreck':
            Memory.missions.military[thismission].creepTypes = ['wrecker', 'wrecker']
            break;
        case 'clear':
            Memory.missions.military[thismission].creepTypes = ['ranger', 'healer', 'ranger', 'archer']
            break;
        case 'doubledrain':
            Memory.missions.military[thismission].creepTypes = ['ranger', 'healer', 'ranger', 'healer']
            break;
        case 'powerkill':
            Memory.missions.military[thismission].creepTypes = ['soldier', 'soldier', 'healer', 'healer', 'ranger', 'healer', 'ranger', 'healer', 'archer']
            break
        case 'expedition':
        case 'mixedattack':
            Memory.missions.military[thismission].creepTypes = ['soldier', 'healer', 'healer', 'archer', 'ranger']
            break;
    }
    return thismission
}
global.transport = function (from, to, res = RESOURCE_ENERGY) {
    mission = createMission('transport', res, to, from)
    log.log('Created a mission to transport excess ' + res + ' from ' + from + ' to ' + to + ' as mission: ' + mission)
}
global.sendExcess = function (res = RESOURCE_ENERGY) {
    sendroomlist = []
    needroomlist = []
    for (roomChecked in Memory.rooms) {
        if (Memory.rooms[roomChecked].my) {
            let totroom = 0
            let excess = false
            let s = Game.rooms[roomChecked].find(FIND_STRUCTURES, {
                filter: (s) => (
                    (s.store && s.store[res] > 0
                        && s.structureType !== STRUCTURE_TOWER
                        && s.structureType !== STRUCTURE_POWER_SPAWN
                        && s.structureType !== STRUCTURE_FACTORY
                        && s.structureType !== STRUCTURE_NUKER
                        && s.structureType !== STRUCTURE_LAB
                    )
                )
            });

            for (item in s) {
                that = s[item]
                if (that.store && that.store[res] > 0) {
                    totroom += that.store[res]
                    if (that.store[res] > 5500) {
                        excess = true
                    }
                }
            }
            if (totroom < 5000) {
                needroomlist.push(roomChecked)
            }
            if (totroom > 18000 && excess) {
                sendroomlist.push(roomChecked)
            }
        }
    }
    if (sendroomlist.length > 0) {
        console.log('Balancing ' + res + ' shows needs in ' + needroomlist.length + ' rooms, using this many senders: ' + sendroomlist.length)
        for (x = 0; x < sendroomlist.length; x++) {
            let min = 50
            for (roomnum = 0; roomnum < needroomlist.length; roomnum++) {
                dist = Game.map.findRoute(sendroomlist[x], needroomlist[roomnum]).length
                if (dist < min) {
                    to = needroomlist[roomnum]
                    min = dist
                }
            }//// done with needrooms
            if (from && to) {
                transport(sendroomlist[x], to, res)
            }
        }/// done with all sendrooms
    }
}
global.resetScoresandAnchors = function () {
    removeAllFlags();
    for (room in Memory.rooms) {
        delete Memory.rooms[room].anchor
        delete Memory.rooms[room].posScore
        delete Memory.rooms[room].Scores
    }
}
