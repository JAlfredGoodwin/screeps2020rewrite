module.exports = {
    run: function (creep) {
        if (creep.memory.mission) {
            mission = creep.memory.mission
        } else {
            creep.memory.role = 'zombie'
            creep.say('ERROR')
        }
        if (creep.spawning) {
            return
        }
        if (!creep.memory.targetSource) {
            creep.memory.targetSource = mission.target
        }
        var source = Game.getObjectById(creep.memory.targetSource);

        if (creep.ticksToLive < 100 && creep.room.controller && !creep.room.controller.my) {
            creep.memory.role = 'repairer';
            creep.memory.working = true;
        }
        if (creep.memory.targetSource) {
            source = Game.getObjectById(mission.target);
        }
        if (!source) {

            sourcePos = new RoomPosition(25, 25, mission.missionRoom)
        }
        creep.memory.room = mission.missionRoom
        mineroom = creep.memory.room;
        if (creep.pos.roomName === mineroom) {
            if (creep.harvest(source) !== 0) {
                if (source && source.pos) {
                    (creep.travelTo(source, {useFindRoute: true}))
                }
            }
        } else {
            let newPos = new RoomPosition(25, 25, creep.memory.room);
            creep.travelTo(newPos, {useFindRoute: true, ignoreCreeps: true});
        }
        let dropped = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (dropped.length >= 1 && dropped[0].amount >= 500 && !checkEmptier(creep.pos.roomName) && !creep.memory.emptier) {
            creep.pickup(dropped[0]);
            createMission('empty', dropped[0].id, creep.pos.roomName)
            creep.memory.emptier = true
        }

    }
}


checkEmptier = function (room) {
    for (amission in Memory.missions.civil) {
        parts = amission.split('_');
        if (_.contains(parts, room) && _.contains(parts, 'empty') && Memory.missions.civil[amission].missionRoom === room) {
            return true
        }
    }
    return false
}
