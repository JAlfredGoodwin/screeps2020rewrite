module.exports = {
    run: function (creep) {
        thisroom = creep.pos.roomName
        //console.log(creep.name + ' is delivering')
        storage=false
        if (Game.rooms[thisroom].storage && Game.rooms[thisroom].storage.my ) {
            storage = Game.rooms[thisroom].storage
        } else {storage = false}
        if (creep.store && creep.store.energy > 0) {
            target = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (s) => s.store && s.store.getFreeCapacity(RESOURCE_ENERGY) >= (creep.store.energy * 0.1)
            });
            if (target && creep.transfer(target, RESOURCE_ENERGY) !== 0) {
                creep.travelTo(target)
                return;
            }
        }
        if (creep.store.getUsedCapacity() > 0) {
            if (storage && storage.store.getFreeCapacity() > (creep.store.getUsedCapacity() * 0.25)) {
                if (creep.pos.isNearTo(storage)) {
                    _.some(creep.carry, (amount, resource) => amount && creep.transfer(storage, resource) !== undefined)
                } else {
                    creep.travelTo(storage)
                }
            } else {
                target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (s) => s.store && s.store.getFreeCapacity() > (creep.store.getUsedCapacity() * 0.1) && (s.structureType === STRUCTURE_CONTAINER || s.structureType === STRUCTURE_TERMINAL)
                });
                if (creep.pos.isNearTo(target)) {
                    _.some(creep.carry, (amount, resource) => amount && creep.transfer(target, resource) !== undefined)
                } else {
                    creep.travelTo(target)
                }
            }
        }


    }
}
