module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (creep.memory.mission) {
            mission = creep.memory.mission
        }
        missiontarget=false
        if (mission.target) {
            missiontarget = Game.getObjectById(mission.target)
        }
        cacheTargets(creep.pos);
        if (missiontarget) {
            if (creep.attack(missiontarget) !== 0) {
                creep.travelTo(missiontarget, {movingTarget: true,brave:true})
            }
            return
        } else {
            if (creep.pos.roomName === mission.missionRoom) {
                target = creep.pos.findClosestByPath(targetcache[creep.pos.roomName].targets)
                if (target) {
                    if (creep.attack(target) !== 0) {
                        creep.travelTo(target, {brave:true})
                        return
                    }

                } else {
                    creep.say('BORED')
                    creep.memory.bored++
                    if (creep.memory.bored >= 75) {
                        delete Memory.missions.military[creep.memory.mission.mission]
                        creep.memory.role = 'zombie'
                    }
                }
            } else{
                newPos = new RoomPosition(25,25, mission.missionRoom)
                creep.travelTo(newPos,{ignoreRoads:true,useFindRoute:true, brave:true})
            }
        }
    }
}
