module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
            return
        }
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.findClosestSpawn().pos.roomName
        }
        if (creep.memory.homeRoom && creep.pos.roomName !== creep.memory.homeRoom) {
            x = new RoomPosition(24, 24, creep.memory.homeRoom)
            creep.travelTo(x, {range: 10})
        }

        if (creep.memory.working === true && creep.store.energy === 0) {
            creep.memory.ticks = undefined;
            creep.memory.working = false;
            creep.memory.target = false;
        }
        if (creep.memory.working === false && creep.store.energy === creep.carryCapacity) {
            creep.memory.working = true;
        }
        let structure = {}
        if (creep.memory.working === true) {

            if (!creep.memory.target) {
                structure = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (s) => s.structureType !== STRUCTURE_ROAD && (((s.hits + 199) < s.hitsMax) && s.structureType !== STRUCTURE_WALL && s.structureType !== STRUCTURE_RAMPART)
                        || ((s.hits < 750) && s.my && s.structureType === STRUCTURE_RAMPART)
                        || ((s.hits < 500) && s.structureType === STRUCTURE_WALL)
                });
                if (!structure) {
                    structure = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (s) => (((s.hits + 399) < s.hitsMax) && ((s.hits < (Memory.Settings.WallLimit * 0.05)) && s.my && s.structureType === STRUCTURE_RAMPART)
                            || ((s.hits < (Memory.Settings.WallLimit * 0.05)) && s.structureType === STRUCTURE_WALL))
                    });
                }
                if (structure) {
                    creep.memory.target = structure.id;
                }
            } else {
                structure = Game.getObjectById(creep.memory.target);
            }

            if (structure instanceof Structure) {
                if (creep.pos.inRangeTo(structure, 3)) {
                    if (creep.repair(structure) === 0) {
                        creep.memory.ticks++
                        creep.travelTo(structure, {range: 1, ignoreCreeps: true, maxRooms: 1});
                    }
                } else {
                    creep.travelTo(structure, {range: 3, ignoreCreeps: false, maxRooms: 1});
                }
            } else {
                delete creep.memory.target
            }
        }

        if (creep.memory.working === false) {
            conta = creep.getEasyEnergy()
            if (creep.pos.isNearTo(conta)) {
                creep.withdraw(conta, RESOURCE_ENERGY)
            } else {
                creep.travelTo(conta, {ignoreRoads: (creep.carry.energy === 0), maxRooms: 1});
            }
        }
        if (creep.memory.ticks >= 25) {
            creep.memory.ticks = 0;
            creep.memory.target = undefined;
        }

        if (structure && structure.hits === structure.hitsMax) {
            creep.memory.target = undefined;
            creep.memory.ticks = 0
        }
        let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (droppedEnergy.length >= 1 && droppedEnergy[0].resourceType === RESOURCE_ENERGY) {
            creep.pickup(droppedEnergy[0]);
        }
        if (creep.store.energy > 0) {
            let roadspot = creep.pos.lookFor(LOOK_STRUCTURES, {filter: (c) => c.structureType === STRUCTURE_ROAD});
            if (roadspot[0] && roadspot[0].hits < roadspot[0].hitsMax) {
                creep.repair(roadspot[0]);
            }
        }
        if (!creep.memory.target) {
            require('role.builder').run(creep);
        }
    }
}
