module.exports = {
    run: function () {
        global.census = {};
        Game.cpu.bucket;
        for (name in Game.creeps) {

            let creep = Game.creeps[name];
            if(!creep.memory.role){creep.memory.role='zombie'}
            creep.memory.role.toLowerCase()
            if (creep.memory.role !== 'eminer' && creep.memory.role !== 'mminer' && !creep.spawning) {
                if (creep.ticksToLive <= 80 && creep.room.controller && creep.room.controller.my) {
                    creep.memory.role = 'zombie';
                }
            }


            //try {
                require('role.'+creep.memory.role).run(creep)

            /*} catch (e) {
                if (true) {
                    console.log(creep.name + ': ' + e)
                }
            }*/
            if (!global.census[creep.memory.role]) {
                global.census[creep.memory.role] = 0
            }
            global.census[creep.memory.role]++;
            if (!global.census[creep.pos.roomName + "_" + creep.memory.role]) {
                global.census[creep.pos.roomName + "_" + creep.memory.role] = 0
            }
            global.census[creep.pos.roomName + "_" + creep.memory.role]++;
        }
    }
};
