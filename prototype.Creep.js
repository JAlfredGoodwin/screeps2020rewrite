module.exports = function () {
    Creep.prototype.moveToRange = function (target, range) {

        if (!range) {
            range = 1;
        }
        let pos = target.pos || target;
        this.moveTo({pos: pos, range: range}, {
            maxRooms: 1,
        });

    };

    Creep.prototype.clearCachedPath = function () {
        delete this.memory._newMoveTo.pathCache
    };


    Creep.prototype.getObjectPosById = function (id) {
        var pos = Game.getObjectById(id);
        if (!(pos == null || pos == undefined)) {
            return pos.pos
        }

        for (let r in Memory.rooms) {
            if (!_.isUndefined(Memory.rooms[r].sources)) {
                for (let i in Memory.rooms[r].sources) {
                    var test = Memory.rooms[r].sources[i];
                    if (test.id == id || i == id) {
                        return new RoomPosition(test.x, test.y, test.roomName);
                    }
                }
            }
            if (!_.isUndefined(Memory.rooms[r].mineralDeposits)) {
                for (let i in Memory.rooms[r].mineralDeposits) {
                    var test = Memory.rooms[r].mineralDeposits[i];
                    if (test.id == id || i == id) {
                        return new RoomPosition(test.x, test.y, test.roomName);
                    }
                }
            }
        }
        return undefined;
    };
    Creep.prototype.getActiveBodyparts = function (type) {
        var count = 0;
        for (let i = this.body.length - 1; i >= 0; i--) {
            if (this.body[i].hits <= 0)
                break;
            if (this.body[i].type === type)
                count++;
        }
        return count;
    };

    Creep.prototype.hasActiveBodypart = function (type) {
        for (let i = this.body.length - 1; i >= 0; i--) {
            if (this.body[i].hits <= 0)
                break;
            if (this.body[i].type === type)
                return true;
        }
        return false;
    };

    Creep.prototype.moveOffRoad = function () {
        let atPos = this.pos.lookFor(LOOK_STRUCTURES);
        for (let s of atPos) {
            if (s.structureType === STRUCTURE_ROAD) {
                let offroadPosition = this.getOffroadMovePosition(this);
                this.travelTo(offroadPosition);
                return true;
            }
        }
        return false;
    }

    Creep.prototype.getOffroadMovePosition = function (creep) {
        let path = PathFinder.search(creep.pos, {pos: creep.pos, range: 10}, {
            plainCost: 1,
            swampCost: 2,
            flee: true,
            roomCallback: getOffroadRoomCallback,
            maxRooms: 1
        }).path;
        for (let pos of path) {
            let structs = pos.lookFor(LOOK_STRUCTURES);
            let creeps = pos.lookFor(LOOK_CREEPS);
            if (structs.length === 0 && creeps.length === 0) {
                return pos;
            }
        }
        return path[0];
    }

    Creep.prototype.findLoadedStorage = function () {
        const storages = _.filter(Game.structures, s => s.structureType === STRUCTURE_STORAGE && s.store[RESOURCE_ENERGY] >= (this.carryCapacity * 5));
        return this.pos.findClosestByPathFinder(storages, s => ({pos: s.pos, range: 1})).goal;
    };

    Creep.prototype.isBoosted = function () {
        return _.any(this.body, p => p.boost != null);
    };
    Creep.prototype.fleeTarget = function (target) {
        if (target instanceof RoomPosition) {
            path = getFleeMove(this, target)
        } else {
            path = getFleeMove(this, target.pos)
        }
        this.moveTo(path)
    }
    Creep.prototype.getEasyEnergy = function () {
        let d = this.room.find(FIND_DROPPED_RESOURCES, {
            filter: (yu) => (yu.resourceType === RESOURCE_ENERGY && yu.amount > 50)
        })
        let r = this.room.find(FIND_RUINS, {
            filter: (sr) => sr.store && sr.store.energy > 0
        })
        let t = this.room.find(FIND_TOMBSTONES, {
            filter: (st) => st.store && st.store.energy > 0
        })
        let s = this.room.find(FIND_MY_STRUCTURES, {
            filter: (s) => (
                (s.store && s.store.energy >= (this.store.getFreeCapacity() * 0.2)
                    && s.structureType !== STRUCTURE_SPAWN
                    && s.structureType !== STRUCTURE_TOWER
                    && s.structureType !== STRUCTURE_POWER_SPAWN
                    && s.structureType !== STRUCTURE_FACTORY
                    && s.structureType !== STRUCTURE_NUKER
                    && s.structureType !== STRUCTURE_LAB
                    && s.structureType !== STRUCTURE_EXTENSION
                )
            )
        });
        let e = this.room.find(FIND_STRUCTURES, {
            filter: (s) => (
                (s.store && s.store.energy > (this.store.getFreeCapacity() * 0.75)
                    && !s.my
                )
            )
        });

        let all = [...d, ...r, ...t, ...s, ...e]
        let y = this.pos.findClosestByPath(all)
        //console.log(this.name + ' found ' + all.length + 'sources in room ' + this.pos.roomName)
        //console.log(y.structureType + ' ' + y.pos)
        return y;
    };
    Creep.prototype.getDroppedResources = function () {
        let d = this.room.find(FIND_DROPPED_RESOURCES, {
            filter: (yu) => (yu.amount > 5)
        })
        let r = this.room.find(FIND_RUINS, {
            filter: (sr) => sr.store && sr.store.getUsedCapacity() > 5
        })
        let t = this.room.find(FIND_TOMBSTONES, {
            filter: (st) => st.store && st.store.getUsedCapacity() > 5
        })

        let all = [...d, ...r, ...t]
        let y = this.pos.findClosestByPath(all)
        //console.log(this.name + ' found ' + all.length + 'sources in room ' + this.pos.roomName)
        //console.log(y.structureType + ' ' + y.pos)
        return y;
    };
    Creep.prototype.grabEasyEnergy = function () {
        that = this
        t = that.getEasyEnergy()
        that.grabResource(t,RESOURCE_ENERGY)
        delete that.memory._trav
    }
    Creep.prototype.grabResource = function (targetId, res = RESOURCE_ENERGY) {
        if (targetId && this.pos.isNearTo(targetId)) {
            if (targetId instanceof Resource) {
                this.pickup(targetId)
            } else {
                this.withdraw(targetId, res)
            }
        } else {
            this.travelTo(targetId, {ignoreRoads: (_.sum(this.store) === 0)})
        }
    }
    Creep.prototype.grabExcessResource = function (res = RESOURCE_ENERGY) {
        targetId = this.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (s) => s.store && s.store[res] > 1500 && s.structureType !== STRUCTURE_NUKER && s.structureType !== STRUCTURE_LAB
        });
        if (targetId && this.pos.isNearTo(targetId)) {
            if (targetId instanceof Resource) {
                this.pickup(targetId)
            } else {
                this.withdraw(targetId, res)
            }
        } else {
            this.travelTo(targetId, {offRoad: (_.sum(this.store) === 0)})
        }
    }
    Creep.prototype.getSpecificResource = function (res = RESOURCE_ENERGY) {
        let d = this.room.find(FIND_DROPPED_RESOURCES, {
            filter: (yu) => (yu.resourceType === res && yu.amount > 5)
        })
        let r = this.room.find(FIND_RUINS, {
            filter: (sr) => sr.store && sr.store[res] > 0
        })
        let t = this.room.find(FIND_TOMBSTONES, {
            filter: (st) => st.store && st.store[res] > 0
        })
        let s = this.room.find(FIND_MY_STRUCTURES, {
            filter: (s) => (
                (s.store && s.store[res] >= (this.store.getFreeCapacity() * 0.2)
                    && s.structureType !== STRUCTURE_POWER_SPAWN
                    && s.structureType !== STRUCTURE_FACTORY
                    && s.structureType !== STRUCTURE_NUKER
                )
            )
        });
        let e = this.room.find(FIND_STRUCTURES, {
            filter: (s) => (
                (s.store && s.store[res] > (this.store.getFreeCapacity() * 0.15)
                    && !s.my
                )
            )
        });

        let all = [...d, ...r, ...t, ...s, ...e]
        let y = this.pos.findClosestByPath(all)
        //console.log(this.name + ' found ' + all.length + 'sources in room ' + this.pos.roomName)
        //console.log(y.structureType + ' ' + y.pos)
        return y;
    };
    Creep.prototype.grabSpecificResource = function (res) {
        that = this
        t = that.getSpecificResource(res)
        that.grabResource(t,res)
        delete that.memory._trav
    }


};

function getOffroadRoomCallback(roomName) {
    let room = Game.rooms[roomName];
    if (!room)
        return new PathFinder.CostMatrix;
    let costs = new PathFinder.CostMatrix;
    room.find(FIND_STRUCTURES).forEach(function (structure) {
        if (structure.structureType === STRUCTURE_ROAD) {
            costs.set(structure.pos.x, structure.pos.y, 20);
        } else if (structure.structureType !== STRUCTURE_CONTAINER &&
            (structure.structureType !== STRUCTURE_RAMPART ||
                !(structure instanceof OwnedStructure && structure.my))) {
            costs.set(structure.pos.x, structure.pos.y, 0xff);
        }
    });
    room.find(FIND_CREEPS).forEach(function (creep) {
        costs.set(creep.pos.x, creep.pos.y, 0xff);
    });
    return costs;
}

function getKitingRoomCallback(roomName) {
    let room = Game.rooms[roomName];
    if (!room)
        return new PathFinder.CostMatrix;
    let costs = new PathFinder.CostMatrix;
    room.find(FIND_STRUCTURES).forEach(function (structure) {
        if (structure.structureType === STRUCTURE_ROAD) {
            costs.set(structure.pos.x, structure.pos.y, 2);
        } else if (structure.structureType !== STRUCTURE_CONTAINER &&
            (structure.structureType !== STRUCTURE_RAMPART ||
                !(structure instanceof OwnedStructure && structure.my))) {
            costs.set(structure.pos.x, structure.pos.y, 0xff);
        }
    });
    room.find(FIND_HOSTILE_CREEPS, {
        filter: function (c) {
            return c.getActiveBodyparts(ATTACK) > 0 || c.getActiveBodyparts(RANGED_ATTACK) > 0 || c.getActiveBodyparts(HEAL) > 0;
        }
    }).forEach(function (hostileCreep) {
        for (let x = -3; x < 4; x++) {
            for (let y = -3; y < 4; y++) {
                costs.set(hostileCreep.pos.x, hostileCreep.pos.y, 10);
                if (Math.abs(x) < 3 && Math.abs(y) < 3) {
                    costs.set(hostileCreep.pos.x, hostileCreep.pos.y, 20);
                }
            }
        }
    });
    room.find(FIND_CREEPS).forEach(function (creep) {
        costs.set(creep.pos.x, creep.pos.y, 0xff);
    });
    return costs;
}

function getFleeMove(creep, position) {
    return PathFinder.search(creep.pos, {pos: position, range: 7}, {
        plainCost: 1,
        swampCost: 10,
        flee: true,
        roomCallback: getKitingRoomCallback,
        maxRooms: 1
    }).path[0];
}

