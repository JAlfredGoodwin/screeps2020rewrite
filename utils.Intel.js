global.intelRun = function () {
    if (!global.hostileCount) {
        global.hostileCount = {}
    }
    if (!Memory.rooms) {
        Memory.rooms = {}
    }
    explan = false
    for (let thisroom in Game.rooms) {
        let thisroomobj = Game.rooms[thisroom]
        if (!Memory.rooms[thisroom]) {
            type = getRoomType(thisroom)
            Memory.rooms[thisroom] = {};
            if (type === 'SourceKeeper') {
                Memory.rooms[thisroom].sk = true
            }
            if (type === 'Highway') {
                Memory.rooms[thisroom].hway = true;
            }
            Memory.rooms[thisroom].neighbors = getRoomsOneAway(thisroom);

        }
        if (!Memory.rooms[thisroom].updateTime || (Game.time - Memory.rooms[thisroom].updateTime) > 500) {
            Memory.rooms[thisroom].updateTime = Game.time + 1;
        }
        if (!Memory.rooms[thisroom].sources) {
            let sourceList = thisroomobj.find(FIND_SOURCES);
            Memory.rooms[thisroom].sources = sourceList
            evaluateRoomForMining(thisroom);
        }
        if (!Memory.rooms[thisroom].mineral) {
            let mineral = thisroomobj.find(FIND_MINERALS);
            if (mineral.length > 0) {
                Memory.rooms[thisroom].mineral = mineral[0].id;
                Memory.rooms[thisroom].mineralType = mineral[0].mineralType;
                Memory.rooms[thisroom].mineralDensity = mineral[0].density;
                Memory.rooms[thisroom].mineralAmount = mineral[0].mineralAmount;
            }
        }
        if (Memory.rooms[thisroom].hway) {
            if (Memory.Settings.Misc.highlevel > 4 && Memory.Settings.miningDeposits) {
                sweepForDeposits(thisroom)
            }
            if (Memory.Settings.Misc.highlevel > 7 && Memory.Settings.miningPower) {
                sweepForPower(thisroom)
            }
        }

        if (thisroomobj.controller && thisroomobj.controller.owner && thisroomobj.controller.my) {
            Memory.rooms[thisroom].my = true
            if (!Memory.rooms[thisroom].level || Memory.rooms[thisroom].level !== thisroomobj.controller.level) {
                Memory.rooms[thisroom].level = thisroomobj.controller.level
                if (Memory.Settings.Misc.highlevel < thisroomobj.controller.level) {
                    Memory.Settings.Misc.highlevel = thisroomobj.controller.level
                }
                buildRoomCore(thisroom)
            }
            if (Memory.rooms[thisroom].level > 5 && (!Memory.rooms[thisroom].extensions || !Memory.rooms[thisroom].extensions.fourth)) {
                if (!explan) {
                    extensionPlanner(thisroom)
                    explan = true
                    console.log('Planning expansions for ' + thisroom)
                }

            }
            if (!Memory.rooms[thisroom].pioneercheck || Memory.rooms[thisroom].pioneercheck < Game.time) {
                pioneercheck = thisroomobj.controller.pos.findClosestByPath(FIND_MY_SPAWNS)
                dyingcheck = false
                if ((!thisroomobj.storage || !thisroomobj.storage.energy < 300) && thisroomobj.energyAvailable < thisroomobj.energyCapacityAvailable && thisroomobj.energyAvailable < 300) {
                    dyingcheck = true
                }
                if (pioneercheck == null || dyingcheck === true) {
                    sendPioneer(thisroom)
                }
                Memory.rooms[thisroom].pioneercheck = Game.time + 1500
                Memory.rooms[thisroom].extracting = true
                evaluateRoomForMining(thisroom)

            }

        } else {
            checkOwnership(thisroom);
        }
        ////// find hostiles verify
        if (Memory.rooms[thisroom].extracting) {
            let hostiles = thisroomobj.find(FIND_HOSTILE_CREEPS, {filter: (s) => !isNeutral(s)})
            if (hostiles.length > 0) {
                Memory.Threats = true
                global.hostileCount[thisroom] = hostiles.length
                //log.warn(hostiles.length + ' hostile creeps found in ' + thisroom)
                if (Memory.rooms[thisroom].extracting === true && hostiles.length <= 3) {
                    if (!Memory.rooms[thisroom].defender || Memory.rooms[thisroom].defender < Game.time - 1000) {
                        Memory.rooms[thisroom].defender = Game.time
                        createSquad('sentry', hostiles[0].id, thisroom)
                        log.warn(thisroom + ' is requesting a defender')
                    }
                }
                if (Memory.rooms[thisroom].extracting === true && hostiles.length > 3) {
                    if (!Memory.rooms[thisroom].defender || Memory.rooms[thisroom].defender < Game.time - 1000) {
                        Memory.rooms[thisroom].defender = Game.time
                        createSquad('clear', undefined, thisroom)
                        log.warn(thisroom + ' is requesting immediate aid')
                    }
                }
            }
            let core = thisroomobj.find(FIND_HOSTILE_STRUCTURES, {filter: (s) => !isNeutral(s)})
            if (core.length > 0) {
                if (!global.hostileCount) {
                    global.hostileCount
                }
                if (!global.hostileCount[thisroom]) {
                    global.hostileCount[thisroom] += core.length
                }
                global.hostileCount[thisroom] += core.length
                //log.warn(core.length + ' hostile structures found in ' + thisroom)
                for (x in core) {
                    if (core[x].structureType === STRUCTURE_INVADER_CORE) {
                        if (!Memory.rooms[thisroom].coreremover || Memory.rooms[thisroom].coreremover < Game.time - 1000 && !Memory.rooms[thisroom].sk) {
                            Memory.rooms[thisroom].coreremover = Game.time
                            reassignRoom('eminer', 'zombie', thisroom)
                            createSquad('poke', core[x].id, thisroom)
                            createSquad('poke', core[x].id, thisroom)
                            if (Game.rooms[thisroom].controller) {
                                createMission('reserve', Game.rooms[thisroom].controller.id, thisroom)
                            }
                            log.notification(thisroom + ' is requesting removal of an invader core')

                        }
                    }
                }
            }
        }
    }
}
global.observerRun = function () {
    let list = [];
    for (room in Memory.rooms) {
        thatroom = Memory.rooms[room];
        thatroomtime = Memory.rooms[room].updateTime;
        list.push({room: room, time: thatroomtime});
    }
    list = _.sortByOrder(list, 'time');
    for (c in Game.structures) {
        if (Game.structures[c].structureType === STRUCTURE_OBSERVER) {
            for (x = 0; x < list.length; x++) {
                targetRoomName = list[x].room
                if (Game.map.getRoomLinearDistance(Game.structures[c].pos.roomName, targetRoomName) <= 10 && Game.map.getRoomStatus(targetRoomName).status === Game.map.getRoomStatus(Game.structures[c].pos.roomName).status) {
                    Game.structures[c].observeRoom(targetRoomName)
                    list.splice(x, 1)
                    x = x - 1
                    console.log(Game.structures[c].pos.roomName + ' is observing ' + targetRoomName)
                    break
                }
            }
        }
    }
}
global.buildSourcesForMining = function () {
    if (!Memory.CurrentlyMining) {
        Memory.CurrentlyMining = []
    }
    for (thisroom in Memory.rooms) {
        if (Memory.rooms[thisroom].extracting) {
            if (Memory.rooms[thisroom].owner && !Memory.rooms[thisroom].my && Memory.rooms[thisroom].owner !== WHOAMI) {
                Memory.rooms[thisroom].extracting = false;
                for (thissource in Memory.rooms[thisroom].sources) {
                    let sid = Memory.rooms[thisroom].sources[thissource].id
                    for (x = 0; x < Memory.CurrentlyMining.length; x++) {
                        if (Memory.CurrentlyMining[x] === sid) {
                            Memory.CurrentlyMining.splice(x, 1)
                        }
                    }
                }
                continue
            }
            for (thissource in Memory.rooms[thisroom].sources) {
                if (Memory.rooms[thisroom].sources[thissource].id && !_.includes(Memory.CurrentlyMining, Memory.rooms[thisroom].sources[thissource].id)) {
                    let sid = Memory.rooms[thisroom].sources[thissource].id
                    Memory.CurrentlyMining.push(sid)
                    Memory.rooms[thisroom].extracting = true;
                }
            }
        }
    }
}
global.evaluateSourcesForMining = function () {
    for (thisroom in Memory.rooms) {
        if (/*Memory.rooms[thisroom].extracting === true ||*/ Memory.rooms[thisroom].sk) {
            return
        }
        if (Memory.rooms[thisroom].my === true) {
            Memory.rooms[thisroom].extracting = true;
            //return
        }
        if (Game.rooms[thisroom] && Game.rooms[thisroom].controller && Memory.rooms[thisroom].owner && Memory.rooms[thisroom].owner !== WHOAMI) {
            Memory.rooms[thisroom].extracting = false;
            return
        }
        evaluateRoomForMining(thisroom)
    }
}
global.evaluateRoomForMining = function (thisroom) {
    if (!Game.rooms[thisroom]) {
        return
    }
    if (!Memory.CurrentlyMining) {
        Memory.CurrentlyMining = []
    }
    if (Game.rooms[thisroom].controller && Memory.rooms[thisroom].owner && Memory.rooms[thisroom].owner !== WHOAMI) {
        Memory.rooms[thisroom].extracting=false
        for (thissource in Memory.rooms[thisroom].sources) {
            let sid = Memory.rooms[thisroom].sources[thissource].id
            for (x = 0; x < Memory.CurrentlyMining.length; x++) {
                if (Memory.CurrentlyMining[x] === sid) {
                    Memory.CurrentlyMining.splice(x, 1)
                }
            }
        }
        return
    }
    if (Memory.rooms[thisroom].sk) {
        log.warn('SK Room marked for mining: ' + thisroom)
        return
    }
    if (Memory.rooms[thisroom].extracting === true) {
        //return;
    }
    if (Memory.rooms[thisroom].my === true) {
        Memory.rooms[thisroom].extracting = true;
        //return;
    }
    for (thissource in Memory.rooms[thisroom].sources) {
        if (Memory.rooms[thisroom].sources[thissource].id) {
            let sid = Memory.rooms[thisroom].sources[thissource].id
            sobj = Game.getObjectById(sid)
            nearspawn = sobj.pos.findClosestSpawn();
            var path = sobj.pos.findPathTo(nearspawn);
            Memory.sources[sid] = {};
            Memory.sources[sid].id = sid;
            Memory.sources[sid].pos = sobj.pos;
            roomsdist = Game.map.getRoomLinearDistance(sobj.pos.roomName, nearspawn.pos.roomName)
            if (roomsdist < 3 && path.length < Memory.Settings.MiningDistanceInSteps && !_.includes(Memory.CurrentlyMining, sid)) {
                Memory.CurrentlyMining.push(sid)
                Memory.rooms[thisroom].extracting = true
                console.log('Now Mining source in room ' + sobj.pos.roomName + ' at distance ' + path.length)
            }
        }
    }
}
global.updateRoomScores = function () {
    if (!global.roomScores) {
        global.roomScores = {}
    }
    updateMarketPrices()
    max = 0;
    min = 500;
    maxRoom = 'empty'
    for (let room in Memory.rooms) {
        if (Game.rooms[room]
            && Game.rooms[room].controller
            && Memory.rooms[room].anchor
            && Memory.rooms[room].posScore) {
            let pscore = Memory.rooms[room].posScore
            if (pscore < 0) {
                global.roomScores[room] = pscore;
                continue;
            }
            switch (Memory.rooms[room].mineralDensity) {
                case 1:
                    densityModifer = 10
                    break;
                case 2:
                    densityModifer = 50
                    break;
                case 3:
                    densityModifer = 100
                    break;
                case 4:
                    densityModifer = 250
                    break;
            }
            minprice = Memory.marketBuy[Memory.rooms[room].mineralType] * densityModifer

            if (Memory.rooms[room].mineralType === RESOURCE_CATALYST) {
                mineralScore = (minprice + .001) * 3;
            } else {
                mineralScore = minprice
            }
            neighborScore = Memory.rooms[room].sources.length
            for (neighbor of Memory.rooms[room].neighbors) {
                neighborScore += checkOwnershipScore(neighbor)
            }

            total = ((Math.sqrt(300 - pscore) + mineralScore) * neighborScore)
            Memory.rooms[room].scores = {}
            Memory.rooms[room].scores.TotalScore = total
            Memory.rooms[room].scores.MineralScore = mineralScore
            Memory.rooms[room].scores.Neighbors = neighborScore
            global.roomScores[room] = total
            if (total > max && !Memory.rooms[room].my && getRoomManhattanDistance(Game.rooms[room].controller.pos.findClosestSpawn().pos.roomName, room) < 8) {
                max = total.toFixed(0)
                maxRoom = room
            }
            if (total < min) {
                min = total.toFixed(0)
            }
        }
    }
    log.notify('Room Scores updated, max score of: ' + max + ', for room ' + maxRoom + ', and minimum score of: ' + min)
    Memory.RoomScores = global.roomScores
    if (!Memory.Settings.Misc.nextRoomScore || max > Memory.Settings.Misc.nextRoomScore) {
        Memory.Settings.Misc.nextRoom = maxRoom
        Memory.Settings.Misc.nextRoomScore = max
    }

}
checkOwnershipScore = function (room) {
    let t = checkOwnership(room)

    switch (t) {
        case  'allied':
            return -3
        case 'neutral':
            return -6
        case 'mine':
            return -8
        case 'highway':
            return 1
        case 'sk':
            return 1
        case 'enemy':
            return -5;
        default :
            return 0
    }
}
checkOwnership = function (room) {
    if (Memory.rooms[room]) {
        if (Memory.rooms[room].mine) {
            return 'mine'
        }
        if (Memory.rooms[room].sk) {
            return 'sk'
        }
        if (Memory.rooms[room].hway) {
            return 'highway'
        }
        if (Memory.rooms[room].owner && Memory.rooms[room].updateTime >= Game.time) {
            owner = Memory.rooms[room].owner
            return isAllied(owner) ? 'allied' : isNeutral(owner) ? 'neutral' : 'enemy';
        }
    }
    if (Game.rooms[room]) {

        thisroomobj = Game.rooms[room]

        if (thisroomobj.my) {
            return 'mine'
        }
        if (thisroomobj.controller) {
            if (thisroomobj.controller.owner) {
                owner = thisroomobj.controller.owner
                Memory.rooms[room].owner = owner
                Memory.rooms[room].updateTime = Game.time + 500
                return isAllied(owner) ? 'allied' : isNeutral(owner) ? 'neutral' : 'enemy';
            }
            if (thisroomobj.controller.reservation && thisroomobj.controller.owner != 'Invader') {
                Memory.rooms[room].owner = thisroomobj.controller.reservation.username;
                Memory.rooms[room].updateTime = thisroomobj.controller.reservation.ticksToEnd + Game.time
                owner = thisroomobj.controller.reservation.username;
                if (isNeutral(owner)) {
                    return 'neutral'
                }
            }

        } else {
            if (isHighway(room)) {
                return 'highway'
            }
            if (getRoomType(room) === 'SourceKeeper') {
                return 'keeper'
            }
        }
    }
}
global.manageReceptions = function () {
    if (!Memory.tradeBalance) {
        Memory.tradeBalance = {};
    }
    if (Game.market.incomingTransactions.length == 0) {
        return;
    }
    if (!Memory.lastIncomingTransaction) {
        Memory.lastIncomingTransaction = 0;
    }
    for (let trans of Game.market.incomingTransactions) {
        if (trans.time <= Memory.lastIncomingTransaction) {
            break;
        }
        if (!trans.sender) {
            continue;
        }
        if (!Memory.tradeBalance[trans.sender.username]) {
            this.clearTradeBalance(trans.sender.username);
        }
        if (!Memory.tradeBalance[trans.sender.username][trans.resourceType]) {
            Memory.tradeBalance[trans.sender.username][trans.resourceType] = 0;
        }
        Memory.tradeBalance[trans.sender.username][trans.resourceType] += trans.amount;
        console.log(String(trans.time) + ": " + trans.sender.username + " at " + trans.from + " to " + trans.to + ", " + trans.amount + " of " + trans.resourceType + ".");
    }
    Memory.lastIncomingTransaction = Game.market.incomingTransactions[0].time;
},
    clearTradeBalance = function (username) {
        Memory.tradeBalance[username] = {};
    },
    reportTradeBalance = function (username) {
        if (!Memory.tradeBalance[username]) {
            console.log("No recorded receptions from " + username + ".");
            return;
        }
        console.log("Received resources from " + username + ":");
        for (let key of Object.keys(Memory.tradeBalance[username])) {
            console.log(" -- " + key + ": " + Memory.tradeBalance[username][key]);
        }
    };
global.getRoomsOneAway = function (baseroom) {
    let roomsOneAway = [];
    let exits = Game.map.describeExits(baseroom);
    _.forEach(exits, function (room) {
        if (room !== undefined) {
            roomsOneAway.push(room);
        }
    });
    return roomsOneAway;
}
module.exports = {
    reportTradeBalance,
    clearTradeBalance,
    manageReceptions,
    checkOwnership,
    evaluateSourcesForMining,
    evaluateRoomForMining,
    buildSourcesForMining,
    intelRun
}
//STRUCTURE_POWER_BANK

sweepForDeposits = function (roomName) {
    t = Game.rooms[roomName].find(FIND_DEPOSITS)
    if (t.length > 0) {
        for (x = 0; x < t.length; x++) {
            newDeposit(t[x].id)
        }
    }
}
sweepForPower = function (roomName) {
    let powerbank = Game.rooms[roomName].find(FIND_STRUCTURES, {filter: (s) => (s.structureType === STRUCTURE_POWER_BANK)})
    if (powerbank.length > 0) {
        newBank(powerbank[0].id)
        readyForHaul(powerbank[0].id)
    }
}
newBank = function (id) {
    thisbank = Game.getObjectById(id)
    if (thisbank.ticksToDecay > 1500 && thisbank.hits > 250000 && (!Memory.powerbanks[id] || !Memory.powerbanks[id].mission)) {
        Memory.powerbanks[id] = {}
        Memory.powerbanks[id].mission = createSquad('powerkill', id, thisbank.pos.roomName);
        log.notify('Attacking bank in ' + thisbank.pos.roomName)
    }
    if (thisbank.ticksToDecay < 500) {
        delete Memory.powerbanks[id]
    }
}
readyForHaul = function (id) {
    bank = Game.getObjectById(id)
    if (!Memory.powerbanks[id].haul && bank.hits < 10000 && Memory.powerbanks[id].mission) {
        numberHaul = Math.ceil((bank.power / 600))
        Memory.powerbanks[id].haul = true
        for (x = 0; x < numberHaul; x++) {
            createMission('rob', RESOURCE_POWER, bank.pos.roomName)
        }
        log.notify('Robbing bank in ' + bank.pos.roomName)
    }
}
newDeposit = function (id) {
    deposit = Game.getObjectById(id)
    if (deposit.ticksToDecay > 1500 && (!Memory.deposits[id] || !Memory.deposits[id].mission)) {
        Memory.deposits[id] = {}
        Memory.deposits[id].mission = createMission('mineral', id, deposit.pos.roomName);
        Memory.deposits[id].haul = createMission('rob', deposit.depositType, deposit.pos.roomName)
        log.notification('Mining deposit of ' + deposit.type + 'in ' + deposit.pos.roomName)
        Memory.sources[id] = {};
        Memory.sources[id].id = id;
        Memory.sources[id].pos = deposit.pos;
    } else {
        if (deposit.ticksToDecay < 500) {
            delete Memory.sources[id]
            delete Memory.deposits[id]
        }
    }
}
