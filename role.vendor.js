module.exports = {
    run: function (creep) {

        if (creep.spawning) {
            return
        }
        if (!creep.memory.settled) {
            if (creep.ticksToLive < 1400){
                creep.memory.role='courier'
            }
            if (!creep.memory.pos) {
                anchor = Memory.rooms[creep.pos.roomName].anchor
                if (!anchor) {
                    creep.memory.role = 'courier';
                    return
                }
                let splitpos = anchor.split('_');
                creep.memory.pos = {}
                creep.memory.pos.x = Number(splitpos[0])
                creep.memory.pos.y = Number(splitpos[1]) + 2

            }
            properpos = new RoomPosition(creep.memory.pos.x, creep.memory.pos.y, creep.pos.roomName)

            if (creep.pos.isEqualTo(properpos)) {
                creep.memory.settled = true
            } else {
                creep.travelTo(properpos)
                creep.say('OMW')
            }
        }
        term = {bob: 1}
        powerSpawn = {bob: 1}
        nuker = {bob: 1}
        link = {bob: 1}
        if (creep.room.storage) {
            st = creep.room.storage
        }
        if (creep.room.terminal) {
            term = creep.room.terminal;
        }


        if (!creep.memory.nuker && creep.room.controller.level === 8 && Game.time % 50 === 5) {
            nukez = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (s) => s.structureType === STRUCTURE_NUKER
            });
            if (nukez) {
                creep.memory.nuker = nukez.id
            }
        }
        if (!creep.memory.powerSpawn && creep.room.controller.level === 8 && Game.time % 50 === 6) {
            nukez = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (s) => s.structureType === STRUCTURE_POWER_SPAWN
            });
            if (nukez) {
                creep.memory.powerSpawn = nukez.id
            }
        }
        if (!creep.memory.link && creep.room.controller.level === 5 && Game.time % 50 === 4) {
            links = creep.pos.findInRange(FIND_MY_STRUCTURES, 1, {
                filter: (s) => s.structureType === STRUCTURE_LINK
            });
            if (links.length > 0) {
                creep.memory.link = links[0].id
            }
        }
        if (creep.memory.link) {
            link = Game.getObjectById(creep.memory.link)
        }
        if (creep.memory.nuker) {
            nuker = Game.getObjectById(creep.memory.nuker)
        }
        if (creep.memory.powerSpawn) {
            powerSpawn = Game.getObjectById(creep.memory.powerSpawn)
        }

        ////Power Spawn
        if (powerSpawn.store && powerSpawn.store[RESOURCE_POWER] > 0 && creep.store[RESOURCE_POWER] > 0) {
            creep.transfer(powerSpawn, RESOURCE_POWER);
            return
        }
        if (powerSpawn.store && powerSpawn.store[RESOURCE_POWER] > 0 && st.store[RESOURCE_POWER] > 0) {
            creep.withdraw(st, RESOURCE_POWER);
            return
        }
        if (powerSpawn.store && powerSpawn.store[RESOURCE_POWER] > 0 && term.store[RESOURCE_POWER] > 0) {
            creep.withdraw(term, RESOURCE_POWER);
            return
        }
        ////Nuker
        if (nuker.store && nuker.store[RESOURCE_GHODIUM] > 0 && creep.store[RESOURCE_GHODIUM] > 0) {
            creep.transfer(nuker, RESOURCE_GHODIUM);
            return
        }
        if (nuker.store && nuker.store[RESOURCE_GHODIUM] > 0 && st.store[RESOURCE_GHODIUM] > 0) {
            creep.withdraw(st, RESOURCE_GHODIUM);
            return
        }
        if (nuker.store && nuker.store[RESOURCE_GHODIUM] > 0 && term.store[RESOURCE_GHODIUM] > 0) {
            creep.withdraw(term, RESOURCE_GHODIUM);
            return
        }
        ////Link
        if (link.store && link.store.getUsedCapacity() < 400 && creep.store.energy > 10) {
            creep.transfer(link, RESOURCE_ENERGY)
            return;
        }
        if (link.store && link.store.getUsedCapacity() > 700 && creep.store.getFreeCapacity() > 100) {
            creep.withdraw(link, RESOURCE_ENERGY)
            return
        }
        ////Storage and Terminal
        if (st &&st.store.energy < 10000 && creep.store.energy > 10) {
            creep.transfer(st, RESOURCE_ENERGY)
            return
        }
        if (term && term.store && term.store.energy < 75000 && creep.store.energy > 10) {
            creep.transfer(term, RESOURCE_ENERGY)
            return
        }
        if (term && st && creep.store.getFreeCapacity() > 100) {
            for (c of RESOURCES_ALL) {
                if ((term && term.store && term.store[c] >= 11000 && c !== RESOURCE_ENERGY)) {
                    creep.withdraw(term, c);
                    return
                }
                if (term && c === RESOURCE_ENERGY && term.store && term.store[RESOURCE_ENERGY] >= 110000) {
                    creep.withdraw(term, c);
                    return
                }
            }
            for (c of RESOURCES_ALL) {
                if ((st.store[c] >= 6000 && c !== RESOURCE_ENERGY)) {
                    creep.withdraw(st, c);
                    return
                }
                if (c === RESOURCE_ENERGY && st.store[RESOURCE_ENERGY] >= 520000) {
                    creep.withdraw(st, c);
                    return
                }
            }
        }
        for (c of RESOURCES_ALL) {
            if ((st.store[c] >= 6000 && term && term.store && term.store[c] < 5000 && c !== RESOURCE_ENERGY)) {
                creep.withdraw(st, c);
                return
            }
            if (c === RESOURCE_ENERGY && term && term.store && term.store[c] < 75000 && st.store[RESOURCE_ENERGY] >= 520000) {
                creep.withdraw(st, c);
                return
            }
        }
        for (c of RESOURCES_ALL) {
            if (st.store[c] >= 5000 && term && term.store && term.store[c] < 5000 && creep.store[c] > 0) {
                creep.transfer(term, c);
                return;
            }
        }
        ////clear non-energy cargo
        if ( st && creep.store.getUsedCapacity() > creep.store.energy) {
            _.some(creep.store, (amount, resource) => amount && creep.transfer(st, resource) !== undefined);
            return
        }
        //// get energy
        t = creep.getEasyEnergy()
        if (t instanceof Resource) {
            creep.pickup(t)
        } else {
            creep.withdraw(t, RESOURCE_ENERGY)
        }

        //// end
    }
}
