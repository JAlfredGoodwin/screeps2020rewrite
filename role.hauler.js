/*  Civil Mission Structure
 *   mission.name = thismission
 *   mission.missionType = mType;
 *   mission.target = mTarget;
 *   mission.missionRoom = mRoom;
 *   mission.assignedSpawnRoom = homeRoom;
 *   mission.spawned = false;
 *   mission.time = Game.time;
 */
module.exports = {
    run: function (creep) {
        if (creep.spawning || Game.cpu.bucket < Memory.Settings.CPU.LowerBucketLimit) {
            return
        }
        //console.log(creep.name + ' exists')
        if (creep.memory.mission) {
            mission = creep.memory.mission
            delete Memory.missions.civil[mission.mission]
        } else {
            mission = {'target': creep.pos.roomName, 'missionRoom': creep.pos.roomName, 'missionType': 'hauler'}
        }

        if (!creep.isBoosted() && creep.ticksToLive < 350 && creep.hitsMax > 2600 && creep.room.controller && creep.room.controller.my && creep.room.energyAvailable && creep.room.energyAvailable >= 900) {
            creep.memory.renewing = true;
        }
        if (creep.memory.renewing && creep.ticksToLive > 1000) {
            creep.memory.renewing = false;
        }
        if (creep.memory.renewing === true) {
            goRenew(creep);
            return;
        }
        if (creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = false;

        } else if (creep.store[RESOURCE_ENERGY] === creep.carryCapacity) {
            creep.memory.working = true;
            delete creep.memory.homeRoom
        }
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.findClosestSpawn().pos.roomName
        }
        if (creep.pos.roomName === creep.memory.homeRoom) {
            if (creep.memory.working) {
                require('role.delivery').run(creep);
                return
            } else {
                dest = Game.getObjectById(mission.target)
                if (dest && dest.pos) {
                    creep.travelTo(dest, {useFindRoute: true, range: 5})
                } else {
                    dest = new RoomPosition(25, 25, mission.missionRoom)
                    creep.travelTo(dest, {useFindRoute: true, range: 5})
                }
            }
        }
        if (creep.pos.roomName === mission.missionRoom) {
            if (creep.memory.working) {
                creep.travelTo(Game.rooms[creep.memory.homeRoom].controller)
                return
            } else {
                if (creep.memory.homeRoom === mission.missionRoom) {
                    creep.grabResource(creep.getDroppedResources())
                } else {
                    creep.grabEasyEnergy()
                }
            }
        }
        if (creep.pos.roomName !== mission.missionRoom && creep.pos.roomName !== creep.memory.homeRoom) {
            if (creep.memory.working) {
                creep.travelTo(Game.rooms[creep.memory.homeRoom].controller)
            } else {
                dest = Game.getObjectById(mission.target)
                if (dest && dest.pos) {
                    creep.travelTo(dest, {useFindRoute: true, range: 5})
                } else {
                    dest = new RoomPosition(25, 25, mission.missionRoom)
                    creep.travelTo(dest, {useFindRoute: true, range: 5})
                }
            }
        }
        let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (droppedEnergy.length >= 1
        ) {
            creep.pickup(droppedEnergy[0]);
        }
        if (creep.store.energy > 0) {
            let roadspot = creep.pos.lookFor(LOOK_STRUCTURES, {filter: (c) => c.structureType === STRUCTURE_ROAD});
            if (roadspot[0] && roadspot[0].hits < roadspot[0].hitsMax) {
                creep.repair(roadspot[0]);
            }
        }
    }
}
