require('prototype.spawn')();


module.exports = {
    run: function () {
        if (Memory.Settings.Spawning === false) {
            return;
        }
        const totals = global.census;
        global.totals = totals;
        cargospawn = false
        vendorspawn = false

        for (SpawnZ in Game.spawns) {
            SpawnT = Game.spawns[SpawnZ];
            if (SpawnT.spawning) {
                continue
            }
            //return
            rn = Game.spawns[SpawnZ].room.name
            thisroom = Game.rooms[rn]
            thisroomname = rn
            thisroomName = rn


            let numberOfScouts = (totals['scout'] || 0);
            let numberOfUpgraders = (totals[rn + '_upgrader'] || 0)
            let numberOfBuilders = (totals[rn + '_builder'] || 0)
            let numberOfRepairers = (totals[rn + '_repairer'] || 0)
            let numberOfCouriers = (totals[rn + '_courier'] || 0)
            let numberOfCargos = (totals[rn + '_cargo'] || 0)
            let numberOfVendors = (totals[rn + '_vendor'] || 0)
            let numberOfMiners = (totals['eminer'] || 0);
            let numberOfTravelingBuilders = (totals['travelingbuilder'] || 0)

            if (Game.time % 5000 === 29) {
                delete SpawnT.memory.mineralAmount
            }
            if (!SpawnT.memory.mineralAmount) {
                let Mincheck = SpawnT.room.find(FIND_MINERALS);
                SpawnT.memory.mineralAmount = Mincheck[0].mineralAmount;
                if (thisroom.controller.level >= 6) {
                    thisroom.createConstructionSite(Mincheck[0].pos, STRUCTURE_EXTRACTOR)
                }
                if (!Memory.MineralSpots) {
                    Memory.MineralSpots = {};
                }
                if (!Memory.MineralSpots[rn]) {
                    Memory.MineralSpots[rn] = {};
                    let MinerPositions = Mincheck[0].getMiningPositions()
                    Game.rooms[rn].createConstructionSite(MinerPositions[0].x, MinerPositions[0].y, STRUCTURE_CONTAINER)
                    let PositionNumbers = Object.keys(MinerPositions).length
                    Memory.MineralSpots[rn].number = PositionNumbers
                    Memory.MineralSpots[rn].type = Mincheck[0].mineralType
                    if (!Memory.MineralSpots[rn].mineralTime) {
                        Memory.MineralSpots[rn].mineralTime = Game.time
                    }
                    console.log('Found ' + PositionNumbers + 'miner spots for room ' + rn + ' of type ' + Mincheck[0].mineralType)
                }
                if (SpawnT.memory.mineralAmount > 0 && thisroom.controller.level >= 6 && Game.time < Memory.MineralSpots[rn].mineralTime) {
                    for (x = 0; x < Memory.MineralSpots[rn].number; x++) {
                        createMission('mineral', Mincheck[0].id, rn)
                        Memory.MineralSpots[rn].mineralTime = Game.time + 1700
                    }
                }
            }
            if (thisroom.controller.level < 8) {
                UpgraderLimit = Memory.Settings.SpawnLimits.Civilian.Upgraders
            } else {
                UpgraderLimit = 1
            }
            let energyCap = thisroom.energyCapacityAvailable;
            let energy = thisroom.energyAvailable;
            let name = undefined;
            if (Memory.respawn === true) {
                name = SpawnT.createWorker(300, 'pioneer')
                if (totals['pioneer'] > 6 || Game.time > Memory.Settings.RespawnTime + 1900) {
                    delete Memory.respawn
                }
                if (name === 0) {
                    log.notify(thisroomName + ' is spawning a pioneer')
                    return;
                }
            }

            if (numberOfCouriers === 0 && thisroom.controller.level > 1) {
                name = SpawnT.createCourier(energy, 'courier');
                if (name === 0) {
                    log.notify(thisroomName + ' is spawning an emergency courier')
                    continue;
                }

            }
            if (totals[thisroomname + '_eminer'] === 0) {
                name = SpawnT.createMiner(energy, 'eminer');
                if (name === 0) {
                    log.notify(thisroomName + ' is spawning an emergency miner')
                    continue;
                }

            }
            switch (true) {
                case numberOfBuilders < Memory.Settings.SpawnLimits.Civilian.Builders:
                    name = SpawnT.createWorker(energyCap, 'builder')
                    if (name === 0) {
                        log.notify(thisroomName + ' is spawning a builder')
                        continue;
                    }
                    break;
                case numberOfCouriers < Memory.Settings.SpawnLimits.Civilian.Couriers:
                    name = SpawnT.createCourier(energyCap, 'courier')
                    if (name === 0) {
                        log.notification(thisroomName + ' is spawning a courier')
                        continue;
                    }
                    break;
                case (!vendorspawn && numberOfVendors < 1 && thisroom.controller.level > 4):
                    name = SpawnT.createCourier(energyCap, 'vendor')
                    if (name === 0) {
                        log.notification(thisroomName + ' is spawning an vendor')
                        vendorspawn = true
                        continue;
                    }
                    break;
                case numberOfUpgraders < UpgraderLimit:
                    name = SpawnT.createWorker(energyCap, 'upgrader')
                    if (name === 0) {
                        log.notification(thisroomName + ' is spawning an upgrader')
                        continue;
                    }
                    break;
                case numberOfRepairers < Memory.Settings.SpawnLimits.Civilian.Repairers:
                    name = SpawnT.createWorker(energyCap, 'repairer')
                    if (name === 0) {
                        log.notification(thisroomName + ' is spawning a repairer')
                        continue;
                    }
                    break;
            }
/////// Mission Spawning
            if (SpawnT.missionSpawn(energyCap) === true) {
                name = 0
                continue;
            }
//return
            /*
                        if (name !== 0 && numberOfMiners < Memory.CurrentlyMining.length) {
                            if (Game.time >= global.pauseMiner || !global.pauseMiner) {
                                name = SpawnT.createMiner(energyCap, 'eminer')
                                if (name === 0) {
                                    totals['eminer']++
                                    log.notification(thisroomName + ' is spawning a miner')
                                    continue;
                                }
                            }
                        }*/
            if (!cargospawn && numberOfCargos < 1 && thisroom.controller.level >= 3) {
                name = SpawnT.createCourier(energyCap, 'cargo')
                if (name === 0) {
                    log.notification(thisroomName + ' is spawning a cargo')
                    cargospawn = true
                    continue;
                }
            }
/////// Scout Spawning
            if ((Game.time > global.scoutPause || !global.scoutPause) && numberOfScouts < (Memory.CurrentlyMining.length * 0.25) && energy > 300 && energy >= (energyCap * 0.6)) {
                name = SpawnT.spawnScout()
                if (name === 0) {
                    global.scoutPause = Game.time + 1750
                    numberOfScouts++
                    log.notification(thisroomName + ' is spawning a scout')
                    continue;
                }
            }
/////// Renewing
            if (name !== 0 && (numberOfTravelingBuilders * 10) < Object.keys(Game.constructionSites).length) {
                name = SpawnT.createWorker(energyCap, 'travelingbuilder')
                if (name === 0) {
                    log.notify(thisroomName + ' is spawning a  traveling builder')
                    continue;
                }
            }
            if (name !== 0) {
                if (!Memory.rooms[thisroomname].idle) {
                    Memory.rooms[thisroomname].idle = 0
                }
                let closeCreep = SpawnT.pos.findInRange(FIND_MY_CREEPS, 1);
                if (Object.keys(closeCreep).length >= 1) {
                    let proposed = _.sortBy(closeCreep, 'ticksToLive');
                    if (!proposed[0].isBoosted() && proposed[0].ticksToLive <= 1350 && thisroom.energyAvailable >= (thisroom.energyCapacityAvailable * .95)) {
                        t = SpawnT.renewCreep(proposed[0]);
                        if (t !== 0) {
                            Memory.rooms[thisroomname].idle++
                        } else {
                            Memory.rooms[thisroomname].idle = 0
                        }
                    }
                }
                if (Memory.rooms[thisroomname].idle && Memory.rooms[thisroomname].idle >= 50 && Game.rooms[thisroomName].controller.level > 3) {
                    //updateRoomScores()
                    if (Memory.Settings.Misc.nextRoom && Memory.Settings.Misc.nextRoomScore > 300) {
                        if (Game.rooms[Memory.Settings.Misc.nextRoom]) {
                            //claimRoom(Memory.Settings.Misc.nextRoom)
                        } else {
                            //sendScout(Memory.Settings.Misc.nextRoom);
                        }
                    }
                }
            }
        }
    }
}
