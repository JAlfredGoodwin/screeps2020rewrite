global.roomPlanningProcess = function () {
    let flagx = -1
    if (!global.planner) {
        global.planner = {};
    }
    if (!Memory.planner) {
        Memory.planner = {};
    }
    if (!Memory.planner.inprogress) {
        Memory.planner.inprogress = {};
    }
    if (Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
        log.warn('Room Planner exiting early due to bucket below ' + Memory.Settings.CPU.MediumBucketLimit + '. Bucket is currently ' + Game.cpu.bucket)
        return
    }
    if (!global.planner.currentroom) {
        for (let room in Memory.rooms) {
            if (Game.rooms[room]
                && Game.rooms[room].controller
                && (!Memory.rooms[room].anchor || !Memory.rooms[room].posScore)) {
                global.planner.currentroom = room
            } else {
                continue;
            }
            if (Memory.planner && Memory.planner.inprogress && Memory.planner.inprogress[room]) {
                global.planner = JSON.parse(Memory.planner.inprogress[room])
            }
        }
        if (!global.planner.currentroom) {
            return
        }
    }
    room = global.planner.currentroom
    if (!Game.rooms[room]) {
        delete global.planner.currentroom
        return;
    } else {
        if (Memory.Settings.Logging.RoomPlanning === true) {
            log.notify('RoomPlanner running on room ' + room)
        }
    }
    if (!global.planner.workingroom) {
        global.planner.workingroom = {}
    }

    if (!global.importants) {
        global.importants = {};
        let sources = Game.rooms[room].find(FIND_SOURCES);
        global.importants.mineral = Game.rooms[room].find(FIND_MINERALS)[0].id
        if (sources.length > 0) {
            try {
                global.importants.s1 = sources[0].id
                global.importants.s2 = sources[1].id
            } catch {
            }
        }
    }

    sourceOne = Game.getObjectById(global.importants.s1)
    sourceTwo = Game.getObjectById(global.importants.s2)
    mineral = Game.getObjectById(global.importants.mineral)
    if (!Memory.rooms[room].posScore) {

        for (x = 6; x < 44; x++) {
            if (!global.planner.workingroom.X) {
                global.planner.workingroom.X = {}
            }
            if (!global.planner.workingroom.X[x]) {
                global.planner.workingroom.X[x] = {}
            }

            for (y = 6; y < 44; y++) {
                if (Game.cpu.getUsed() >= (Game.cpu.limit * 0.8)) {
                    if (Memory.Settings.Logging.RoomPlanning === true) {
                        log.warn('Room Planner Exiting due to CPU Use Limit, current Used: ' + Game.cpu.getUsed())
                    }
                    return
                }
                if (Game.cpu.bucket < Memory.Settings.CPU.MediumBucketLimit) {
                    if (Memory.Settings.Logging.RoomPlanning === true) {
                        log.warn('Room Planner Exiting due to bucket below ' + Memory.Settings.CPU.MediumBucketLimit + '. Bucket is currently ' + Game.cpu.bucket)
                    }
                    return
                }

                if (!global.planner.workingroom.X[x].Y) {
                    global.planner.workingroom.X[x].Y = {}
                }
                if (global.planner.workingroom.X[x].Y
                    && global.planner.workingroom.X[x].Y[y]) {
                    continue;
                }
                let cp = [x, y];
                if (_.isEqual(global.planner.currentPos, cp)) {
                    global.planner.currentPoscount++;
                } else {
                    global.planner.currentPos = [x, y]
                    global.planner.currentPoscount = 1;
                }
                if (global.planner.currentPoscount >= 15) {
                    global.planner.workingroom.X[x].Y[y] = 75000;
                    log.error('Room planner hung up on a spot')
                    continue;
                }

                let candidate = new RoomPosition(x, y, room)
                Game.rooms[room].visual.rect(candidate.x, candidate.y)
                if (candidate) {
                    if (openSpaceAround(candidate, 4) === true) {
                        score = 0
                    } else {
                        if (openSpaceAround(candidate, 3) === true) {
                            score = 15
                        } else {
                            score = 5000;
                            continue;
                        }

                    }
                    cent = new RoomPosition(25,25,room)
                    score += (candidate.findPathTo(cent, {
                        ignoreDestructibleStructures: true,
                        ignoreRoads: true,
                        ignoreCreeps: true
                    }).length)
                    score += (candidate.findPathTo(sourceOne, {
                        ignoreDestructibleStructures: true,
                        ignoreRoads: true,
                        ignoreCreeps: true
                    }).length * 0.5)
                    if (sourceTwo) {
                        score += (candidate.findPathTo(sourceTwo, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length * 0.5)
                    } else {
                        score += 30
                    }
                    if (mineral) {
                        score += (candidate.findPathTo(mineral, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length + 8)
                    }
                    conscore = candidate.findPathTo(Game.rooms[room].controller, {
                        ignoreDestructibleStructures: true,
                        ignoreRoads: true,
                        ignoreCreeps: true
                    }).length
                    if (conscore < 6) {
                        conscore = 5000
                    }
                    score += conscore
                    if (Game.rooms[room].storage) {
                        score += candidate.findPathTo(Game.rooms[room].storage, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length
                    }
                    if (Game.rooms[room].terminal) {
                        score += (candidate.findPathTo(Game.rooms[room].terminal, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length - 2)
                    }
                    global.planner.workingroom.X[x].Y[y] = score;
                }
            }
        }
        let finalpos = undefined;
        let current = 500000000000;
        let lowest = 500000000000;
        for (x = 6; x < 44; x++) {
            for (y = 6; y < 44; y++) {
                if (!global.planner.workingroom.X[x].Y[y]) {
                    continue

                }
                currentpos = global.planner.workingroom.X[x].Y[y]
                if (currentpos <= lowest) {
                    lowest = currentpos;
                    finalpos = x + '_' + y
                    flagx = x
                    flagy = y
                }
            }
        }
        if (Game.time % 15 === 0) {
            Memory.planner.inprogress[room] = JSON.stringify(global.planner);
        }
        if (flagx > 0) {
            name = 'Anchor-' + room
            Game.rooms[room].createFlag(flagx, flagy, name)
            Memory.rooms[room].anchor = finalpos
            Memory.rooms[room].posScore = lowest
            log.notification('Finished assessing room ' + room + ' with position score ' + lowest + ' and anchor at ' + finalpos)
            delete global.planner;
            delete global.importants;
            delete Memory.planner.inprogress[room]
        } else {
            log.err('Room Planner has failed to find an anchor position for room ' + room);
            Memory.rooms[room].posScore = -500
            Memory.rooms[room].anchor = 'x_y'
            delete global.planner;
            delete global.importants;
            delete Memory.planner.inprogress[room]

        }
    }
}
openSpaceAround = function (positon, input = 5) {
    for (let x = 0 - input; x < 1 + input; x++) {
        for (let y = 0 - input; y < 1 + input; y++) {
            let position = new RoomPosition(positon.x + x, positon.y + y, positon.roomName);
            let terrainAtPositon = Game.map.getTerrainAt(position);
            if (terrainAtPositon === 'wall') {
                Game.rooms[positon.roomName].visual.rect(positon.x, positon.y, {stroke: 'red'})
                return false;
            }
        }
    }
    return true;
};
global.emptySpaceAround = function (positon, input = 3) {
    for (let y = 0 - input; y < 1 + input; y++) {
        for (let x = 0 - input; x < 1 + input; x++) {
            let position = new RoomPosition(positon.x + x, positon.y + y, positon.roomName);
            items = Game.rooms[positon.roomName].lookAt(position)
            if (items.length === 1 && items[0].terrain === 'wall') {
                return false
            }
            for (item = 0; item < items.length; item++) {
                let thisitem = items[item]
                if (thisitem.type === 'terrain' && thisitem.terrain === 'wall') {
                    return false
                }
                if (thisitem.type === 'structure') {
                    if (thisitem.structure.structureType === 'road' || thisitem.structure.structureType === 'rampart' || thisitem.structure.structureType === 'extension') {

                    } else {
                        return false
                    }
                }
            }
        }
    }
    return true;
};
global.extensionPlanner = function (room) {
    if (!Memory.rooms[room].anchor) {
        return
    }
    if (!Memory.rooms[room].extensions) {
        Memory.rooms[room].extensions = {}
    }
    if (!Memory.extensionsplanner) {
        Memory.extensionsplanner = {}
        Memory.extensionsplanner.inprogress = {}
    }
    if (!global.extensionsplanner) {
        global.extensionsplanner = {}
        global.extensionsplanner.workingroom = {}
    }
    anchor = Memory.rooms[room].anchor
    let splitpos = anchor.split('_');
    let anchorPos = new RoomPosition(Number(splitpos[0]), Number(splitpos[1]), room);
    anchorx = anchorPos.x % 2
    anchory = anchorPos.y % 2
    firstPos = false
    secPos = false
    thrPos = false
    if (Memory.rooms[room].extensions.first) {
        splitfpos = Memory.rooms[room].extensions.first.split('_');
        firstPos = new RoomPosition(Number(splitfpos[0]), Number(splitfpos[1]), room);
    }
    if (Memory.rooms[room].extensions.second) {
        splitspos = Memory.rooms[room].extensions.second.split('_');
        secPos = new RoomPosition(Number(splitspos[0]), Number(splitspos[1]), room);
    }
    if (Memory.rooms[room].extensions.third) {
        splittpos = Memory.rooms[room].extensions.third.split('_');
        thrPos = new RoomPosition(Number(splittpos[0]), Number(splittpos[1]), room);
    }
    if (!Memory.rooms[room].extensions.fourth) {
        for (x = 4; x < 46; x++) {
            if (!global.extensionsplanner.workingroom.X) {
                global.extensionsplanner.workingroom.X = {}
            }
            if (!global.extensionsplanner.workingroom.X[x]) {
                global.extensionsplanner.workingroom.X[x] = {}
            }
            for (y = 4; y < 46; y++) {
                if (Game.cpu.getUsed() >= (Game.cpu.limit * 0.8)) {
                    if (Memory.Settings.Logging.RoomPlanning === true) {
                        log.warn('Room extensionsplanner Exiting due to CPU Use Limit')
                    }
                    return
                }
                if (Game.cpu.bucket < Memory.Settings.CPU.UpperBucketLimit) {
                    if (Memory.Settings.Logging.RoomPlanning === true) {
                        log.notification('Extension Planner Exiting due to bucket below ' + Memory.Settings.CPU.UpperBucketLimit + '. Bucket is currently ' + Game.cpu.bucket)
                    }
                    return
                }

                if (!global.extensionsplanner.workingroom.X[x].Y) {
                    global.extensionsplanner.workingroom.X[x].Y = {}
                }
                if (global.extensionsplanner.workingroom.X[x].Y
                    && global.extensionsplanner.workingroom.X[x].Y[y]) {
                    continue;
                }
                let cp = [x, y];
                if (_.isEqual(global.extensionsplanner.currentPos, cp)) {
                    global.extensionsplanner.currentPoscount++;
                } else {
                    global.extensionsplanner.currentPos = [x, y]
                    global.extensionsplanner.currentPoscount = 1;
                }
                if (x % 2 !== anchorx || y % 2 != anchory) {
                    continue
                }
                if (global.extensionsplanner.currentPoscount >= 15) {
                    global.extensionsplanner.workingroom.X[x].Y[y] = Infinity;
                    console.log('continuing for infinity')
                    continue;
                }
                let candidate = new RoomPosition(x, y, room)
                Game.rooms[candidate.roomName].visual.circle(candidate.x, candidate.y)
                if (candidate) {
                    if (emptySpaceAround(candidate, 2) === true) {
                        score = 0
                    } else {
                        if (emptySpaceAround(candidate, 1) === true) {
                            score = 20
                        } else {
                            score = 5000;
                            continue;
                        }
                    }
                    ascore = candidate.findPathTo(anchorPos, {
                        ignoreDestructibleStructures: true,
                        ignoreRoads: true,
                        ignoreCreeps: true
                    }).length
                    if (ascore < 5) {
                        ascore = 5000
                    }
                    score += ascore
                    extender = candidate.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => (structure.structureType === STRUCTURE_EXTENSION)
                    });
                    escore = candidate.findPathTo(extender, {
                        ignoreDestructibleStructures: true,
                        ignoreRoads: true,
                        ignoreCreeps: true
                    }).length

                    score += escore
                    conscore = candidate.findPathTo(Game.rooms[room].controller, {
                        ignoreDestructibleStructures: true,
                        ignoreRoads: true,
                        ignoreCreeps: true
                    }).length
                    if (conscore < 5) {
                        conscore = 5000
                    } else {
                        conscore = 0
                    }
                    score += conscore
                    if (firstPos) {
                        fcore = candidate.findPathTo(firstPos, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length
                        if (fcore < 4) {
                            fcore = 5000
                        }

                        score += fcore
                    }
                    if (secPos) {
                        Sscore = candidate.findPathTo(secPos, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length
                        if (Sscore < 4) {
                            Sscore = 5000
                        }
                        score += Sscore
                    }
                    if (thrPos) {
                        tscore = candidate.findPathTo(thrPos, {
                            ignoreDestructibleStructures: true,
                            ignoreRoads: true,
                            ignoreCreeps: true
                        }).length
                        if (tscore < 4) {
                            tscore = 5000
                        }
                        score += tscore
                    }

                    global.extensionsplanner.workingroom.X[x].Y[y] = score;
                }
            }
        }
        let finalpos = undefined;
        let lowest = 50000;
        for (x = 4; x < 46; x++) {
            for (y = 4; y < 46; y++) {
                if (!global.extensionsplanner.workingroom.X[x].Y[y]) {
                    continue
                }
                currentpos = global.extensionsplanner.workingroom.X[x].Y[y]
                if (currentpos <= lowest) {
                    lowest = currentpos;
                    finalpos = x + '_' + y
                    flagx = x
                    flagy = y
                    if (!Memory.rooms[room].extensions.first && Memory.Settings.Logging.PlannerFlags) {
//                        fl4 = new RoomPosition(flagx, flagy, room)
//                        fl4.createFlag('Ext-' + room + flagx + flagy)
                    }
                }
            }
        }
        if (Game.time % 15 === 0) {
            if (!Memory.extensionsplanner.inprogress) {
                Memory.extensionsplanner.inprogress = {}
            }
            Memory.extensionsplanner.inprogress[room] = JSON.stringify(global.extensionsplanner);
        }
        if (flagx > 0) {
            if (Memory.rooms[room].extensions.first && Memory.rooms[room].extensions.second && Memory.rooms[room].extensions.third && !Memory.rooms[room].extensions.fourth) {
                Memory.rooms[room].extensions.fourth = finalpos
            }
            if (Memory.rooms[room].extensions.first && Memory.rooms[room].extensions.second && !Memory.rooms[room].extensions.third) {
                Memory.rooms[room].extensions.third = finalpos
            }
            if (Memory.rooms[room].extensions.first && !Memory.rooms[room].extensions.second) {
                Memory.rooms[room].extensions.second = finalpos
            }
            if (!Memory.rooms[room].extensions.first) {
                Memory.rooms[room].extensions.first = finalpos
                //flfin = new RoomPosition(flagx, flagy, room)
                //flfin.createFlag('Fin-' + room + flagx + flagy)
            }
            delete global.extensionsplanner;
            delete Memory.extensionsplanner
        } else {
            delete global.extensionsplanner;
            delete Memory.extensionsplanner

        }
    }
}
