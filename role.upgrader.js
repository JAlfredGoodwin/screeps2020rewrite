module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        var conta = {};
        if (!creep.memory.homeRoom) {
            creep.memory.homeRoom = creep.pos.roomName
        }
        if (creep.pos.roomName != creep.memory.homeRoom) {
            creep.travelTo(Game.rooms[creep.memory.homeRoom].controller, {repath: 0.85, stuckValue: 1})
        }
        if (creep.memory.working == true && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.store.getFreeCapacity()===0) {
            creep.memory.working = true;
        }
        let droppedEnergy = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1);
        if (droppedEnergy.length >= 1 && droppedEnergy[0].resourceType === RESOURCE_ENERGY) {
            creep.pickup(droppedEnergy[0]);
        }
        if (creep.memory.renewing && creep.ticksToLive > 900) {
            creep.memory.renewing = false;
        }
        if (creep.ticksToLive < 350 && creep.carryCapacity > 50 && creep.hitsMax >= 2600 && !creep.isBoosted() && creep.room.energyAvailable >= 2500) {
            creep.memory.renewing = true;
        }
        if (creep.memory.renewing === true) {
            goRenew(creep);
            return;
        }
        if (creep.memory.working === true) {
            if (!creep.room.controller){return}
            if (creep.room.controller && creep.pos.inRangeTo(creep.room.controller, 3)) {
                creep.upgradeController(creep.room.controller);
                creep.travelTo(creep.room.controller, {range: 2, ignoreCreeps: false, maxRooms: 1, repath: 0.91});
            } else {
                creep.travelTo(creep.room.controller, {range: 2, ignoreCreeps: false, maxRooms: 1, repath: 0.91});
            }
        } else {
            conta = creep.getEasyEnergy()
            if (conta) {
                if (creep.pos.isNearTo(conta)) {
                    creep.withdraw(conta, RESOURCE_ENERGY);
                    if (!creep.pos.inRangeTo(creep.room.controller, 3)) {
                        creep.travelTo(creep.room.controller, {range: 3, ignoreCreeps: false});
                    }
                } else {
                    creep.travelTo(conta, {range: 1, ignoreCreeps: false});
                }
            }
        }
    }
}
