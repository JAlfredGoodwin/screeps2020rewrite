module.exports = function () {
    // ADD MEMORY TO STRUCTURES
    Object.defineProperty(Structure.prototype, 'memory', {
        configurable: true,
        get: function () {
            if (_.isUndefined(Memory.structure)) {
                Memory.structure = {};
            }
            if (!_.isObject(Memory.structure)) {
                return undefined;
            }
            return Memory.structure[this.id] = Memory.structure[this.id] || undefined;
        },
        set: function (value) {
            if (_.isUndefined(Memory.structure)) {
                Memory.structure = {};
            }
            if (!_.isObject(Memory.structure)) {
                throw new Error('Could not set structure memory');
            }
            Memory.structure[this.id] = value;
        }
    });
    StructureContainer.prototype.percentFilled =
        StructureStorage.prototype.percentFilled = function () {
            return (this.store.energy / this.storeCapacity);
        };
    StructureExtension.prototype.percentFilled =
        StructureSpawn.prototype.percentFilled =
            StructureLink.prototype.percentFilled = function () {
                return (this.energy / this.energyCapacity);
            };
    Structure.prototype.isNear = function (arg, range) {
        if (typeof arg == 'string')
            return this.pos.findInRange(FIND_STRUCTURES, range, {filter: s => s.structureType == arg}).length > 0;
        if (typeof arg == 'number')
            return this.pos.findInRange(arg, range).length > 0;
    };

    StructureTerminal.prototype.getPayloadWeCanAfford = function (dest) {
        return Math.floor(this.store.energy / (this.getFee(dest) + 1));
    };

    StructureTerminal.prototype.getFee = function (dest) {
        let dist = Game.map.getRoomLinearDistance(this.room.name, dest);
        return Math.log(0.1 * dist + 0.9) + 0.1;
    };

    StructureTerminal.prototype.sendAllEnergy = function (dest) {
        let amt = this.getPayloadWeCanAfford(dest);
        if (amt < TERMINAL_MIN_SEND)
            return ERR_NOT_ENOUGH_RESOURCES;
        return this.send(RESOURCE_ENERGY, amt, dest, 'Resource transfer');
    }
    Structure.prototype.freePositionsAround = function () {
        let positions = [];
        for (let x = -1; x < 2; x++) {
            for (let y = -1; y < 2; y++) {
                let position = new RoomPosition(this.pos.x + x, this.pos.y + y, this.room.name);
                if (!(position.x === this.pos.x && position.y === this.pos.y)) {
                    let terrainAtPositon = Game.map.getTerrainAt(position);
                    if (terrainAtPositon === "swamp" || terrainAtPositon === "plain") {
                        positions.push(position);
                    }
                }
            }
        }
        return positions;
    }
};
