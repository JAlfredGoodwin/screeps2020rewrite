/*  Civil Mission Structure
 *   mission.name = thismission
 *   mission.missionType = mType;
 *   mission.target = mTarget; id of controller
 *   mission.missionRoom = mRoom; room to be claimed
 *   mission.assignedSpawnRoom = homeRoom;
 *   mission.spawned = false;
 *   mission.time = Game.time;
 */

module.exports = {
    run: function (creep) {
        if (creep.spawning) {
            return
        }
        if (creep.memory.mission) {
            mem = creep.memory.mission
        } else {
            creep.memory.role = 'zombie';
            return
        }
        delete Memory.missions.civil[mem.mission]
        targetController = Game.getObjectById(mem.target)
        if (!targetController) {
            targetController = {};
            targetController.pos = new RoomPosition(25, 25, mem.missionRoom);
        }
        if (creep.pos.isNearTo(targetController)) {
            if ((targetController.owner  &&  !targetController.my) || (targetController.reservation && targetController.reservation.username !== WHOAMI)) {
                creep.attackController(targetController);
                creep.memory.role = 'zombie';
            } else {
                creep.claimController(targetController);
            }
        } else {
            if (targetController.pos) {
                creep.travelTo(targetController.pos);
            }
        }
    }
}
